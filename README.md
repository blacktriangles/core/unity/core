# UnityCore

UnityCore is a set of utilities and common scripts shared by all blacktriangles
Unity projects.

Maintained by Howard N Smith
howard@blacktriangles.com
http://blog.blacktriangles.com

Public Repository:
- ssh: git@gitlab.com:blacktriangles/core/unity

## Versions
- Unity v2020.1.17f1
- Requires Scripting Runtime Version 4.x Equivalent

## Credits
This repository uses resources from the following sources

Icons By:
- Glyphicons Pro (http://glyphicons.com)
- Mario Del Valle (mariodelvalle.github.io/CaptainIconWeb)

JSON Parser Heavily Modified From:
- https://gist.github.com/darktable/1411710 (Calvin Rien)
- http://techblog.procurios.nl/k/618/news/view/14605/14863/How-do-I-write-my-own-parser-for-JSON.html (Patrick van Bergen)

Grid Texture:
- https://www.flickr.com/photos/zooboing/4302326159/ (Patrick Hoesly)
    -[https://creativecommons.org/licenses/by/2.0/]

TestCharacterSpritesheet from:
- http://opengameart.org/content/player-0 (renegreg)
    - https://www.facebook.com/The.Dark.Puppet
    -[http://creativecommons.org/licenses/by/3.0/]

`MiscUtil` by Jon Skeet:
- http://www.yoda.arachsys.com/csharp/miscutil/

`protobuf-net` net by Marc Gravell
- https://github.com/mgravell/protobuf-net

`Deque` class by Nito (Stephen Cleary)
- http://nitodeque.codeplex.com/SourceControl/latest#Source/PortableClassLibrary/Deque.cs

`BitmaskDrawer` from unity3d answers post by Bunny83
- http://answers.unity3d.com/questions/393992/custom-inspector-multi-select-enum-dropdown.html

`PasswordHash` from Roger Knapp's awesome blog post
- http://csharptest.net/470/another-example-of-how-to-store-a-salted-password-hash/

`CustomListDrawer` from t0chas' gist
- https://gist.github.com/t0chas/34afd1e4c9bc28649311

## Roadmap
- [] Lint / Standardize formatting
- [] Revamped Networking
    - [x] TCP 
    - [x] UDP
    - [] WebSocket
    - [x] Beacon / Probing for local discovery
    - [] NAT Traversal
    - [] Matchmaking
- [] Non-Script Template Support
- [] Expanded UnitTest Coverage
- [] .NET 4.5 Standalone Builds
