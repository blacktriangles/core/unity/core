//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using blacktriangles;
using Jint;
using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace blacktriangles.Testing
{
    [TestFixture]
    public class TestJintIntegration
    {
        // types //////////////////////////////////////////////////////////////
        public class TestObject
        {
            public string name;
            public int age;
            public float weight;
            private bool cannotAccess                              = false;

            public bool publicCanAccess                         { get { return cannotAccess; } }

            public int privateAccess                            { get; private set; }

            public TestObject()
            {
                privateAccess = 100;
            }

            public void Foo(System.Action cb)
            {
                cb();
            }
        }

        public enum TestEnum
        {
            Foo,
            Bar,
            Biz,
            Buzz
        }

        public enum TestEnum2
        {
            One,
            Two,
            Three
        }
        
        // members ////////////////////////////////////////////////////////////

        // tests //////////////////////////////////////////////////////////////
        [Test]
        public void BasicTest()
        {
            TestObject test = new TestObject() {
                name = "TestName",
                age = 25,
                weight = 125
            };

            List<int> objects = new List<int>();
            for(int i = 0; i < 10; ++i)
            {
                objects.Add(i);
            }

            var engine = new Jint.Engine()
                .SetValue("test", new System.Action<int, int, bool>((int a, int b, bool t)=>{
                    if(t) {
                        Assert.AreEqual(a, b);
                    }
                    else {
                        Assert.AreNotEqual(a, b);
                    }
                }))
                .SetValue("objects", objects.ToArray())
                .SetValue("list", objects)
                .SetValue("log", new System.Action<string>((str)=>{
                    Dbg.Log(str);
                }))
                .SetValue("TestEnum", new Dictionary<string,TestEnum>() {
                    { "Foo", TestEnum.Foo },
                    { "Bar", TestEnum.Bar },
                    { "Biz", TestEnum.Biz },
                    { "Buzz", TestEnum.Buzz }
                })
                .SetValue("TestEnum2", JintUtility.Enum<TestEnum2>())
                .Execute(@"
                    var main = function(obj, e, e2) {
                        test(1,1,true);
                        test(1,2,false);
                        test(obj.age, 25, true);
                        test(obj.weight, 125, true);
                        obj.cannotAccess = true;
                        obj.name = 'Modified Name';

                        test(obj.privateAccess, 100, true);
                        obj.privateAccess = 200;
                        test(obj.privateAccess, 200, true); // public/private access ignored

                        test(e, TestEnum.Foo, true);
                        test(e2, TestEnum2.Three, true);

                        var capture = 0;
                        obj.Foo(function(){ capture = 1 });
                        test(capture,1,true);

                        test(objects.length, 10, true);
                        test(list.Count, 10, true);

                        var j = 0;
                        for(var i in list)
                        {
                            test(list[i],j,true);
                            j++;
                        }
                    }
                ");

            Assert.AreEqual(test.name, "TestName");
            engine.Invoke("main", test, TestEnum.Foo, TestEnum2.Three);
            Assert.AreEqual(test.name, "Modified Name");

            // private members are not accessible in scripts, cool!
            Assert.AreEqual(test.publicCanAccess, false);
        }
    }
}
