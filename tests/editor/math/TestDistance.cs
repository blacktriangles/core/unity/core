//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using blacktriangles;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace blacktriangles.Testing
{
    [TestFixture]
    public class TestDistance
    {
        //
        // tests ///////////////////////////////////////////////////////////////
        //

        [Test]
        public void MovingUp()
        {
            Distance dist = new Distance() {
                value = 1.49598e+11,
                unit = DistanceUnit.Meter
            };

            Distance distm = dist.Convert(DistanceUnit.Meter);
            Assert.AreEqual(distm.unit, dist.unit);
            Assert.That(distm.value, Is.EqualTo(dist.value).Within(1).Ulps);

            Distance distkm = dist.Convert(DistanceUnit.Kilometer);
            Assert.AreEqual(distkm.unit, DistanceUnit.Kilometer);
            Assert.That(distkm.value, Is.EqualTo(1.49598e+8).Within(1).Ulps);

            Distance distmm = dist.Convert(DistanceUnit.Megameter);
            Assert.AreEqual(distmm.unit, DistanceUnit.Megameter);
            Assert.That(distmm.value, Is.EqualTo(149598).Within(1).Ulps);

            Distance distau = dist.Convert(DistanceUnit.AstronomicalUnit);
            Assert.AreEqual(distau.unit, DistanceUnit.AstronomicalUnit);
            Assert.That(distau.value, Is.EqualTo(1).Within(1).Ulps);

            Distance distly = dist.Convert(DistanceUnit.Lightyear);
            Assert.AreEqual(distly.unit, DistanceUnit.Lightyear);
            Assert.That(distly.value, Is.EqualTo(1.58125016800783e-5).Within(1).Ulps);

            Distance distpc = dist.Convert(DistanceUnit.Parsec);
            Assert.AreEqual(distpc.unit, DistanceUnit.Parsec);
            Assert.That(distpc.value, Is.EqualTo(4.8481406689063828e-6).Within(1).Ulps);
        }

        //
        // --------------------------------------------------------------------
        //
        
        [Test]
        public void MovingDown()
        {
            Distance dist = new Distance() {
                value =  4.8481406689063828e-6,
                unit = DistanceUnit.Parsec
            };

            Distance distpc = dist.Convert(DistanceUnit.Parsec);
            Assert.AreEqual(distpc.unit, dist.unit);
            Assert.That(distpc.value, Is.EqualTo(dist.value).Within(1).Ulps);

            Distance distly = dist.Convert(DistanceUnit.Lightyear);
            Assert.AreEqual(distly.unit, DistanceUnit.Lightyear);
            Assert.That(distly.value, Is.EqualTo(1.58125016800783e-5).Within(1).Ulps);

            Distance distau = dist.Convert(DistanceUnit.AstronomicalUnit);
            Assert.AreEqual(distau.unit, DistanceUnit.AstronomicalUnit);
            Assert.That(distau.value, Is.EqualTo(1).Within(1).Ulps);

            Distance distmm = dist.Convert(DistanceUnit.Megameter);
            Assert.AreEqual(distmm.unit, DistanceUnit.Megameter);
            Assert.That(distmm.value, Is.EqualTo(149598).Within(1).Ulps);

            Distance distkm = dist.Convert(DistanceUnit.Kilometer);
            Assert.AreEqual(distkm.unit, DistanceUnit.Kilometer);
            Assert.That(distkm.value, Is.EqualTo(1.49598e+8).Within(1).Ulps);

            Distance distm = dist.Convert(DistanceUnit.Meter);
            Assert.AreEqual(distm.unit, DistanceUnit.Meter);
            Assert.That(distm.value, Is.EqualTo(1.49598e+11).Within(1).Ulps);
        }

        //
        // --------------------------------------------------------------------
        //

        [Test]
        public void BasicTest()
        {
            Distance oneAU = new Distance() { value = 1.0, unit = DistanceUnit.AstronomicalUnit };
            Distance oneAUm = oneAU.Convert(DistanceUnit.Meter);
            Assert.AreEqual(oneAUm.unit, DistanceUnit.Meter);
            Assert.That(oneAUm.value, Is.EqualTo(1.49598e+11).Within(1).Ulps);

            Distance oneAUly = oneAU.Convert(DistanceUnit.Lightyear);
            Assert.AreEqual(oneAUly.unit, DistanceUnit.Lightyear);
            Assert.That(oneAUly.value, Is.EqualTo(1.58125016800783e-5).Within(1).Ulps);
        }
        
    }
}
