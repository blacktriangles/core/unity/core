//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using blacktriangles;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace blacktriangles.Testing
{
    [TestFixture]
    public class TestPathGenerator
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        public class Node
            : IPathNode
        {
            public Vector3 worldpos                             { get; set; }
        }

        //
        // helpers ////////////////////////////////////////////////////////////
        //

        public Node[,] GenerateGraph(IntVec2 size)
        {
            Node[,] result = new Node[size.x, size.y];
            for(int x = 0; x < size.x; ++x)
            {
                for(int y = 0; y < size.y; ++y)
                {
                    result[x,y] = new Node() { worldpos = new Vector3(x*10, y*10, 0f) };
                }
            }

            return result;
        }

        //
        // tests ///////////////////////////////////////////////////////////////
        //

        [Test]
        public void FindClosestNode()
        {
            Node[,] graph = GenerateGraph(new IntVec2(10,10));

            PathGenerator.SearchResult result = PathGenerator.FindClosestNode(new Vector3(-100, -100, -100), graph);
            Assert.AreEqual(result.node, graph[0,0]);
            Assert.AreEqual(result.index, IntVec2.zero);

            result = PathGenerator.FindClosestNode(new Vector3(1000, 1000, 1000), graph);
            Assert.AreEqual(result.node, graph[9,9]);
            Assert.AreEqual(result.index, new IntVec2(9,9));

            result = PathGenerator.FindClosestNode(new Vector3(1000, 0, 0), graph);
            Assert.AreEqual(result.node, graph[9,0]);
            Assert.AreEqual(result.index, new IntVec2(9,0));

            result = PathGenerator.FindClosestNode(new Vector3(0, 1000, 0), graph);
            Assert.AreEqual(result.node, graph[0,9]);
            Assert.AreEqual(result.index, new IntVec2(0,9));

            result = PathGenerator.FindClosestNode(new Vector3(12, 0, 0), graph);
            Assert.AreEqual(result.node, graph[1,0]);
            Assert.AreEqual(result.index, new IntVec2(1,0));

            result = PathGenerator.FindClosestNode(new Vector3(0, 47, 0), graph);
            Assert.AreEqual(result.index, new IntVec2(0,5));
            Assert.AreEqual(result.node, graph[0,5]);
            
            result = PathGenerator.FindClosestNode(new Vector3(33, 92, 0), graph);
            Assert.AreEqual(result.node, graph[3,9]);
            Assert.AreEqual(result.index, new IntVec2(3,9));

            // now lets mess around with z
            result = PathGenerator.FindClosestNode(new Vector3(41, 4, 100), graph);
            Assert.AreEqual(result.node, graph[4,0]);
            Assert.AreEqual(result.index, new IntVec2(4,0));

            Vector3 newpos = graph[5,5].worldpos;
            newpos.z = 1000f;
            graph[5,5].worldpos = newpos;
            result = PathGenerator.FindClosestNode(new Vector3(49, 51, 0), graph);
            Assert.AreEqual(result.index, new IntVec2(4,5));
            Assert.AreEqual(result.node, graph[4,5]);
            
            result = PathGenerator.FindClosestNode(new Vector3(49, 51, 1100), graph);
            Assert.AreEqual(result.index, new IntVec2(5,5));
            Assert.AreEqual(result.node, graph[5,5]);
        }

        //
        // --------------------------------------------------------------------
        //

        [Test]
        public void FindStartAndEndpoints()
        {
            Node[,] graph = GenerateGraph(new IntVec2(10,10));

            PathGenerator.SearchResult[] result = PathGenerator.FindStartAndEndpoint(
                new Vector3(-100, -100, -100), 
                new Vector3(1000, 1000, 1000),
                graph);

            Assert.AreEqual(result[0].node, graph[0,0]);
            Assert.AreEqual(result[0].index, IntVec2.zero);
            Assert.AreEqual(result[1].node, graph[9,9]);
            Assert.AreEqual(result[1].index, new IntVec2(9,9));

            result = PathGenerator.FindStartAndEndpoint(
                new Vector3(1000, 0, 0),
                new Vector3(0, 1000, 0),
                graph);

            Assert.AreEqual(result[0].node, graph[9,0]);
            Assert.AreEqual(result[0].index, new IntVec2(9,0));
            Assert.AreEqual(result[1].node, graph[0,9]);
            Assert.AreEqual(result[1].index, new IntVec2(0,9));

            result = PathGenerator.FindStartAndEndpoint(
                new Vector3(12, 0, 0),
                new Vector3(0, 47, 0),
                graph
            );

            Assert.AreEqual(result[0].node, graph[1,0]);
            Assert.AreEqual(result[0].index, new IntVec2(1,0));
            Assert.AreEqual(result[1].index, new IntVec2(0,5));
            Assert.AreEqual(result[1].node, graph[0,5]);
            
            result = PathGenerator.FindStartAndEndpoint(
                new Vector3(33, 92, 0),
                new Vector3(33, 92, 0), 
                graph
            );
            Assert.AreEqual(result[0].node, result[1].node);
            Assert.AreEqual(result[0].node, graph[3,9]);
            Assert.AreEqual(result[1].node, graph[3,9]);
            Assert.AreEqual(result[0].index, new IntVec2(3,9));
            Assert.AreEqual(result[1].index, new IntVec2(3,9));

            // now lets mess around with z
            result = PathGenerator.FindStartAndEndpoint(
                new Vector3(54, 58, 100),
                new Vector3(41, 4, 100), 
                graph
            );
            Assert.AreEqual(result[0].node, graph[5,6]);
            Assert.AreEqual(result[0].index, new IntVec2(5,6));
            Assert.AreEqual(result[1].node, graph[4,0]);
            Assert.AreEqual(result[1].index, new IntVec2(4,0));

            Vector3 newpos = graph[5,5].worldpos;
            newpos.z = 1000f;
            graph[5,5].worldpos = newpos;
            result = PathGenerator.FindStartAndEndpoint(
                new Vector3(49, 51, 1100),
                new Vector3(49, 51, 0),
                graph
            );
            Assert.AreEqual(result[0].index, new IntVec2(5,5));
            Assert.AreEqual(result[0].node, graph[5,5]);
            Assert.AreEqual(result[1].index, new IntVec2(4,5));
            Assert.AreEqual(result[1].node, graph[4,5]);
        }

        //
        // --------------------------------------------------------------------
        //
        
    }
}
