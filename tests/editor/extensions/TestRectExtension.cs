//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace blacktriangles.Testing
{
    [TestFixture]
    public class TestRectExtension
    {
        // tests ///////////////////////////////////////////////////////////////
        [Test]
        public void ConstrainTest()
        {
            Rect parent = new Rect(50, 100, 400, 300);
            Rect child = new Rect(0,0,200,100);

            child.ConstrainTo(parent);

            Assert.AreEqual(parent.x, child.x, Mathf.Epsilon);
            Assert.AreEqual(parent.y, child.y, Mathf.Epsilon);

            child.x = 55.0f;
            child.y = 105.0f;

            Assert.AreEqual(55.0f, child.x, Mathf.Epsilon);
            Assert.AreEqual(105.0f, child.y, Mathf.Epsilon);

            child.ConstrainTo(parent);

            Assert.AreEqual(55.0f, child.x, Mathf.Epsilon);
            Assert.AreEqual(105.0f, child.y, Mathf.Epsilon);

            child.x = 300;

            child.ConstrainTo(parent);

            Assert.AreEqual(250.0f, child.x, Mathf.Epsilon);
            Assert.AreEqual(105.0f, child.y, Mathf.Epsilon);

            child.y = 450.0f;

            child.ConstrainTo(parent);

            Assert.AreEqual(250.0f, child.x, Mathf.Epsilon);
            Assert.AreEqual(300.0f, child.y, Mathf.Epsilon);
        }
    }
}
