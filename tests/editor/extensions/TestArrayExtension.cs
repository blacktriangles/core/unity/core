//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace blacktriangles.Testing
{
    [TestFixture]
    public class TestArrayExtension
    {
        // members /////////////////////////////////////////////////////////////

        // tests ///////////////////////////////////////////////////////////////
        [Test]
        public void BasicTest()
        {
            int[] intArray = new int[] { 5, 4, 3, 2, 1 };
            Assert.That( intArray.Contains( 3 ) );
            Assert.That( intArray.IndexOf( 4 ) == 1 );
            Assert.That( intArray.Contains( 6 ) == false );
            Assert.That( intArray.IndexOf( 100 ) == -1 );
            Assert.That( intArray.IsValidIndex( 0 ) );
            Assert.That( intArray.IsValidIndex( 4 ) );
            Assert.That( intArray.IsValidIndex( 5 ) == false );
            Assert.That( intArray.IsValidIndex( -1 ) == false );
            Assert.That( intArray.IsValidIndex( 6 ) == false );

            System.Random rng = new System.Random( 10 );
            Assert.AreEqual( intArray.Random( rng ), 1, "We are using a seeded number generator so we should be able to predict the results 1" );
            Assert.AreEqual( intArray.Random( rng ), 2, "We are using a seeded number generator so we should be able to predict the results 2" );
            Assert.AreEqual( intArray.Random( rng ), 2, "We are using a seeded number generator so we should be able to predict the results 3" );
            Assert.AreEqual( intArray.Random( rng ), 2, "We are using a seeded number generator so we should be able to predict the results 4" );
        }

        [Test]
        public void MapTest()
        {
            string[] arr = new string[] { "Hello", "This", "Is", "A", "Test" };

            List<int> counts = arr.Map<int, string>((str)=>{
                return str.Length;
            });

            Assert.AreEqual(counts.Count, arr.Length);
            Assert.AreEqual(counts[0], 5);
            Assert.AreEqual(counts[1], 4);
            Assert.AreEqual(counts[2], 2);
            Assert.AreEqual(counts[3], 1);
            Assert.AreEqual(counts[4], 4);
        }
    }
}
