//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using blacktriangles;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace blacktriangles.Testing
{
    [TestFixture]
    public class TestColorExtension
    {
        //
        // types //////////////////////////////////////////////////////////////
        //
       
        //
        // tests ///////////////////////////////////////////////////////////////
        //

        [Test]
        public void FromHexString()
        {
            // a bad parse returns black
            Color badParse = ColorExtension.FromHexString("JKL;");
            Assert.AreEqual(badParse, Color.black);

            // a bad parse is one that doesn't start iwth a #
            badParse = ColorExtension.FromHexString("00112233");
            Assert.AreEqual(badParse, Color.black);

            // or one that doesn't have enough numbers
            badParse = ColorExtension.FromHexString("00112");
            Assert.AreEqual(badParse, Color.black);

            // a is assumed to be 1.0 unless otherwise specified
            Color check0 = ColorExtension.FromHexString("#ff0000");
            Assert.That(check0.r.IsApproximately(1.0f));
            Assert.That(check0.g.IsApproximately(0.0f));
            Assert.That(check0.b.IsApproximately(0.0f));
            Assert.That(check0.a.IsApproximately(1.0f));

            Color check1 = ColorExtension.FromHexString("#ff00ff00");
            Assert.That(check1.r.IsApproximately(1.0f));
            Assert.That(check1.g.IsApproximately(0.0f));
            Assert.That(check1.b.IsApproximately(1.0f));
            Assert.That(check1.a.IsApproximately(0.0f));

            Color check2 = ColorExtension.FromHexString("#11223344");
            Assert.That((check2.r*255.0f).IsApproximately(0x11));
            Assert.That((check2.g*255.0f).IsApproximately(0x22));
            Assert.That((check2.b*255.0f).IsApproximately(0x33));
            Assert.That((check2.a*255.0f).IsApproximately(0x44));
        }

        //
        // --------------------------------------------------------------------
        //

        [Test]
        public void ToHexString()
        {
            Color check1 = new Color(0.0f, 0.0f, 0.0f);
            Assert.AreEqual(check1.ToHexString(), "#000000");

            Color check2 = new Color(1.0f, 0.0f, 1.0f, 0.0f);
            Assert.AreEqual(check2.ToHexString(), "#FF00FF");

            Color check3 = new Color(0xAA/255.0f, 0xBB/255.0f, 0xCC/255.0f, 0xDD/255.0f);
            Assert.AreEqual(check3.ToHexString(), "#AABBCC");

            Color check4 = new Color(0x0A/255.0f, 0x90/255.0f, 0x08/255.0f, 0x70/255.0f);
            Assert.AreEqual(check4.ToHexString(), "#0A9008");
        }

        //
        // --------------------------------------------------------------------
        //

        [Test]
        public void ToHexStringRGBA()
        {
            Color check1 = new Color(0.0f, 0.0f, 0.0f);
            Assert.AreEqual(check1.ToHexStringRGBA(), "#000000FF");

            Color check2 = new Color(1.0f, 0.0f, 1.0f, 0.0f);
            Assert.AreEqual(check2.ToHexStringRGBA(), "#FF00FF00");

            Color check3 = new Color(0xAA/255.0f, 0xBB/255.0f, 0xCC/255.0f, 0xDD/255.0f);
            Assert.AreEqual(check3.ToHexStringRGBA(), "#AABBCCDD");

            Color check4 = new Color(0x0A/255.0f, 0x90/255.0f, 0x08/255.0f, 0x70/255.0f);
            Assert.AreEqual(check4.ToHexStringRGBA(), "#0A900870");
        }       
    }
}
