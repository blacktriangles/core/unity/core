//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace blacktriangles.Testing
{
    [TestFixture]
    public class TestHashSetExtension
    {
        // tests ///////////////////////////////////////////////////////////////
        [Test]
        public void BasicTest()
        {
            HashSet<int> intSet = new HashSet<int>();
            Assert.That( intSet.ToArray().Length == 0 );

            intSet.Add( 1 );
            Assert.That( intSet.ToArray().Length == 1 );

            intSet.Add( 100 );
            Assert.That( intSet.ToArray().Length == 2 );

            intSet.Add( 50 );
            int[] array = intSet.ToArray();
            Assert.That( array.Length == 3 );
            Assert.That( array[0] == 1 );
            Assert.That( array[1] == 100 );
            Assert.That( array[2] == 50 );
        }
    }
}
