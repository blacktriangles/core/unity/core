//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using blacktriangles;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace blacktriangles.Testing
{
    [TestFixture]
    public class TestUniqueList
    {
        // tests ///////////////////////////////////////////////////////////////
        [Test]
        public void BasicTest()
        {
            var vec3set = new UniqueList<Vector3>();
            Assert.AreEqual(0, vec3set.Count);

            Vector3 a = Vector3.zero;
            int i1 = vec3set.Add(a);
            Assert.AreEqual(0, i1);
            Assert.AreEqual(1, vec3set.Count);
            Assert.AreEqual(a.x, vec3set[i1].x);

            int i2 = vec3set.Add(Vector3.zero);
            Assert.AreEqual(0, i2);
            Assert.AreEqual(i1, i2);
            Assert.AreEqual(1, vec3set.Count);
            Assert.AreEqual(a.y, vec3set[i2].y);
    
            Vector3 b = Vector3.one;
            int i3 = vec3set.Add(b);
            Assert.AreEqual(1, i3);
            Assert.AreNotEqual(i2, i3);
            Assert.AreEqual(2, vec3set.Count);
            Assert.AreEqual(b, vec3set[i3]);
        }
    }
}
