//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace blacktriangles.Testing
{
    [TestFixture]
    public class TestScratchPad
    {
        // tests ///////////////////////////////////////////////////////////////
        [Test]
        public void BasicTest()
        {
            Vector3 a = new Vector3(0f,1f,2f);
            Vector3 b = new Vector3(0f,1f,2f);

            Assert.AreEqual(a.GetHashCode(), b.GetHashCode());
        }
    }
}
