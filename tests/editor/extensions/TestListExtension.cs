//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace blacktriangles.Testing
{
    [TestFixture]
    public class TestListExtension
    {
        // tests ///////////////////////////////////////////////////////////////
        [Test]
        public void BasicTest()
        {
            List<int> intList = new List<int>();

            // a new list has no valid indicies
            Assert.That( intList.IsValidIndex(0) == false );
            Assert.That( intList.IsValidIndex(-1) == false );

            // an attempt to swap items that are not valid fails silently.
            intList.SwapItems( 1, 2 );

            // lets add an item
            intList.Add( 1 );

            // we now have one valid index
            Assert.That( intList.IsValidIndex(0) );
            Assert.That( intList.IsValidIndex(-1) == false );
            Assert.That( intList.IsValidIndex(1) == false );

            // lets add another item
            intList.Add( 2 );

            // after we swap indicies 0 and 1 they should be swapped
            intList.SwapItems( 0, 1 );
            Assert.That( intList.IsValidIndex(0) );
            Assert.That( intList.IsValidIndex(1) );
            Assert.That( intList[0] == 2 );
            Assert.That( intList[1] == 1 );

            // we can ensure a minimum length
            intList.SetMinimumLength( 3 );
            Assert.That( intList.Count == 3 );
            // the new items come in as default(T), in this case 0.
            Assert.That( intList[2] == 0 );

            intList.SetMinimumLength( 10 );
            Assert.That( intList.Count == 10 );
            Assert.That( intList[9] == 0 );

            // minimum length does nothing if it's already larget
            intList.SetMinimumLength( 5 );
            Assert.That( intList.Count == 10 );

            intList.SetMinimumLength( 0 );
            Assert.That( intList.Count == 10 );

            // this will never do anything
            intList.SetMinimumLength( -1 );
            Assert.That( intList.Count == 10 );

            // we can ensure a maximum length, removing items that go over
            intList.SetMaximumLength( 5 );
            Assert.That( intList.Count == 5 );

            // setting a maximum larger than the current length does nothing
            intList.SetMaximumLength( 100 );
            Assert.That( intList.Count == 5 );

            // setting a maximum less than or equal to 0 clears the list.
            intList.SetMaximumLength( 0 );
            Assert.That( intList.Count == 0 );

            // lets add a few items to test negative max length
            intList.AddRange( new int[]{ 1,2,3,4,5 } );
            Assert.That( intList.Count == 5 );

            // setting a maximum less than or equal to 0 clears the list.
            intList.SetMaximumLength( -100 );
            Assert.That( intList.Count == 0 );

            // set length will force a length of a certain size, adding or removing as necessary
            intList.SetLength( 100 );
            Assert.That( intList.Count == 100 );
            intList.SetLength( 100 );
            Assert.That( intList.Count == 100 );
            intList.SetLength( 99 );
            Assert.That( intList.Count == 99 );
            intList.SetLength( 101 );
            Assert.That( intList.Count == 101 );
            intList.SetLength( -1 );
            Assert.That( intList.Count == 0 );
        }
    }
}
