//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using blacktriangles.Network;
using NUnit.Framework;
using System.IO;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace blacktriangles.Testing
{
    [TestFixture]
    public class TestRawPacketBuilder
    {
        //
        // #####################################################################
        //

        [Test]
        public void Building()
        {
            // create a builder
            RawPacketBuilder builder = new RawPacketBuilder();

            // lets make some raw packets to put in and take out
            UTF8TextPacket[] packets = {
                new UTF8TextPacket("The quick brown fox"),
                new UTF8TextPacket("jumps over the lazy"),
                new UTF8TextPacket("dog")
            };

            // now lets pack them all into a byte list
            List<byte> bytes = new List<byte>();

            int expectedLength = 0;
            foreach(UTF8TextPacket packet in packets)
            {
                RawPacket raw = packet.Encode();
                byte[] b = raw.ToBytes();
                expectedLength += b.Length;
                bytes.AddRange(b);
            }

            Assert.AreEqual(bytes.Count, expectedLength);

            // now lets feed in some bytes, first lets test passing in a WHOLE
            // message
            {
                RawPacket raw = packets[0].Encode();
                builder.AddBytes(raw.ToBytes());
                RawPacket res = builder.GetRawPacket();

                Assert.That(res != null);
                Assert.AreEqual(res.header.id, raw.header.id);
                Assert.AreEqual(res.header.payloadSize, raw.header.payloadSize);
                Assert.AreEqual(res.payload.Length, res.header.payloadSize);
                Assert.AreEqual(res.payload.Length, raw.payload.Length);
                for(int i=0; i<res.payload.Length;++i)
                {
                    Assert.AreEqual(res.payload[i], raw.payload[i]);
                }

                res = builder.GetRawPacket();
                Assert.That(res == null);
            }

            // Lets feed in parts of a message
            {
                byte[] b = bytes.ToArray();
                UTF8TextPacket textPacket = new UTF8TextPacket();
                int offset = 0;
                builder.AddBytes(b, offset, 2);
                RawPacket res = builder.GetRawPacket();
                Assert.That(res == null);
                offset += 2;

                builder.AddBytes(b, offset, 33);
                res = builder.GetRawPacket();
                Assert.That(res != null);
                offset += 33;

                textPacket.Decode(res);
                Assert.AreEqual(textPacket.text, packets[0].text);

                Assert.That(builder.GetRawPacket() == null);

                builder.AddBytes(b, offset);
                res = builder.GetRawPacket();
                Assert.That(res != null);
                textPacket.Decode(res);
                Assert.AreEqual(textPacket.text, packets[1].text);

                res = builder.GetRawPacket();
                Assert.That(res != null);
                textPacket.Decode(res);
                Assert.AreEqual(textPacket.text, packets[2].text);

                Assert.That(builder.GetRawPacket() == null);
            }

            {
                // now lets feed it using a binary reader!
                UTF8TextPacket textPacket = new UTF8TextPacket();
                BinaryReader reader = new BinaryReader(new MemoryStream(bytes.ToArray()));
                builder.AddBytes(reader);

                RawPacket res = builder.GetRawPacket();
                textPacket.Decode(res);
                Assert.AreEqual(textPacket.text, packets[0].text);

                res = builder.GetRawPacket();
                textPacket.Decode(res);
                Assert.AreEqual(textPacket.text, packets[1].text);

                res = builder.GetRawPacket();
                textPacket.Decode(res);
                Assert.AreEqual(textPacket.text, packets[2].text);
            }
        }

    }
}
