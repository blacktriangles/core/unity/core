//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using blacktriangles.Network;
using NUnit.Framework;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace blacktriangles.Testing
{
    //
    // fixture ////////////////////////////////////////////////////////////////
    //

    [TestFixture]
    public class TestPacketRouter
    {
        //
        // tests //////////////////////////////////////////////////////////////
        //
        //
        // --------------------------------------------------------------------
        //

        [Test]
        public void TestRoute()
        {
            //
            // make a couple of locals for testing
            //

            TcpConnection fakeConn = new TcpConnection();
            string expected = "expected string";
            PacketRouter router = new PacketRouter();

            System.Action<string> SendPacket = (str)=>{
                UTF8TextPacket packet = new UTF8TextPacket(str);
                packet.receiver = fakeConn;
                router.Route(packet.Encode());
            };

            //
            // handler 1
            //

            int count1 = 0;
            System.Func<IConnection, UTF8TextPacket, bool> handler = (conn, packet)=>{
                ++count1;
                Assert.AreEqual(expected, packet.text);
                return true;
            };

            //
            // handler 2
            //

            int count2 = 0;
            System.Func<IConnection, UTF8TextPacket, bool> handler2 = (conn, packet)=>{
                ++count2;
                Assert.AreEqual(fakeConn, conn);
                Assert.AreEqual(expected, packet.text);
                return true;
            };

            //
            // register handler
            //

            router.AddRoute(handler);

            //
            // route packet, our handler should have been called once
            //
            Assert.AreEqual(0, count1);
            Assert.AreEqual(0, count2);

            SendPacket("expected string");
            
            Assert.AreEqual(1, count1);
            Assert.AreEqual(0, count2);

            //
            // if we register our handler again, we should still only get called once
            //

            expected = "foobar";
            router.AddRoute(handler);
            Assert.AreEqual(1, count1);
            Assert.AreEqual(0, count2);

            SendPacket("foobar");

            Assert.AreEqual(2, count1);
            Assert.AreEqual(0, count2);

            //
            // register second handler
            //

            router.AddRoute(handler2);
            Assert.AreEqual(2, count1);
            Assert.AreEqual(0, count2);

            SendPacket("foobar");

            Assert.AreEqual(3, count1);
            Assert.AreEqual(1, count2);

            //
            // can multiple register with no side effects
            //

            router.AddRoute(handler2);

            expected = "third times a charm";
            SendPacket("third times a charm");

            Assert.AreEqual(4, count1);
            Assert.AreEqual(2, count2);

            //
            // we can remove a handler and it no longer gets called
            //

            router.RemoveRoute(handler);

            expected = "sophieisadog";
            SendPacket("sophieisadog");

            Assert.AreEqual(4, count1);
            Assert.AreEqual(3, count2);

            //
            // we can remove a handler multiple times with no side effects
            //

            router.RemoveRoute(handler);

            expected = "test";
            SendPacket("test");
            Assert.AreEqual(4, count1);
            Assert.AreEqual(4, count2);

            //
            // we can remove all handlers
            //

            router.RemoveRoute(handler2);
            expected = "test2";
            SendPacket("test2");
            Assert.AreEqual(4, count1);
            Assert.AreEqual(4, count2);

            //
            // empty handlers do no throw exceptions
            //

            router.RemoveRoute(handler); router.RemoveRoute(handler2);
            expected = "test3";
            SendPacket("test3");
            Assert.AreEqual(4, count1);
            Assert.AreEqual(4, count2);
        }

        //
        // end class //////////////////////////////////////////////////////////
        //
    }
}
 
