//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace blacktriangles.Testing
{
    [TestFixture]
    public class TestJsonObject
    {
        // tests ///////////////////////////////////////////////////////////////
        [Test]
        public void RunJsonObjectTests()
        {
            // here is how we expect you to interact with JSON mostly.

            // create a class and a new JsonObject
            MyClass myClass = new MyClass( "Name", 1234, 5.67f );
            JsonObject json = new JsonObject();

            // you don't need to call ToJson if your class inherits from IJsonSerializable
            json["myClass"] = myClass;

            // nor do you need to call ToJson for Arrays
            json["arr"] = new int[] { 1, 2, 4, 8, 16 };

            // nor do you need to convert IList
            List<bool> bools = new List<bool>();
            bools.Add( true );
            bools.Add( true );
            bools.Add( false );
            bools.Add( true );
            json["list"] = bools;

            // nor do you need to convert dictionaries
            Dictionary< int, string > dict = new Dictionary< int, string >();
            dict[0] = "Zero";
            dict[1] = "One";
            dict[2] = "Two";
            json["dict"] = dict;

            // you can use enums as keys, and classes as values
            Dictionary< MyEnum, MyClass > dict2 = new Dictionary< MyEnum, MyClass >();
            dict2[ MyEnum.MyEnumValue1 ] = new MyClass( "First", 1, 2.3f );
            dict2[ MyEnum.MyEnumValue2 ] = new MyClass( "Second", 4, 5.6f );
            dict2[ MyEnum.MyEnumValue3 ] = new MyClass( "Third", 7, 8.9f );
            json["dict2"] = dict2;

            ///###hsmith $NOTE the get extension methods broke?
            // nor do you need to call ToJson if you have an extension method for this class
            // Vector3 origVector3 = new Vector3( 10f, 25f, 56f );
            // json["vec"] = origVector3;

            // this will change, but right now you need ot use the ToJson extension for Vector3s
            json["vector3"] = new Vector3( 123,456, 789 ).ToJson();

            // same for quaternions
            json["quaternion"] = new Quaternion( 4,3,2,1 ).ToJson();

            // we also support enums (as strings)
            json["singleEnum"] = MyEnum.MyEnumValue3;
            json["enum"] = new MyEnum[] { MyEnum.MyEnumValue1, MyEnum.MyEnumValue3, MyEnum.MyEnumValue2 };

            // and feel free to pass in null, we got you covered.
            json["none"] = null;

            // you can also use value types
            json["int"] = 1337;
            json["negint"] = -1337;
            json["float"] = 3.14f;
            json["double"] = 19.85;

            // you can convert it to a json string using ToString
            string str = json.ToString();

            // or you can convert it using the JsonSerializer static method
            string sameStr = JsonSerializer.Serialize( json );

            // they work out to be the same
            Assert.That( str == sameStr );

            // you can get another json object from this string
            JsonObject obj = JsonObject.FromString( str );

            // an object you hand craft, and one that is made from a string are NOT equal
            // the one you hand create still has references to the original objects you stored.
            // this means that the dictionary we put into json is still a dictionary.
            Assert.That( json["dict"].GetType() == dict.GetType() );

            // but the one we just serialized from string, it got converted into a JsonObject
            Assert.That( obj["dict"].GetType() != dict.GetType() );
            Assert.That( obj["dict"].GetType() == typeof( JsonObject ) );

            // similarly, while we may have put a list of bools in
            Assert.That( json["list"].GetType() == bools.GetType() );

            // the version we built from the string stores it as a typed fixed array
            Assert.That( obj["list"].GetType().IsArray );

            // also that custom object we put in MyClass? It's still an instance of MyClass in the first object
            Assert.That( json["myClass"].GetType() == typeof( MyClass ) );

            // but in the version built from the string, it's another JsonObject
            Assert.That( obj["myClass"].GetType() == typeof( JsonObject ) );

            // basically we don't convert these objects when you put them in
            // but when we parse a string we only create types of:
            //  - System.Int64
            //  - System.Double
            //  - string
            //  - object[]
            //  - JsonObject
            // these are the only types that are returned by the indexing operator []
            // on parsed objects.

            // We haven't really come up with a use case for this, but in case this
            // discrepancy is a problem in the future ( the fact that a JsonObject
            // made from a string is different from one that you packed yourself )
            // there is a NormalizeObject static method.
            JsonObject normObj = JsonObject.NormalizeObject( json );

            // the original object passed in remains unchanged, but the result is
            // a JsonObject where all it's fields are normalized to one of the
            // natively supported types (listed above).
            Assert.That( normObj.ToString() == json.ToString() );
            Assert.That( normObj["list"].GetType() != json["list"].GetType() );
            Assert.That( normObj["list"].GetType() == obj["list"].GetType() );

            // This shouldn't matter for you or your consumers, especially since
            // we've added the GetField function.


            // If we use GetField and tell it we want a MyClass object out, it
            // knows to instantiate and call FromJson on it with the correct field
            MyClass newClass = obj.GetField<MyClass>( "myClass" );
            Assert.That( newClass != null );

            // we can see all the fields got properly deserialized
            Assert.That( newClass.Check( myClass ) );

            // if we query for a value that doesn't exist, we get a default value
            // for that type.  Refer to C# default keyword to know how this will
            // impact the types you are using.
            MyClass nullClass = obj.GetField<MyClass>( "noval" );
            Assert.That( nullClass == default(MyClass) );

            int defaultInt = obj.GetField<int>( "noval" );
            Assert.That( defaultInt == default(System.Int32) );

            // if we try and convert a field to a type that wasn't originally used
            // to serialize the string, we do the best we can to fulfill that request
            nullClass = obj.GetField<MyClass>( "dict" );

            // nullClass isn't null here, because dict was a valid key, we
            // located that value, and passed it to nullClass's FromJson function
            Assert.That( nullClass != null );

            // it didn't find any keys it was looking for, so it's basically just
            // a default constructed MyClass object.
            MyClass defaultMyClass = new MyClass();
            Assert.That( nullClass.Check( defaultMyClass ) );

            // here's an interesting side effect, lets take a look at SomeOtherClass
            SomeOtherClass anotherClass = obj.GetField<SomeOtherClass>( "myClass" );

            // SomeOtherClass has a string name, and an int age, and the from and to
            // json methods save and pull those values from keys "string" and "int"
            // Because of this, anotherClass could pull the data originally written
            // by a myClass object.
            Assert.That( anotherClass.name == myClass.classString );
            Assert.That( anotherClass.age == myClass.classInt );

            // keep this in mind! It's all about the keys used and the ToJson and
            // FromJson methods.  It'll work both ways too! We often use this
            // to support cross serializing polymorphic types, but as in this
            // example case, the classes don't even need to inherit from one
            // another.

            // you can extend classes to be serialized even if you can't modify the
            // class directly by using extension methods.  We did one for
            // UnityEngine.Vector3.  You saw it go in, here it comes out.
            ///###hsmith $BROKEN ???
            // Vector3 newVec = obj.GetField<Vector3>( "vec" );
            // Assert.That( newVec == origVector3 );

            ///###hsmith $BROKEN this broke at some point, I think the getextension methods.
            // X it's important to keep in mind that if you are extending a ValueType
            // X FromJson needs to use a different function signature than when you
            // X are extending a Reference Type. This is because the extension function
            // X can't pass the "this" parameter by reference!  If this doesn't make sense the
            // X safest route for your extension methods is to use this function
            // X signature for the FromJson method:
            // X        public static [YOUR_TYPE] FromJson( this [YOUR_TYPE] vec, JsonObject json )
            // X the JsonObject will realize you're requesting a value type, and
            // X use assignment to set the deserialized value.
            // X See: JsonSerializerExtensions.cs for examples.

            // this same limitation applies to your own structs so you can't
            // inherit from IJsonSerializable just yet!  This is on the roadmap
            // to be fixed, and likely be to change the function signature of
            // FromJson to return the object, or to take a reference to the
            // object?  We don't really like either of those options, so for
            // now, we only support IJsonSerializable Reference Types, and
            // using extension methods when you have to use a struct.

            // you can get type safe arrays out as well.
            int[] items = obj.GetField<int[]>( "arr" );
            Assert.That( items.Length == 5 );
            Assert.That( items[0] == 1 );
            Assert.That( items[1] == 2 );
            Assert.That( items[2] == 4 );
            Assert.That( items[3] == 8 );
            Assert.That( items[4] == 16 );

            // lists also become typed fixed arrays
            bool[] bArray = obj.GetField<bool[]>( "list" );
            Assert.That( bArray.Length == bools.Count );
            Assert.That( bArray[0] == bools[0] );
            Assert.That( bArray[1] == bools[1] );
            Assert.That( bArray[2] == bools[2] );
            Assert.That( bArray[3] == bools[3] );

            // dictionaries become JsonObjects, where the Keys are converted to strings
            JsonObject dictJson = obj.GetField<JsonObject>( "dict" );
            Assert.That( dictJson.GetField<string>( "0" ) == "Zero" );
            Assert.That( dictJson.GetField<string>( "1" ) == "One" );
            Assert.That( dictJson.GetField<string>( "2" ) == "Two" );

            // if you used an enum, these are stringified as well
            JsonObject dict2Json = obj.GetField<JsonObject>( "dict2" );
            MyClass dict2Json0 = dict2Json.GetField<MyClass>( "MyEnumValue1" );
            Assert.That( dict2Json0 != null );
            Assert.That( dict2Json0.classString == "First" );

            // you can use ToString to get the right key
            MyClass dict2Json1 = dict2Json.GetField<MyClass>( MyEnum.MyEnumValue2.ToString() );
            Assert.That( dict2Json1 != null );
            Assert.That( dict2Json1.classString == "Second" );

            // vector3s ToJson comes back as JsonObjects, with X,Y,Z encoded as fields
            JsonObject vec3Json = obj.GetField<JsonObject>( "vector3" );
            Assert.That( vec3Json.GetField<float>( "x" ) == 123f );
            Assert.That( vec3Json.GetField<float>( "y" ) == 456f );
            Assert.That( vec3Json.GetField<float>( "z" ) == 789f );

            // you can also decode them using the FromJson extension
            // I'm going to eventually make this auto handle through the regular API
            Vector3 testVec3 = Vector3JsonExtension.FromJson( vec3Json );
            Assert.That( testVec3.x == 123f );
            Assert.That( testVec3.y == 456f );
            Assert.That( testVec3.z == 789f );

            // quaternions using ToJson also get packed as json objects
            // I'm going to eventually make this auto handle through the regular API
            JsonObject quatJson = obj.GetField<JsonObject>( "quaternion" );
            Assert.That( quatJson.GetField<float>( "x" ) == 4f );
            Assert.That( quatJson.GetField<float>( "y" ) == 3f );
            Assert.That( quatJson.GetField<float>( "z" ) == 2f );
            Assert.That( quatJson.GetField<float>( "w" ) == 1f );

            // and a useful FromJson method also exist on QuaternionExtension
            Quaternion quat = QuaternionJsonExtension.FromJson( quatJson );
            Assert.That( quat.x == 4f );
            Assert.That( quat.y == 3f );
            Assert.That( quat.z == 2f );
            Assert.That( quat.w == 1f );

            // we store enums as strings, not as integers to prevent invalidating json
            // due to changes in the enumeration.  Just be aware when you rename or remove enum values!
            MyEnum myEnum = obj.GetField<MyEnum>( "singleEnum" );
            Assert.That( myEnum == MyEnum.MyEnumValue3 );

            MyEnum[] enums = obj.GetField<MyEnum[]>( "enum" );
            Assert.That( enums[0] == MyEnum.MyEnumValue1 );
            Assert.That( enums[1] == MyEnum.MyEnumValue3 );
            Assert.That( enums[2] == MyEnum.MyEnumValue2 );

            // we also support nulls.
            Assert.That( obj["none"] == null );

            // if you try and get an object that was set as null, well, you get null.
            MyClass noneClass = obj.GetField<MyClass>( "none" );
            Assert.That( noneClass == null );

            // if you try and use null on an object that can't be null, you get
            // the default version of that value.
            int noneInt = obj.GetField<int>( "none" );
            Assert.That( noneInt == default(int) );

            // if you want to make sure a key is there (or not), just use the ContainsKey function
            Assert.That( obj.ContainsKey( "none" ) );
            Assert.That( obj.ContainsKey( "There is no key such as this" ) == false );

            // don't forget those basic types
            Assert.That( obj.GetField<int>("int") == 1337 );
            Assert.That( obj.GetField<int>("negint") == -1337 );
            Assert.That( obj.GetField<float>("float").IsApproximately( 3.14f ) );
            Assert.That( obj.GetField<double>("double").IsApproximately( 19.85 ) );

            obj.GetField<float>("float2");

            // HTH!
            //    - Howard
        }

        [Test]
        public void RunSafeSerializeTests()
        {
            // sometimes you want to inline serialization or deserialization of an
            // IJsonSerializable object, but doing so safely can be a little verbose.
            MyClass myClass = new MyClass( "WannaBeSafe", 123, 987.654f );

            // ... some code later ... //
            JsonObject json = new JsonObject();
            if( myClass != null )
            {
                json = myClass.ToJson();
            }

            // when you have to do this for lots of classes in a ToJson function, it
            // can get pretty messy, pretty quick.  To make this easier, we've added
            // a SafeSerialize static function on the JsonObject class.
            json = null;
            json = JsonObject.SafeSerialize( myClass );
            Assert.That( json.ToString() == myClass.ToJson().ToString() );

            // if you give it a null object it will simply return a null JsonObject
            // which in most cases, is what you want to store in the json document
            // (the fact that it was null)
            JsonObject nullJson = JsonObject.SafeSerialize( null );
            Assert.That( nullJson == null );

            // similarly, for deserialization we offer two distinct methods.
            // one for converting from a json object
            JsonObject myClassJson = myClass.ToJson();
            MyClass myOtherClass = new MyClass( "Overwrite Me!", 0, 0f );
            JsonObject.SafeDeserialize( myOtherClass, myClassJson );
            Assert.That( myOtherClass.Check( myClass ) );

            // it won't throw exceptions if you pass it a null json object
            JsonObject.SafeDeserialize( myOtherClass, null );
            Assert.That( myOtherClass.Check( myClass ) );

            // it will do nothing if you pass it a null object to deserialize to as well
            JsonObject.SafeDeserialize( null, myClassJson );

            // we also have a signature that supports sub object, as is often the
            // case when deserializing json into a class which as members of IJsonSerializable
            JsonObject multiObj = new JsonObject();
            multiObj["myClass"] = myClass.ToJson();

            // here, we serialize from the "subobject" field of multiObj
            MyClass myThirdClass = new MyClass( "This will go away!", 0, 0f );
            JsonObject.SafeDeserialize( myThirdClass, multiObj, "myClass" );
            Assert.That( myThirdClass != null );
            Assert.That( myThirdClass.Check( myClass ) );

            // this will similarly not work if you pass it a null JsonObject
            JsonObject.SafeDeserialize( myThirdClass, null, "myClass" );
            Assert.That( myThirdClass != null );
            Assert.That( myThirdClass.Check( myClass ) );

            // or if you request a field that does not exist
            JsonObject.SafeDeserialize( myThirdClass, null, "i do not exist" );
            Assert.That( myThirdClass != null );
            Assert.That( myThirdClass.Check( myClass ) );

            // or if you try and deserialize to a null IJsonSerializable
            JsonObject.SafeDeserialize( null, multiObj, "myClass" );
            JsonObject.SafeDeserialize( null, multiObj, "i do not exist" );
        }

        [Test]
        public void RunParsingTests()
        {
            // this proves the internals work, but this isn't the expected usage!

            // basic key - value test works great.
            string json = "{ \"Hello\": \"World\" }";
            blacktriangles.JsonObject obj = blacktriangles.JsonObject.FromString( json );

            // you can use the array indexer (returns a System.Object)
            Assert.That( (string)obj[ "Hello" ] == "World" );

            // or you can use the GetField (returns a System.Object)
            Assert.That( (string)obj.GetField( "Hello" ) == "World" );

            // or you can use the generic GetField, which returns of the type requested
            Assert.That( obj.GetField<System.String>( "Hello" ) == "World" );

            // you can serialize compound objects as well, where values are objects
            json = "{ \"Nested\": { \"Inner\": \"Value\" }";
            obj = blacktriangles.JsonObject.FromString( json );
            Assert.That( obj.GetField<JsonObject>( "Nested" ).GetField<System.String>( "Inner" ) == "Value" );

            // you can serialize array types as well
            json = "{ \"Array\": [ { \"Name\": \"Object1\" }, { \"Name\": \"Object2\" } ] }";
            obj = blacktriangles.JsonObject.FromString( json );
            object[] arr = obj.GetField<object[]>( "Array" );

            foreach( JsonObject innerObj in arr )
            {
                Assert.That( innerObj.GetField<System.String>( "Name" ).StartsWith( "Object" ) );
            }

            // you can serialize number values as well (they are internally stored as Int64 and Doubles accordingly)
            json = "{ \"Number1\": 10, \"Number2\": 3.14 }";
            obj = blacktriangles.JsonObject.FromString( json );

            // even though they are int64 internally, you can get them as any convertable type
            Assert.That( obj.GetField<int>("Number1") == 10 );
            Assert.That( obj.GetField<short>("Number1") == 10 );
            Assert.That( obj.GetField<long>("Number1" ) == 10 );
            Assert.That( obj.GetField<uint>("Number1" ) == 10 );

            // real numbers are suported as well in much the same way.
            double dEpsilon = 0.001;
            float fEpsilon = 0.001f;
            decimal mEpsilon = 0.001m;

            Assert.That( System.Math.Abs( obj.GetField<double>( "Number2" ) - 3.14 ) < dEpsilon );
            Assert.That( System.Math.Abs( obj.GetField<decimal>( "Number2" ) - 3.14m ) < mEpsilon );
            Assert.That( btMath.Abs( obj.GetField<float>( "Number2" ) - 3.14f ) < fEpsilon );

            ///###hsmith $TODO this is broken right now, I think it has to do with the get extension methods method
            // you can write extensions to work with special objects intuitively, like this Vector3
            // json = "{ \"vec\": { \"x\": 100, \"y\": 200, \"z\": 300 } }";
            // obj = blacktriangles.JsonObject.FromString( json );
            // Vector3 vec3 = obj.GetField<Vector3>( "vec" );
            // Assert.That( vec3 == new Vector3( 100f, 200f, 300f ) );
        }
    }

    //
    // test types ##############################################################
    //
    public class MyClass
        : IJsonSerializable
    {
        public string classString;
        public int classInt;
        public float classFloat;

        // require a default constructor for automagic Json serialization
        public MyClass()
        {
        }

        public MyClass( string str, int i, float f )
        {
            classString = str;
            classInt = i;
            classFloat = f;
        }

        public JsonObject ToJson()
        {
            JsonObject result = new JsonObject();
            result["string"] = classString;
            result["int"] = classInt;
            result["float"] = classFloat;
            return result;
        }

        public void FromJson( JsonObject json )
        {
            classString = json.GetField<string>( "string" );
            classInt = json.GetField<int>( "int" );
            classFloat = json.GetField<float>( "float" );
        }

        public bool Check( MyClass other )
        {
            return ( classString == other.classString )
                    && ( classInt == other.classInt )
                    && ( classFloat == other.classFloat );
        }
    }

    public class SomeOtherClass
        : IJsonSerializable
    {
        public string name;
        public int age;

        public void FromJson( JsonObject json )
        {
            name = json.GetField<string>( "string" );
            age = json.GetField<int>( "int" );
        }

        public JsonObject ToJson()
        {
            JsonObject result = new JsonObject();
            result["string"] = name;
            result["int"] = age;
            return result;
        }
    }

    public enum MyEnum { MyEnumValue1, MyEnumValue2, MyEnumValue3 };

}
