//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace blacktriangles.Testing
{
    [TestFixture]
    public class TestArgumentParser
    {
        // members /////////////////////////////////////////////////////////////

        // tests ///////////////////////////////////////////////////////////////
        [Test]
        public void BasicTest()
        {
            ArgumentParser parser = new ArgumentParser( new string[] { "app", "-test", "value1", "value2", "--test2", "value3", "-test3", "-test", "value4", "value5", "value6" } );

            // we only parse arguments taht start with a dash
            Assert.That( parser.HasArgument( "app" ) == false );

            // arguments have leading dashes stripped
            Assert.That( parser.HasArgument( "test" ) );

            // double dash is acceptable
            Assert.That( parser.HasArgument( "test2" ) );
            Assert.That( parser.HasArgument( "test3" ) );

            // this was never added
            Assert.That( parser.HasArgument( "test4" ) == false );

            Assert.That( parser.GetValues( "test" ) != null );
            Assert.That( parser.GetValues( "test" ).Count == 3 );
            Assert.That( parser.GetValues( "test" )[0] == "value4" );
            Assert.That( parser.GetValues( "test" )[1] == "value5" );
            Assert.That( parser.GetValues( "test" )[2] == "value6" );

            Assert.That( parser.GetValues( "test2" ).Count == 1 );
            Assert.That( parser.GetValues( "test2" )[0] == "value3" );

            Assert.That( parser.GetValues( "test3" ).Count == 0 );
        }

        [Test]
        public void DynamicArgumentParseTest()
        {
            ArgumentParser parser = new ArgumentParser( new string[] {
                    "app",
                    "-test", "hello",
                    "-test2", "1.34", "4.31",
                    "-test3", "1337",
                    "-test4", "true",
                    "-test5", "TRUE",
                    "-test6", "false",
                    "-test7", "FALSE",
                });

            Assert.That( parser.GetValue<string>( "test", "FALSE" ) == "hello" );
            Assert.That( parser.GetValue<float>( "test2", 500.0f ).IsApproximately( 1.34f ) );
            Assert.That( parser.GetValue<double>( "test2", 500.0 ).IsApproximately( 1.34 ) );
            Assert.That( parser.GetValue<int>( "test3", 1337 ) == 1337 );
            Assert.That( parser.GetValue<bool>( "test4", false ) == true );
            Assert.That( parser.GetValue<bool>( "test5", false ) == true );
            Assert.That( parser.GetValue<bool>( "test6", true ) == false );
            Assert.That( parser.GetValue<bool>( "test7", true ) == false );

            //invalid types throw an exception
            bool success = false;
            try
            {
                parser.GetValue<List<string>>( "test12", null );
            }
            catch( System.ArgumentException )
            {
                success = true;
            }
            Assert.That( success );

        }
    }
}
