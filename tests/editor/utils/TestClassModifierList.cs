//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using blacktriangles;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace blacktriangles.Testing
{
    [TestFixture]
    public class TestClassModifierList
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        public struct MyStruct
        {
            public int integer;
            public float flt;
            public double dbl;
            public string nomod;
        }

        //
        // --------------------------------------------------------------------
        //

        public class MyStructModifierList
            : ClassModifierList<MyStruct>
        {
        }

        //
        // tests ///////////////////////////////////////////////////////////////
        //

        [Test]
        public void BasicTest()
        {
            MyStruct s1 = new MyStruct() {
                integer = 0,
                flt = 1f,
                dbl = 2.0,
                nomod = "cannot change",
            };

            MyStructModifierList list = new MyStructModifierList();

            //
            // integer add ----------------------------------------------------
            //
            
            list.AddModifier(new ClassModifier() {
                field = list.FindField("integer"),
                type = ClassModifierType.Add,
                modifier = 1f,
            });

            MyStruct s2 = list.Apply(s1);

            Assert.AreNotEqual(s2.integer, s1.integer);
            Assert.AreEqual(s2.integer, 1);

            //
            // float subtract -------------------------------------------------
            //

            list.AddModifier(new ClassModifier() {
                field = list.FindField("flt"),
                type = ClassModifierType.Subtract,
                modifier = 3f,
            });

            s2 = list.Apply(s1);
            Assert.AreEqual(s2.integer, 1);
            Assert.AreNotEqual(s2.flt, s1.flt);
            Assert.AreEqual(s2.flt, -2f);

            //
            // double multiply ------------------------------------------------
            //

            list.AddModifier(new ClassModifier() {
                field = list.FindField("dbl"),
                type = ClassModifierType.Multiply,
                modifier = 2.0f,
            });

            s2 = list.Apply(s1);
            Assert.AreNotEqual(s2.dbl, s1.dbl);
            Assert.AreEqual(s2.dbl, 4.0);

            //
            // float divide ---------------------------------------------------
            //

            list.AddModifier(new ClassModifier() {
                field = list.FindField("flt"),
                type = ClassModifierType.Divide,
                modifier = 2f
            });

            s2 = list.Apply(s1);
            Assert.AreEqual(s2.flt, -1.0f);

            //
            // float set ------------------------------------------------------
            //

            list.AddModifier(new ClassModifier() {
                field = list.FindField("flt"),
                type = ClassModifierType.Set,
                modifier = 1337f,
            });

            s2 = list.Apply(s1);
            Assert.AreEqual(s2.flt, 1337f);

            //
            // string multiply ------------------------------------------------
            // (we expect this to throw a format exception)

            list.AddModifier(new ClassModifier() {
                field = list.FindField("nomod"),
                type = ClassModifierType.Multiply,
                modifier = 2.0f,
            });

            Assert.Throws(typeof(System.FormatException), ()=>{
                s2 = list.Apply(s2);
            });
        }
    }
}
