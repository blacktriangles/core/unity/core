//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using blacktriangles;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace blacktriangles.Testing
{
    [TestFixture]
    public class TestCardinalDirection
    {
        // tests ///////////////////////////////////////////////////////////////
        [Test]
        public void InverseTest()
        {
            Assert.That( CardinalDirectionUtils.Inverse(CardinalDirection.North), Is.EqualTo(CardinalDirection.South));
            Assert.That( CardinalDirectionUtils.Inverse(CardinalDirection.South), Is.EqualTo(CardinalDirection.North));
            Assert.That( CardinalDirectionUtils.Inverse(CardinalDirection.East), Is.EqualTo(CardinalDirection.West));
            Assert.That( CardinalDirectionUtils.Inverse(CardinalDirection.West), Is.EqualTo(CardinalDirection.East));
        }

        [Test]
        public void RotateTest()
        {
            // rotate once
            Assert.That( CardinalDirectionUtils.RotateClockwise(CardinalDirection.North), Is.EqualTo(CardinalDirection.East));
            Assert.That( CardinalDirectionUtils.RotateClockwise(CardinalDirection.North, 0), Is.EqualTo(CardinalDirection.North));
            Assert.That( CardinalDirectionUtils.RotateClockwise(CardinalDirection.North, 1), Is.EqualTo(CardinalDirection.East));
            Assert.That( CardinalDirectionUtils.RotateClockwise(CardinalDirection.North, 2), Is.EqualTo(CardinalDirection.South));
            Assert.That( CardinalDirectionUtils.RotateClockwise(CardinalDirection.North, 3), Is.EqualTo(CardinalDirection.West));
            Assert.That( CardinalDirectionUtils.RotateClockwise(CardinalDirection.North, 4), Is.EqualTo(CardinalDirection.North));
            Assert.That( CardinalDirectionUtils.RotateClockwise(CardinalDirection.North, 5), Is.EqualTo(CardinalDirection.East));
            Assert.That( CardinalDirectionUtils.RotateClockwise(CardinalDirection.North, 6), Is.EqualTo(CardinalDirection.South));
            Assert.That( CardinalDirectionUtils.RotateClockwise(CardinalDirection.North, 7), Is.EqualTo(CardinalDirection.West));
            Assert.That( CardinalDirectionUtils.RotateClockwise(CardinalDirection.North, 8), Is.EqualTo(CardinalDirection.North));

            Assert.That( CardinalDirectionUtils.RotateClockwise(CardinalDirection.East), Is.EqualTo(CardinalDirection.South));
            Assert.That( CardinalDirectionUtils.RotateClockwise(CardinalDirection.East, 0), Is.EqualTo(CardinalDirection.East));
            Assert.That( CardinalDirectionUtils.RotateClockwise(CardinalDirection.East, 1), Is.EqualTo(CardinalDirection.South));
            Assert.That( CardinalDirectionUtils.RotateClockwise(CardinalDirection.East, 2), Is.EqualTo(CardinalDirection.West));
            Assert.That( CardinalDirectionUtils.RotateClockwise(CardinalDirection.East, 3), Is.EqualTo(CardinalDirection.North));
            Assert.That( CardinalDirectionUtils.RotateClockwise(CardinalDirection.East, 4), Is.EqualTo(CardinalDirection.East));
            Assert.That( CardinalDirectionUtils.RotateClockwise(CardinalDirection.East, 5), Is.EqualTo(CardinalDirection.South));
            Assert.That( CardinalDirectionUtils.RotateClockwise(CardinalDirection.East, 6), Is.EqualTo(CardinalDirection.West));
            Assert.That( CardinalDirectionUtils.RotateClockwise(CardinalDirection.East, 7), Is.EqualTo(CardinalDirection.North));
            Assert.That( CardinalDirectionUtils.RotateClockwise(CardinalDirection.East, 8), Is.EqualTo(CardinalDirection.East));

            Assert.That( CardinalDirectionUtils.RotateClockwise(CardinalDirection.South), Is.EqualTo(CardinalDirection.West));
            Assert.That( CardinalDirectionUtils.RotateClockwise(CardinalDirection.South, 0), Is.EqualTo(CardinalDirection.South));
            Assert.That( CardinalDirectionUtils.RotateClockwise(CardinalDirection.South, 1), Is.EqualTo(CardinalDirection.West));
            Assert.That( CardinalDirectionUtils.RotateClockwise(CardinalDirection.South, 2), Is.EqualTo(CardinalDirection.North));
            Assert.That( CardinalDirectionUtils.RotateClockwise(CardinalDirection.South, 3), Is.EqualTo(CardinalDirection.East));
            Assert.That( CardinalDirectionUtils.RotateClockwise(CardinalDirection.South, 4), Is.EqualTo(CardinalDirection.South));
            Assert.That( CardinalDirectionUtils.RotateClockwise(CardinalDirection.South, 5), Is.EqualTo(CardinalDirection.West));
            Assert.That( CardinalDirectionUtils.RotateClockwise(CardinalDirection.South, 6), Is.EqualTo(CardinalDirection.North));
            Assert.That( CardinalDirectionUtils.RotateClockwise(CardinalDirection.South, 7), Is.EqualTo(CardinalDirection.East));
            Assert.That( CardinalDirectionUtils.RotateClockwise(CardinalDirection.South, 8), Is.EqualTo(CardinalDirection.South));


            Assert.That( CardinalDirectionUtils.RotateClockwise(CardinalDirection.West), Is.EqualTo(CardinalDirection.North));
            Assert.That( CardinalDirectionUtils.RotateClockwise(CardinalDirection.West, 0), Is.EqualTo(CardinalDirection.West));
            Assert.That( CardinalDirectionUtils.RotateClockwise(CardinalDirection.West, 1), Is.EqualTo(CardinalDirection.North));
            Assert.That( CardinalDirectionUtils.RotateClockwise(CardinalDirection.West, 2), Is.EqualTo(CardinalDirection.East));
            Assert.That( CardinalDirectionUtils.RotateClockwise(CardinalDirection.West, 3), Is.EqualTo(CardinalDirection.South));
            Assert.That( CardinalDirectionUtils.RotateClockwise(CardinalDirection.West, 4), Is.EqualTo(CardinalDirection.West));
            Assert.That( CardinalDirectionUtils.RotateClockwise(CardinalDirection.West, 5), Is.EqualTo(CardinalDirection.North));
            Assert.That( CardinalDirectionUtils.RotateClockwise(CardinalDirection.West, 6), Is.EqualTo(CardinalDirection.East));
            Assert.That( CardinalDirectionUtils.RotateClockwise(CardinalDirection.West, 7), Is.EqualTo(CardinalDirection.South));
            Assert.That( CardinalDirectionUtils.RotateClockwise(CardinalDirection.West, 8), Is.EqualTo(CardinalDirection.West));
        }

        [Test]
        public void ToDirectionTest()
        {
            Assert.That( CardinalDirectionUtils.ToDirectionXZ(CardinalDirection.North), Is.EqualTo(Vector3.forward));
            Assert.That( CardinalDirectionUtils.ToDirectionXZ(CardinalDirection.South), Is.EqualTo(Vector3.back));
            Assert.That( CardinalDirectionUtils.ToDirectionXZ(CardinalDirection.East), Is.EqualTo(Vector3.right));
            Assert.That( CardinalDirectionUtils.ToDirectionXZ(CardinalDirection.West), Is.EqualTo(Vector3.left));
           
        }
    }
}
