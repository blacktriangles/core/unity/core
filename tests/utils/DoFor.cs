//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using System.Collections;
using UnityEngine;

namespace blacktriangles.Testing
{
    public static partial class Utils
    {
        //
        // Waits for a condition, or until a timeout occurs, triggers a warning
        // if it was the timeout.
        //

        public static IEnumerator DoFor(System.Action act, double timeout)
        {
            double endtime = Epoch.now + timeout;
            while(endtime > Epoch.now) 
            {
                act();
                yield return new WaitForSeconds(0.1f);
            }
        }
    }
}
