//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using blacktriangles.Network;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using UnityEngine.TestTools;

namespace blacktriangles.Testing
{
    [TestFixture]
    public class TestTcpClient
    {
        //
        // constants //////////////////////////////////////////////////////////
        //

        private static readonly int kHostPort                   = 30000;
        private static readonly byte kSessionType               = 1;


        //
        // members ////////////////////////////////////////////////////////////
        //

        private TcpServer server                                = null;
        private TcpClient client                                = null;

        //
        // lifecycle //////////////////////////////////////////////////////////
        //

        [TearDown]
        public void TearDown()
        {
            if(server != null)
            {
                server.Stop();
                server = null;
            }

            if(client != null)
            {
                client.Stop();
                client = null;
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public void Update()
        {
            if(server != null) server.Update();
            if(client != null) client.Update();
        }


        //
        // tests //////////////////////////////////////////////////////////////
        //

        [UnityTest]
        public IEnumerator TestLocalConnect()
        {
            server = new TcpServer();
            System.Net.IPEndPoint endpoint = new System.Net.IPEndPoint(IpUtils.GetLocalIp(), kHostPort);
            server.StartHost(endpoint, kSessionType, false);

            int serverRx = 0;
            server.router.AddRoute<UTF8TextPacket>((conn, packet)=>{
                ++serverRx;
                return true;
            });

            client = new TcpClient();
            client.ConnectToLocalHost(kHostPort);

            yield return Utils.WaitOrTimeout(()=>{
                Update();
                return client.conn.isConnected && server.conn.connections.Length > 0;
            }, 1.0);

            Assert.True(client.conn.isConnected);
            Assert.AreEqual(1, server.conn.connections.Length);
            client.conn.Send(new UTF8TextPacket("Hello World"));

            yield return Utils.DoFor(Update, 1.0);

            Assert.AreEqual(1, serverRx);
        }

        //
        // --------------------------------------------------------------------
        //
        
        [UnityTest]
        public IEnumerator TestSsl()
        {
            //client = new TcpClient();
            //ConnectionConfig config = new ConnectionConfig();

            //TODO: this server is no longer available
            //config.remoteEndpoint = IpUtils.Resolve("dev.blacktriangles.net", 7200);
            //config.nameOnCertificate = "dev.blacktriangles.net";
            //config.useTls = true;
            //client.Connect(config);

            //yield return Utils.DoFor(Update, 1.0);
            yield return true;

        }
    }
}
 
