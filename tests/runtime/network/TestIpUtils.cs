//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using blacktriangles.Network;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using UnityEngine.TestTools;

namespace blacktriangles.Testing
{
    [TestFixture]
    public class TestIpUtils
    {
        //
        // constants //////////////////////////////////////////////////////////
        //

        //
        // lifecycle //////////////////////////////////////////////////////////
        //

        //
        // tests //////////////////////////////////////////////////////////////
        //

        [UnityTest]
        public IEnumerator PrintIpInfo()
        {
            Assert.That(IpUtils.IsNetworkAvailable());
            Dbg.Log("Host Name: {0}", IpUtils.GetHostName());
            Dbg.Log("Local IP: {0}", IpUtils.GetLocalIp());
            Dbg.Log("Remote IP: {0}", IpUtils.GetRemoteIp());
            yield return true;
        }
    }
}
 
