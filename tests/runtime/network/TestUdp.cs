//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using blacktriangles.Network;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using UnityEngine.TestTools;

namespace blacktriangles.Testing
{
    [TestFixture]
    public class TestUdp
    {
        //
        // constants //////////////////////////////////////////////////////////
        //

        private static readonly IPAddress kHostName             = IPAddress.Parse("127.0.0.1");
        private static readonly int kPort1                      = 31001;
        private static readonly int kPort2                      = 31002;

        private UdpConnection conn0                             = null;
        private UdpConnection conn1                             = null;

        //
        // lifecycle //////////////////////////////////////////////////////////
        //

        [TearDown]
        public void TearDown()
        {
            if(conn0 != null)
            {
                conn0.Disconnect();
                conn0 = null;
            }

            if(conn1 != null)
            {
                conn1.Disconnect();
                conn1 = null;
            }
        }


        //
        // tests //////////////////////////////////////////////////////////////
        //

        [UnityTest]
        public IEnumerator ConnectTest()
        {
            ConnectionConfig config0 = new ConnectionConfig();
            config0.localEndpoint = new IPEndPoint(kHostName, kPort1);
            config0.remoteEndpoint = new IPEndPoint(kHostName, kPort2);

            ConnectionConfig config1 = new ConnectionConfig();
            config1.localEndpoint = config0.remoteEndpoint;
            config1.remoteEndpoint = config0.localEndpoint;

            //
            // create first udp connection
            //
            conn0 = new UdpConnection();
            conn0.OnNetworkError += OnNetworkError;
            conn0.Connect(config0);
            Assert.That(conn0.isActive);
            Assert.That(conn0.isConnected);

            //
            // create second udp connection
            //
            conn1 = new UdpConnection();
            conn1.OnNetworkError += OnNetworkError;
            conn1.Connect(config1);
            Assert.That(conn1.isActive);
            Assert.That(conn1.isConnected);

            //
            // send a message
            //
            UTF8TextPacket tx0 = new UTF8TextPacket("PING");
            conn0.Send(tx0);

            yield return Utils.WaitOrTimeout(()=>{
                return conn1.incomingCount > 0;
            }, 2f);

            //
            // receive a messsage
            //
            List<RawPacket> raw = conn1.TakePackets();
            Assert.AreEqual(1, raw.Count);
            List<UTF8TextPacket> rxs0 = raw.Map<RawPacket, UTF8TextPacket>((rawp)=>{
                return PacketFactory.Create<UTF8TextPacket>(rawp);
            });
            Assert.AreEqual(rxs0.Count, 1);
            Assert.AreEqual(rxs0[0].text, tx0.text);
            Assert.AreEqual(rxs0[0].receiver, conn1);
            Assert.AreEqual(rxs0[0].senderEndpoint, conn0.config.Value.localEndpoint);

            //
            // cleanup
            //
            conn0.Disconnect();
            conn1.Disconnect();
        }

        //
        // callbacks //////////////////////////////////////////////////////////
        //

        void OnNetworkError(NetworkError err)
        {
            Dbg.Error(err.message);
            Assert.That(false);
        }

        //
        // End Class //////////////////////////////////////////////////////////
        //
    }
}
 
