//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using blacktriangles.Network;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using UnityEngine.TestTools;

namespace blacktriangles.Testing
{
    [TestFixture]
    public class TestBeacon
    {
        //
        // constants //////////////////////////////////////////////////////////
        //

        private Beacon beacon                                   = null;
        private UdpConnection conn                              = null;

        //
        // lifecycle //////////////////////////////////////////////////////////
        //

        [TearDown]
        public void TearDown()
        {
            if(beacon != null)
            {
                beacon.Stop();
                beacon = null;
            }

            if(conn != null)
            {
                conn.Disconnect();
                conn = null;
            }
        }

        //
        // tests //////////////////////////////////////////////////////////////
        //

        [UnityTest]
        public IEnumerator ConnectTest()
        {
            //
            // start the beacon
            //
            DiscoveryData discoveryData = new DiscoveryData();
            discoveryData.sessionType = 0x10;
            discoveryData.address = "OpenAddress";
            discoveryData.port = 1337;
            beacon = new Beacon();
            beacon.Start(discoveryData);
            IPEndPoint rxEndpoint = new IPEndPoint(IPAddress.Broadcast, Beacon.kDiscoveryPort);

            //
            // create a connection (usually you would use a probe)
            //

            ConnectionConfig config = new ConnectionConfig();
            config.localEndpoint = new IPEndPoint(IPAddress.Any, 0);
            config.remoteEndpoint = null;
            conn = new UdpConnection();
            conn.Connect(config);

            //
            // send a discovery packet
            //
            DiscoveryRequest packet = new DiscoveryRequest();
            packet.sessionType = discoveryData.sessionType;
            conn.Send(packet, rxEndpoint);

            //
            // give it some time
            //
            yield return new WaitForSeconds(1f);

            //
            // we expect a discovery response
            //

            List<RawPacket> rawList = conn.TakePackets();
            List<DiscoveryResponse> resList = rawList.Map<RawPacket, DiscoveryResponse>((raw)=>{
                return PacketFactory.Create<DiscoveryResponse>(raw);
            });

            Assert.AreEqual(resList.Count, 1);
            DiscoveryResponse res = resList[0];
            Assert.AreEqual(res.data.sessionType, discoveryData.sessionType);
            Assert.AreEqual(res.data.address, discoveryData.address);
            Assert.AreEqual(res.data.port, discoveryData.port);
        }

        //
        // callbacks //////////////////////////////////////////////////////////
        //

        void OnNetworkError(NetworkError err)
        {
            Dbg.Error(err.message);
            Assert.That(false);
        }

        //
        // End Class //////////////////////////////////////////////////////////
        //
    }
}
 
