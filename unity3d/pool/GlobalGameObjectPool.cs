//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Collections.Generic;
using UnityEngine;

namespace blacktriangles
{
    public static class GlobalGameObjectPool
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        private static Dictionary<GameObject, GameObjectPool> pools = new Dictionary<GameObject, GameObjectPool>();
        private static Transform poolRoot = null;

        //
        // public members /////////////////////////////////////////////////////
        //

        public static GameObjectPool Create(GameObject prefab, Transform root)
        {
            GameObjectPool pool = null;
            if(!pools.TryGetValue(prefab, out pool))
            {
                pool = new GameObjectPool();
            }

            if(root == null)
            {
                if(poolRoot == null)
                {
                    GameObject go = new GameObject($"Global GameObject Pool");
                    poolRoot = go.transform;
                }

                GameObject prefabRoot = new GameObject($"{prefab.name} POOL");
                root = prefabRoot.transform;
                root.SetParent(poolRoot);
            }

            pool.prefab = prefab;
            pool.root = root;
            pools[prefab] = pool;
            return pool;
        }

        //
        // --------------------------------------------------------------------
        //

        public static GameObject Take(GameObject prefab, Vector3 position, Quaternion rotation, Transform parent)
        {
            if(prefab == null) return null;

            GameObjectPool pool = null;
            if(!pools.TryGetValue(prefab, out pool))
            {
                pool = Create(prefab, null);
            }

            return pool.Take(position, rotation, parent);
        }
    }
}
