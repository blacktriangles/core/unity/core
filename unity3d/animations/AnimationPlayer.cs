//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Playables;

namespace blacktriangles
{
    public class AnimationPlayer
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public Animator animator                                = null;

        private PlayableGraph graph;

        private AnimationLayerMixerPlayable mixer;
        private AnimatorControllerPlayable animatorPlayable;

        private Dictionary<int, AnimationClipPlayable> clips    = null;

        private float activeStart                               = 0f;
        private AnimationClipPlayable activeClip;
        private AnimationClipPlayable nextClip;

        //
        // initialize /////////////////////////////////////////////////////////
        //

        public void Initialize()
        {
            if(animator == null) animator = GetComponent<Animator>();
            Dbg.Assert(animator != null, "Animation Stack does not have animator!");

            clips = new Dictionary<int, AnimationClipPlayable>();

            if(graph.IsValid())
            {
                graph.Destroy();
            }

            graph = PlayableGraph.Create(name + "AnimStack Graph");

            // input 1 -> the base animator
            // input 2 -> the main item 
            // input 3 -> an overlay item
            mixer = AnimationLayerMixerPlayable.Create(graph, 3);

            // hook up our animator as the primary input.
            animatorPlayable = AnimatorControllerPlayable.Create(graph, animator.runtimeAnimatorController);
            graph.Connect(animatorPlayable, 0, mixer, 0);
            mixer.SetInputWeight(animatorPlayable, 1f);
            mixer.SetLayerAdditive(0, false);

            // hook up the mixer to the animator via an AnimationPlayableOutput node
            var animOutput = AnimationPlayableOutput.Create(graph, "AnimationOutput", animator);
            animOutput.SetSourcePlayable(mixer, 0);

            graph.Play();

        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void RegisterClip<EnumType>(EnumType idx, AnimationClip clip)
            where EnumType: System.Enum
        {
            if(clip != null)
            {
                AnimationClipPlayable playable = AnimationClipPlayable.Create(graph, clip);
                clips[EnumUtility.Convert<EnumType, int>(idx)] = playable;
            }
        }

        //
        // --------------------------------------------------------------------
        //
        

        public void Play<EnumType>(EnumType idx, float speed, bool interrupt = false)
            where EnumType: System.Enum
        {
            int id = EnumUtility.Convert<EnumType, int>(idx);

            if(clips.ContainsKey(id) == false) return;

            AnimationClipPlayable tryClip = clips[id];
            if(!nextClip.Equals(tryClip) && !nextClip.Equals(tryClip))
            {
                nextClip = clips[id];
                nextClip.SetSpeed(speed);
                if(interrupt)
                {
                    Stop();
                }
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        public IEnumerator PlayAsync<EnumType>(EnumType idx, float speed)
        {
            int id = EnumUtility.Convert<EnumType, int>(idx);

            if(clips.ContainsKey(id) == false) yield break;

            AnimationClipPlayable clip = clips[id];
            clip.Pause();
            clip.SetSpeed(speed);
            clip.SetTime(0f);

            Play(clip);
            
            // wait for it
            yield return new WaitForSeconds(clip.GetDuration());

            clip.Pause();
            clip.SetSpeed(0f);
            clip.SetTime(0f);

            Stop();
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private void Play(AnimationClipPlayable clip)
        {
            // set its time to 0
            clip.SetTime(0f);

            // connect it
            graph.Disconnect(mixer, 1);
            graph.Connect(clip, 0, mixer, 1);

            //  swap to it
            mixer.SetInputWeight(1, 1f);
            mixer.SetInputWeight(0, 0f);

            clip.Play();
        }
        
        //
        // --------------------------------------------------------------------
        //

        private void Stop()
        {
            if(activeClip.IsValid())
            {
                activeClip.Pause();
            }

            // swap it back
            mixer.SetInputWeight(0, 1f);
            mixer.SetInputWeight(1, 0f);

            // disconnect it
            graph.Disconnect(mixer, 1);

            activeClip = (AnimationClipPlayable)Playable.Null;
        }
        

        //
        // unity callbacks ////////////////////////////////////////////////////
        //
        
        protected virtual void Update()
        {
            if(activeClip.IsValid())
            {
                float elapsed = Time.time - activeStart;
                if(activeClip.IsDone())
                {
                    Stop();
                }
            }

            if(nextClip.IsValid() && !activeClip.IsValid())
            {
                Play(nextClip);
                activeClip = nextClip;
                activeStart = Time.time;
                nextClip = (AnimationClipPlayable)Playable.Null;
            }
        }
        

        //
        // --------------------------------------------------------------------
        //

        protected virtual void OnDestroy()
        {
            graph.Destroy();
        }
    }
}
