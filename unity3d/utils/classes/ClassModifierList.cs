//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace blacktriangles
{

    //
    // ########################################################################
    //
    
    [System.Serializable]
    public class BaseClassModifierList
    {
        public virtual System.Type modifiedType                 { get { return null; } }
        public virtual int count                                { get { return 0; } }

        public virtual List<ClassModifier> GetModifiers()       { return null; }
        public virtual List<FieldInfo> GetFields()              { return null; }
        public virtual void AddModifier(ClassModifier modifier) { return; }
        public virtual void UpdateModifier(int idx, ClassModifier modifier) { return; }
        public virtual void RemoveModifier(int idx)             { return; }
        public virtual FieldInfo FindField(string name)         { return null; }

        public virtual void Reset()                             { return; }
    }

    //
    // ########################################################################
    //
    
    [System.Serializable]
    public struct SerdeClassModifier
    {
        public string fieldName;
        public ClassModifierType type;
        public float modifier;
    }

    //
    // ########################################################################
    //

    [System.Serializable]
    public class ClassModifierList<T>
        : BaseClassModifierList
        , IEnumerable<ClassModifier>
        , ISerializationCallbackReceiver
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public override System.Type modifiedType                { get { return typeof(T); } }
        public override int count                               { get { return modifiers.Count; } }

        private static List<FieldInfo> fields                   = null;

        private List<ClassModifier> modifiers                        = new List<ClassModifier>();
        [SerializeField] List<SerdeClassModifier> serdeMods                    = new List<SerdeClassModifier>();

        //
        // ////////////////////////////////////////////////////////////////////////
        //
        
        static ClassModifierList()
        {
            fields = new List<FieldInfo>();
            FieldInfo[] infos = typeof(T).GetFields();
            foreach(FieldInfo info in infos)
            {
                if(info.IsPublic)
                {
                    fields.Add(info);
                }
            }
        }

        //
        // public members /////////////////////////////////////////////////////
        //
        
        public override List<ClassModifier> GetModifiers()
        {
            return modifiers;
        }

        //
        // --------------------------------------------------------------------
        //

        public override void UpdateModifier(int idx, ClassModifier modifier)
        {
            if(modifiers.IsValidIndex(idx))
            {
                modifiers[idx] = modifier;
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        public override void AddModifier(ClassModifier modifier)
        {
            modifiers.Add(modifier);
        }

        //
        // --------------------------------------------------------------------
        //
        
        public override void RemoveModifier(int idx)
        {
            modifiers.RemoveAt(idx);
        }

        //
        // --------------------------------------------------------------------
        //
        
        public override List<FieldInfo> GetFields()
        {
            return fields;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public override FieldInfo FindField(string name)
        {
            return fields.Find((f)=>f.Name == name);
        }
        
        //
        // --------------------------------------------------------------------
        //
        
        public T Apply(object target)
        {
            foreach(ClassModifier modifier in modifiers)
            {
                float val = System.Convert.ToSingle(modifier.field.GetValue(target));
                switch(modifier.type)
                {
                    case ClassModifierType.Add:
                        val += modifier.modifier;
                        break;

                    case ClassModifierType.Subtract:
                        val -= modifier.modifier;
                        break;

                    case ClassModifierType.Multiply:
                        val *= modifier.modifier;
                        break;

                    case ClassModifierType.Divide:
                        val = val / modifier.modifier;
                        break;

                    case ClassModifierType.Set:
                        val = modifier.modifier;
                        break;
                }

                object newVal = System.Convert.ChangeType(val, modifier.field.FieldType);
                modifier.field.SetValue(target, newVal);
            }
            return (T)target;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public override void Reset()
        {
            modifiers.Clear();
        }

        //
        // ////////////////////////////////////////////////////////////////////
        //
        
        public IEnumerator<ClassModifier> GetEnumerator()
        {
            foreach(ClassModifier mod in modifiers)
            {
                yield return mod;
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        //
        // ISerializationCallbackReciever /////////////////////////////////////
        //
        
        public void OnBeforeSerialize()
        {
            serdeMods = new List<SerdeClassModifier>();
            if(modifiers == null) return;

            foreach(ClassModifier modifier in modifiers)
            {
                if(modifier.field != null)
                {
                    serdeMods.Add(new SerdeClassModifier() {
                        fieldName = modifier.field.Name,
                        type = modifier.type,
                        modifier = modifier.modifier,
                    });
                }
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void OnAfterDeserialize()
        {
            modifiers = new List<ClassModifier>();
            if(serdeMods == null) return;

            foreach(SerdeClassModifier mod in serdeMods)
            {
                FieldInfo field = FindField(mod.fieldName);
                modifiers.Add(new ClassModifier(){
                    field = field,
                    type = mod.type,
                    modifier = mod.modifier,
                });
            }
        }

    }
}
