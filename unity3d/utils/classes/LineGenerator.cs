//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using UnityEngine;
using System.Collections.Generic;

namespace blacktriangles
{

    //
    // Line Generator /////////////////////////////////////////////////////////
    //

    public static class LineGenerator
    {
        public static List<Line> Radial(Vector3 start, float length, float degrees, uint count)
        {
            return Radial(start, length, degrees, count, Vector3.forward, Vector3.up);
        }

        //
        // --------------------------------------------------------------------
        //

        public static List<Line> Radial(Vector3 start, float length, float degrees, uint count, Vector3 forward, Vector3 up)
        {
            List<Line> result = new List<Line>();

            float halfdeg = degrees / 2.0f;
            float step = degrees / (count-1);
            for(uint i = 0; i < count; ++i)
            {
                Line newLine = new Line() { start = start, end = start + forward };
                float angleDeg = (step * i) - halfdeg;
                Quaternion rot = Quaternion.AngleAxis(angleDeg, up);
                newLine.end = rot * forward * length + start;
                result.Add(newLine);
            }

            return result;
        }
    }
}
