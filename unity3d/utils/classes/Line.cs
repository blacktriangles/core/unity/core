//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using blacktriangles;
using UnityEngine;

namespace blacktriangles
{
    public struct Line
    {
        public Vector3 start;
        public Vector3 end;
    }
}
