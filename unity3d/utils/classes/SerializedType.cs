//
// (C) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using blacktriangles;
using UnityEngine;

namespace blacktriangles
{

    //
    // Handles serializing System.Type in Unity3d
    //

    [System.Serializable]
    public class SerializedType
        : ISerializationCallbackReceiver
    {
        //
        // members  ///////////////////////////////////////////////////////////
        //
        public System.Type type                                 = null;
        [SerializeField] private string _typename               = null;

        //
        // static methods /////////////////////////////////////////////////////
        //

        public static System.Type ToType(string typename)
        {
            System.Type result = null;
            if(!System.String.IsNullOrEmpty(typename))
            {
                result = System.Type.GetType(typename);
            }
            return result;
        }


        //
        // --------------------------------------------------------------------
        //

        public static string ToString(System.Type type)
        {
            string result = System.String.Empty;
            if(type != null)
            {
                result = type.AssemblyQualifiedName;
            }
            return result;
        }

        //
        // constructor / initializer //////////////////////////////////////////
        //

        public SerializedType(System.Type _type)
        {
            type = _type;
        }

        //
        // serialization //////////////////////////////////////////////////////
        //

        public void OnBeforeSerialize()
        {
            _typename = ToString(type); 
        }

        //
        // ----------------------------------------------------------------------------
        //

        public void OnAfterDeserialize()
        {
            type = ToType(_typename);
        }

        //
        // operators  /////////////////////////////////////////////////////////
        //

        public static implicit operator System.Type(SerializedType type)
        {
            return type.type;
        }

        //
        // ----------------------------------------------------------------------------
        //

        public static implicit operator SerializedType(System.Type type)
        {
            return new SerializedType(type);
        }
    };
}
