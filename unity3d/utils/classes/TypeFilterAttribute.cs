//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    public class TypeFilterAttribute
        : PropertyAttribute
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public System.Type[] types                              { get; private set; }

        //
        // constructor / initializer  /////////////////////////////////////////
        //
        
        public TypeFilterAttribute(System.Type baseType)
        {
            #if UNITY_EDITOR
            types = AssemblyUtility.CollectAllOfType(baseType).ToArray();
            #endif
        }

        //
        // --------------------------------------------------------------------
        //

        public TypeFilterAttribute(System.Type[] _types)
        {
            types = _types;
        }
    }
}
