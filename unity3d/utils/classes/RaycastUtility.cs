//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using UnityEngine;
using System.Collections.Generic;

namespace blacktriangles
{
    public struct RaycastResult
    {
        public bool didHit;
        public float closestHit;
        public List<RaycastHit> hits;
        public List<Line> lines;

        public static RaycastResult Default()
        {
            return new RaycastResult() {
                didHit = false,
                closestHit = Mathf.Infinity,
                hits = new List<RaycastHit>(),
                lines = new List<Line>()
            };
        }
    };

    public static class RaycastUtility
    {
        public static RaycastResult Sweep(Vector3 pos, float dist, float degrees, uint count)
        {
            return Sweep(
                pos,
                dist,
                degrees,
                count,
                Vector3.forward,
                Vector3.up,
                0 //layercast all
            );
        }

        public static RaycastResult Sweep(
            Vector3 pos, 
            float dist, 
            float angle, 
            uint count,
            Vector3 forward,
            Vector3 up,
            int layerMask = ~0)
        {
            RaycastResult result = RaycastResult.Default();

            result.lines = LineGenerator.Radial(pos, dist, angle, count, forward, up);

            RaycastHit hit;
            foreach(Line line in result.lines)
            {
                bool didHit = Physics.Linecast(line.start, line.end, out hit, layerMask);
                if(didHit)
                {
                    result.didHit = true;
                    result.closestHit = Mathf.Min(result.closestHit, hit.distance);
                    result.hits.Add(hit);
                }
            }

            return result;
        }
    }
}
