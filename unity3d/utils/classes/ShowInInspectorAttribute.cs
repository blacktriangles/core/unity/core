//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System;
using UnityEngine;

namespace blacktriangles
{
    [AttributeUsage(AttributeTargets.Field|AttributeTargets.Property)]
    public class ShowInInspectorAttribute
        : PropertyAttribute
    {
    }
}
