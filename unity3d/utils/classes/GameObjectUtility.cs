//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using System.Collections.Generic;
using UnityEngine;

namespace blacktriangles
{
    public static class GameObjectUtility
    {
        #if UNITY_EDITOR

        public static T[] LoadAssetsAtPrefab<T>(this GameObject go)
            where T : UnityEngine.Object
        {
            string path = go.GetPrefabPath();
            if(path == System.String.Empty)
            {
                Dbg.Warn("Only works on prefabs");
                return null;
            }

            path = System.IO.Path.GetDirectoryName(path);
            return AssetDb.LoadAllAssetsInPath<T>(path).ToArray();
        }

        //
        // --------------------------------------------------------------------
        //

        public static string GetPrefabPath(this GameObject go)
        {
            string result = System.String.Empty;
            Object obj = go;
            if(!go.IsPrefab())
            {   
                obj = UnityEditor.PrefabUtility.GetCorrespondingObjectFromSource(go);
            }

            return UnityEditor.AssetDatabase.GetAssetPath(obj);
        }
        #endif

        //
        // ------------------------------------------------------------------------
        //
        
        public static bool UpdatePrefab(this GameObject go)
        {
            bool result = false;
            if(!go.IsPrefab())
            {
                #if UNITY_EDITOR
                    if(UnityEditor.PrefabUtility.IsPartOfAnyPrefab(go))
                    {
                        UnityEditor.PrefabUtility.ApplyPrefabInstance(go, UnityEditor.InteractionMode.AutomatedAction);
                        result = true;
                    }
                #endif
            }

            return result;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public static Bounds CalculateBounds( this GameObject go )
        {
            Bounds bounds = new Bounds();
            Renderer[] renderers = go.GetComponentsInChildren<Renderer>();
            bounds.center = go.transform.position;

            foreach(Renderer renderer in renderers)
            {
                bounds.Encapsulate(renderer.bounds);
            }

            return bounds;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public static bool IsPrefab( this GameObject go )
        {
            return go.scene.rootCount == 0;
        }

        //
        // --------------------------------------------------------------------
        //

        public static List<T> GetAllComponent<T>(IEnumerable<GameObject> gos)
            where T: Component
        {
            List<T> result = new List<T>();
            foreach(GameObject go in gos)
            {
                result.AddRange(go.GetComponentsInChildren<T>());
            }

            return result;
        }

        //
        // --------------------------------------------------------------------
        //

        public static List<Material> GetAllMaterials( this GameObject go )
        {
            List<Material> result = new List<Material>();
            Renderer[] renderers = go.GetComponentsInChildren<Renderer>();
            foreach(Renderer rend in renderers)
            {
                foreach(Material mat in rend.materials)
                {
                    result.Add(mat);
                }
            }

            return result;
        }
        
        //
        // --------------------------------------------------------------------
        //
        
        public static List<Material> GetAllSharedMaterials( this GameObject go )
        {
            List<Material> result = new List<Material>();
            Renderer[] renderers = go.GetComponentsInChildren<Renderer>();
            foreach(Renderer rend in renderers)
            {
                foreach(Material mat in rend.sharedMaterials)
                {
                    result.Add(mat);
                }
            }

            return result;
        }
    }
}
