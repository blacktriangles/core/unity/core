//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

//
// https://answers.unity.com/questions/489942/how-to-make-a-readonly-property-in-inspector.html
//

using UnityEngine;

namespace blacktriangles
{
    public class ReadOnlyAttribute
        : PropertyAttribute
    {
    }
}
