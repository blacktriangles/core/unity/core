//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    public static partial class MeshUtility
    {

        //
        // members ////////////////////////////////////////////////////////////////////////
        //
        
        public static Mesh uprightPlane                         { get { return GetUprightPlane(); } }
        private static Mesh _uprightPlane                       = null;

        public static Mesh arrowPlane                           { get { return GetArrowPlane(); } }
        private static Mesh _arrowPlane                         = null;

        public static Mesh circlePlane                          { get { return GetCirclePlane(); } }
        private static Mesh _circlePlane                        = null;

        //
        // defaults ///////////////////////////////////////////////////////////
        //

        private static Mesh GetUprightPlane()
        {
            if(_uprightPlane == null)
            {
                _uprightPlane = CreateUprightPlane(1f);
            }

            return _uprightPlane;
        }

        //
        // --------------------------------------------------------------------
        //
        
        private static Mesh GetArrowPlane()
        {
            if(_arrowPlane == null)
            {
                _arrowPlane = CreateArrowPlane(1f);
            }

            return _arrowPlane;
        }

        //
        // --------------------------------------------------------------------
        //
        
        private static Mesh GetCirclePlane()
        {
            if(_circlePlane == null)
            {
                _circlePlane = CreateCirclePlane(16, 1f);
            }

            return _circlePlane;
        }

        //
        // create methods /////////////////////////////////////////////////////
        //

        //
        // Creates a two sided plane that is "standing up", facing in the 
        // positive z direction
        //
        
        public static Mesh CreateUprightPlane(float size)
        {
            Mesh result = new Mesh();
            result.Clear();

            Vector3[] verts = new Vector3[] { 
                new Vector3(-0.5f, 0f, 0f) * size,
                new Vector3(-0.5f, 1f, 0f) * size,
                new Vector3(0.5f, 1f, 0f) * size,
                new Vector3(0.5f, 0f, 0f) * size
            };

            int[] tris = new int[] {
                // back face
                0, 1, 2,
                0, 2, 3,

                // front face
                2, 1, 0,
                3, 2, 0,
            };

            Vector2[] uvs = new Vector2[] {
                new Vector2(-1f, -1f),
                new Vector2(-1f, 1f),
                new Vector2(1f, 1f),
                new Vector2(1f, -1f)
            };

            Vector3[] norms = new Vector3[] {
                Vector3.forward,
                Vector3.forward,
                Vector3.forward,
                Vector3.forward
            };

            result.vertices = verts;
            result.triangles = tris;
            result.uv = uvs;
            result.normals = norms;

            return result;
        }

        //
        // Creates a 2d arrow, with the normals facing up
        //

        public static Mesh CreateArrowPlane(float size)
        {
            Mesh result = new Mesh();
            result.Clear();

            Vector3[] verts = new Vector3[] {
                new Vector3(-0.25f, 0f, 0f) * size,     // 0
                new Vector3(-0.25f, 0f, 0.75f) * size,  // 1
                new Vector3(-0.5f, 0f, 0.75f) * size,   // 2
                new Vector3(0f, 0f, 1f) * size,         // 3
                new Vector3(0.5f, 0f, 0.75f) * size,    // 4
                new Vector3(0.25f, 0f, 0.75f) * size,   // 5
                new Vector3(0.25f, 0f, 0f) * size,      // 6
            };

            int[] tris = new int[] {
                // tip (front/back)
                2, 3, 4,
                4, 3, 2,

                // body (front, back)
                0, 1, 5,
                5, 1, 0,
                0, 5, 6,
                6, 5, 0,
            };

            Vector2[] uvs = new Vector2[] {
                new Vector2(-0.5f, 0f),
                new Vector2(-0.5f, 0.75f),
                new Vector2(-1f, 0.75f),
                new Vector2(0f, 1f),
                new Vector2(1f, 0.75f),
                new Vector2(0.5f, 0.75f),
                new Vector2(0.5f, 0f),
            };

            Vector3[] norms = new Vector3[7];
            norms.Fill(Vector3.up);
            
            result.vertices = verts;
            result.triangles = tris;
            result.uv = uvs;
            result.normals = norms;

            return result;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public static Mesh CreateCirclePlane(int vcount, float radius)
        {
            Mesh result = new Mesh();
            result.Clear();

            int vertCount = vcount + 1;
            Vector3[] verts = new Vector3[vertCount];

            verts[0] = Vector3.zero;

            float step = 2 * btMath.PI / ((float)vcount-1);
            for(int i = 1; i < vertCount; ++i)
            {
                float angle = step * (i-1);
                verts[i] = new Vector3(
                    radius * btMath.Cos(angle),
                    0f,
                    radius * btMath.Sin(angle)
                );
            }

            int[] tris = new int[vertCount*3];
            for(int i = 1; i < vertCount; ++i)
            {
                int tridx = i * 3;
                tris[tridx] = i+1;
                tris[tridx+1] = i;
                tris[tridx+2] = 0;
            }

            int idx = (vertCount-1)*3;
            tris[idx] = vertCount-1;
            tris[idx+1] = vertCount-2;
            tris[idx+2] = 0;

            Vector2[] uvs = new Vector2[vertCount];
            uvs.Fill(Vector2.zero);
            
            Vector3[] normals = new Vector3[vertCount];
            normals.Fill(Vector3.up);
            
            result.vertices = verts;
            result.triangles = tris;
            result.uv = uvs;
            result.normals = normals;

            return result;

        }
    }
}
