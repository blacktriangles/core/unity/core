//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

// Palattes: http://www.colourlovers.com/palette/1220087/Dreaming_in_Santiago
// #322B28     50,43,40     Gray
// #2B343E    43,52,62    Bluer Gray
// #3B8686     59,134,134    Light Blue
// #0B486B    11,72,107    Deeper Blue

using UnityEngine;

namespace blacktriangles
{
    public static class GUIStyles
    {
        // constants //////////////////////////////////////////////////////////
        public static readonly Color orangeColor                = new Color( 0.964f, 0.5803f, 0.2352f );
        public static readonly Texture2D orange                 = ConvertColorToTexture2D( orangeColor );

        // members ////////////////////////////////////////////////////////////
        public static readonly Color gray                       = new Color( 0.15f, 0.15f, 0.15f );
        public static readonly Color lightboxCol                = new Color( 0.2f, 0.2f, 0.2f );
        public static readonly Color lightgray                  = new Color( 0.4f, 0.4f, 0.45f );
        public static readonly Color black                      = new Color( 0f, 0f, 0f, 1f );
        public static readonly Color blackLightAlpha            = new Color( 0f, 0f, 0f, 0.5f );
        public static readonly Color blackAlpha                 = new Color( 0f, 0f, 0f, 0.25f );
        public static readonly Color ghost                      = new Color( 43f/255, 52f/255, 62f/255, 0.4f );
        public static readonly Color offwhite                   = new Color( 0.7f, 0.7f, 0.7f );
        public static readonly Color bluegray                   = new Color( 43f/255, 52f/255, 62f/255 );
        public static readonly Color lightbluegray              = new Color( 38f/255, 47f/255, 57f/255 );
        public static readonly Color lightblue                  = new Color( 59f/255, 134f/255, 134f/255 );
        public static readonly Color deepblue                   = new Color( 11f/255, 72f/255, 107f/255 );
        public static readonly Color deepblueAlpha              = new Color( 11f/255, 72f/255, 107f/255, 0.5f );
        public static readonly Color yellow                     = new Color( 218f/255, 246f/255, 43f/255, 1f );
        public static readonly Color darkyellow                 = new Color( 156f/255, 150f/255, 0f, 1f );
        public static readonly Color green                      = new Color( 0f, 200f/255f, 0f, 1f );
        public static readonly Color red                        = new Color( 1f, 0f, 0f, 1f );
        
        public static readonly Color primaryBg                  = gray;
        public static readonly Color primaryBgHighlight         = deepblue;
        public static readonly Color inactiveTextColor          = lightgray;
        public static readonly Color activeTextColor            = Color.white;

        public static readonly Color headerColor                = bluegray;
        public static readonly Color lightHeaderColor           = lightbluegray;
        public static readonly Color secondaryBg                = ghost;
        public static readonly Color secondaryBgHighlight       = deepblue;

        public static Texture2D lightboxTex                     { get; private set; }
        public static Texture2D blackAlphaTex                   { get; private set; }

        public static Texture2D headerBgNormalTex               { get; private set; }
        public static Texture2D lightHeaderBgNormalTex          { get; private set; }

        public static Texture2D buttonBgNormalTex               { get; private set; }
        public static Texture2D buttonBgHighlightTex            { get; private set; }
        public static Texture2D secondaryBgNormalTex            { get; private set; }
        public static Texture2D secondaryBgHighlightTex         { get; private set; }

        public static GUIStyle header                           { get; private set; }
        public static GUIStyle lightHeader                      { get; private set; }

        public static GUIStyle buttonNormal                     { get; private set; }
        public static GUIStyle buttonNormalBorder               { get; private set; }
        public static GUIStyle buttonHighlight                  { get; private set; }
        public static GUIStyle buttonNoBorder                   { get; private set; }
        public static GUIStyle buttonNoBorderHighlight          { get; private set; }

        public static GUIStyle lbuttonNormal                    { get; private set; }
        public static GUIStyle lbuttonHighlight                 { get; private set; }

        public static GUIStyle secondaryButtonNormal            { get; private set; }
        public static GUIStyle secondaryButtonHighlight         { get; private set; }

        public static GUIStyle lightButtonNormal                { get; private set; }

        public static GUIStyle box                              { get; private set; }
        public static GUIStyle darkbox                          { get; private set; }
        public static GUIStyle lightbox                         { get; private set; }
        public static GUIStyle errorbox                         { get; private set; }
        public static GUIStyle noticebox                        { get; private set; }

        public static GUIStyle treeitem                         { get; private set; }

        // constructor / initializer //////////////////////////////////////////////
        static public void Initialize()
        {
        }

        static public void OnGUI()
        {
            ///###hsmith $TODO figure out how to cache this properly
            lightboxTex = ConvertColorToTexture2D( lightboxCol );
            buttonBgNormalTex = ConvertColorToTexture2D( primaryBg );
            buttonBgHighlightTex = ConvertColorToTexture2D( primaryBgHighlight );
            secondaryBgNormalTex = ConvertColorToTexture2D( secondaryBg );
            secondaryBgHighlightTex = ConvertColorToTexture2D( secondaryBgHighlight );
            headerBgNormalTex = ConvertColorToTexture2D( headerColor );
            lightHeaderBgNormalTex = ConvertColorToTexture2D( lightHeaderColor );

            blackAlphaTex = ConvertColorToTexture2D( blackAlpha );

            buttonNormal = new GUIStyle( GUI.skin.box );
            buttonNormal.stretchWidth = true;
            buttonNormal.normal.textColor = offwhite;
            buttonNormal.normal.background = buttonBgNormalTex;
            buttonNormal.active.textColor = activeTextColor;
            buttonNormal.active.background = buttonBgHighlightTex;

            lbuttonNormal = new GUIStyle(GUI.skin.label);
            lbuttonNormal.stretchWidth = true;
            lbuttonNormal.normal.textColor = offwhite;
            lbuttonNormal.normal.background = buttonBgNormalTex;
            lbuttonNormal.active.textColor = activeTextColor;
            lbuttonNormal.active.background = buttonBgHighlightTex;

            buttonHighlight = new GUIStyle( buttonNormal );
            buttonHighlight.normal.textColor = activeTextColor;
            buttonHighlight.normal.background = buttonBgHighlightTex;

            lbuttonHighlight = new GUIStyle( lbuttonNormal );
            lbuttonHighlight.normal.textColor = activeTextColor;
            lbuttonHighlight.normal.background = buttonBgHighlightTex;


            secondaryButtonNormal = new GUIStyle( buttonNormal );
            secondaryButtonNormal.normal.background = secondaryBgNormalTex;

            secondaryButtonHighlight = new GUIStyle( buttonHighlight );
            secondaryButtonHighlight.normal.background = secondaryBgHighlightTex;

            lightButtonNormal = new GUIStyle( buttonNormal );
            lightButtonNormal.normal.background = lightboxTex;

            buttonNoBorder = new GUIStyle( buttonNormal );
            buttonNoBorder.normal.background = null;
            buttonNoBorder.border = new RectOffset( 0, 0, 0, 0 );

            buttonNoBorderHighlight = new GUIStyle( buttonHighlight );
            buttonNoBorderHighlight.border = new RectOffset( 0, 0, 0, 0 );

            header = new GUIStyle( buttonNormal );
            header.normal.background = headerBgNormalTex;
            header.normal.textColor = Color.white;

            lightHeader = new GUIStyle( header );
            header.normal.background = lightHeaderBgNormalTex;

            box = new GUIStyle( GUI.skin.box );

            darkbox = new GUIStyle( GUI.skin.box );
            darkbox.normal.background = buttonBgNormalTex;

            lightbox = new GUIStyle( GUI.skin.box );
            lightbox.normal.background = lightboxTex;

            errorbox = new GUIStyle( buttonNormal );
            errorbox.normal.textColor = red;

            noticebox = new GUIStyle( GUI.skin.box );
            noticebox.normal.textColor = Color.white;

            treeitem = new GUIStyle( GUI.skin.label );
            treeitem.normal.background = secondaryBgNormalTex;
            treeitem.margin = new RectOffset(5, 5, 5, 5);
        }

        public static Texture2D ConvertColorToTexture2D( Color color )
        {
            Texture2D result = new Texture2D( 1, 1 );
            result.SetPixel( 0, 0, color );
            result.Apply();
            return result;
        }

        public static GUIStyle ConvertColorToStyle( Color color )
        {
            Texture2D tex = ConvertColorToTexture2D( color );
            GUIStyle result = new GUIStyle( GUI.skin.box );
            result.normal.background = tex;
            return result;
        }

        // private methods ////////////////////////////////////////////////////
        private static void Validate()
        {

        }
    }
}
