//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
// Using modified bezier curve drawing code originally written by Linusmartensson
// http://forum.unity3d.com/threads/drawing-lines-in-the-editor.71979/
//
//=============================================================================

using UnityEngine;
using System.Collections.Generic;
using System.Reflection;

namespace blacktriangles
{
    #if UNITY_EDITOR
        [UnityEditor.InitializeOnLoad]
    #endif
    public static class DrawUtility
    {
        // constants //////////////////////////////////////////////////////////
        public static readonly string kShaderName                 = "Unlit/Color";
        // members ////////////////////////////////////////////////////////////
        public static Material lineMaterial                        = null;
        private static Texture2D aaLineTex                         = null;
        private static Texture2D lineTex                         = null;

        // constructor / initializer //////////////////////////////////////////
        static DrawUtility()
        {
            lineMaterial = CreateMaterial( kShaderName );
        }

        // public methods /////////////////////////////////////////////////////
        public static void DrawGrid( Rect rect, Vector2 cellSize, Vector2 scroll, Color color )
        {
            DrawGrid( rect, cellSize, scroll, color, color, 0 );
        }

        public static void DrawGrid( Rect rect, Vector2 cellSize, Vector2 scroll, Color majorColor, Color minorColor, int majorLineOn )
        {
            if( Event.current.type == EventType.Repaint )
            {
                DrawUtility.lineMaterial.SetPass( 0 );

                bool hasMinorLines = (majorLineOn > 0);
                float xoffset = scroll.x % cellSize.x;
                float yoffset = scroll.y % cellSize.y;

                GL.Begin( GL.LINES );
                    GL.Color( majorColor );

                    int count = (int)btMath.Round( scroll.y / cellSize.y );
                    for( float y = rect.y-yoffset; y < rect.yMax+yoffset; y += cellSize.y )
                    {
                        if( hasMinorLines )
                        {
                            bool majorLine = count++ % majorLineOn == 0;
                            GL.Color( majorLine ? majorColor : minorColor );
                        }

                        if( y >= rect.y && y <= rect.yMax)
                        {
                            GL.Vertex3( rect.x, y, 0 );
                            GL.Vertex3( rect.xMax, y, 0 );
                        }
                    }

                    count = (int)btMath.Floor( scroll.x / cellSize.x );
                    for( float x = rect.x-xoffset; x < rect.xMax; x += cellSize.x )
                    {
                        if( hasMinorLines )
                        {
                            bool majorLine = count++ % majorLineOn == 0;
                            GL.Color( majorLine ? majorColor : minorColor );
                        }

                        if( x >= rect.x && x <= rect.xMax)
                        {
                            GL.Vertex3( x, rect.y, 0 );
                            GL.Vertex3( x, rect.yMax, 0 );
                        }
                    }
                GL.End();
            }
        }

        public static void DrawUnityStyleGrid( Rect rect, Vector2 offset )
        {
            DrawGrid( rect
                        , new Vector2( 10f, 10f )
                        , offset
                        , GUIStyles.black
                        , GUIStyles.blackAlpha
                        , 10
                    );
        }

        public static void DrawBox( Rect rect, Color color )
        {
            DrawBox( rect, color, rect );
        }

        public static void DrawBox( Rect rect, Color color, Rect clipRect )
        {
            if( Event.current.type != EventType.Repaint ) return;
            if( clipRect.Overlaps( rect ) == false ) return;

            Vector2 min = new Vector2( btMath.Max( rect.x, clipRect.x )
                                     , btMath.Max( rect.y, clipRect.y )
                                );

            Vector2 max = new Vector2( btMath.Min( rect.xMax, clipRect.xMax )
                                     , btMath.Min( rect.yMax, clipRect.yMax )
                                );

            DrawUtility.lineMaterial.SetPass( 0 );
            GL.Begin( GL.LINES );
                GL.Color( color );

                if( rect.y >= clipRect.y )
                {
                    GL.Vertex3( min.x, min.y, 0 );
                    GL.Vertex3( max.x, min.y, 0 );
                }

                if( rect.xMax <= clipRect.xMax )
                {
                    GL.Vertex3( max.x, min.y, 0 );
                    GL.Vertex3( max.x, max.y, 0 );
                }

                if( rect.yMax <= clipRect.yMax )
                {
                    GL.Vertex3( max.x, max.y, 0 );
                    GL.Vertex3( min.x, max.y, 0 );
                }

                if( rect.x >= clipRect.x )
                {
                    GL.Vertex3( min.x, max.y, 0 );
                    GL.Vertex3( min.x, min.y, 0 );
                }
            GL.End();
        }

        public static void DrawLine(Vector2 pointA, Vector2 pointB, Color color, float width, bool antiAlias)
        {
            Color savedColor = GUI.color;
            Matrix4x4 savedMatrix = GUI.matrix;

            if (!lineTex)
            {
                lineTex = new Texture2D(1, 1, TextureFormat.ARGB32, true);
                lineTex.SetPixel(0, 1, Color.white);
                lineTex.Apply();
            }
            if (!aaLineTex)
            {
                aaLineTex = new Texture2D(1, 3, TextureFormat.ARGB32, true);
                aaLineTex.SetPixel(0, 0, new Color(1, 1, 1, 0));
                aaLineTex.SetPixel(0, 1, Color.white);
                aaLineTex.SetPixel(0, 2, new Color(1, 1, 1, 0));
                aaLineTex.Apply();
            }
            if (antiAlias) width *= 3;
            float angle = Vector3.Angle(pointB - pointA, Vector2.right) * (pointA.y <= pointB.y?1:-1);
            float m = (pointB - pointA).magnitude;
            if (m > 0.01f)
            {
                Vector3 dz = new Vector3(pointA.x, pointA.y, 0);

                GUI.color = color;
                GUI.matrix = TranslationMatrix(dz) * GUI.matrix;
                GUIUtility.ScaleAroundPivot(new Vector2(m, width), new Vector3(-0.5f, 0, 0));
                GUI.matrix = TranslationMatrix(-dz) * GUI.matrix;
                GUIUtility.RotateAroundPivot(angle, Vector2.zero);
                GUI.matrix = TranslationMatrix(dz + new Vector3(width / 2, -m / 2) * btMath.Sin(angle * btMath.Deg2Rad)) * GUI.matrix;

                if (!antiAlias)
                    GUI.DrawTexture(new Rect(0, 0, 1, 1), lineTex);
                else
                    GUI.DrawTexture(new Rect(0, 0, 1, 1), aaLineTex);
            }

            GUI.matrix = savedMatrix;
            GUI.color = savedColor;
        }

        public static void BezierLine(Vector2 start, Vector2 startTangent, Vector2 end, Vector2 endTangent, Color color, float width, bool antiAlias, int segments)
        {
            Vector2 lastV = CubeBezier(start, startTangent, end, endTangent, 0);
            for (int i = 1; i <= segments; ++i)
            {
                Vector2 v = CubeBezier(start, startTangent, end, endTangent, i/(float)segments);

                DrawLine(
                    lastV,
                    v,
                    color, width, antiAlias);
                lastV = v;
            }
        }

        private static Vector2 CubeBezier(Vector2 s, Vector2 st, Vector2 e, Vector2 et, float t){
            float rt = 1-t;
            float rtt = rt * t;
            return rt*rt*rt * s + 3 * rt * rtt * st + 3 * rtt * t * et + t*t*t* e;
        }

        // private methods ////////////////////////////////////////////////////
        public static Material CreateMaterial( string shaderName )
        {
            Material result = new Material( Shader.Find( shaderName ) );
            result.hideFlags = HideFlags.HideAndDontSave;
            result.shader.hideFlags = HideFlags.HideAndDontSave;
            return result;
        }

        private static Matrix4x4 TranslationMatrix(Vector3 v)
        {
            return Matrix4x4.TRS(v,Quaternion.identity,Vector3.one);
        }
    }
}
