//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    [System.Serializable]
    public class LockStack
        : CustomYieldInstruction
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public bool locked                                      { get { return count > 0; } }
        public int count                                        { get; private set; }
        public override bool keepWaiting                        { get { return locked; } }

        //
        // constructor ////////////////////////////////////////////////////////
        //
        
        public LockStack()
        {
            count = 0;
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void Lock()
        {
            ++count;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void Unlock()
        {
            count = System.Math.Max(count-1, 0);
        }
        
        //
        // operators //////////////////////////////////////////////////////////
        //

        public static implicit operator bool(LockStack stack)
        {
            return stack.locked;
        }
    }
}
