//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace blacktriangles
{
    public static class AssetDb
    {
        public static List<T> LoadAllAssetsInPath<T>(string path)
            where T: Object
        {
            List<T> result = new List<T>();
            #if UNITY_EDITOR
            Dbg.Assert(Directory.Exists(path), $"Could not find directory {path}");
            FileUtility.ForEachFile(path, (System.IO.FileInfo info)=>{
                string relpath = FileUtility.MakePathRelativeToAssetDir(info.FullName);
                T loaded = (T)UnityEditor.AssetDatabase.LoadAssetAtPath(relpath, typeof(T));
                if(loaded != null)
                {
                    result.Add(loaded);
                }
            });
            #endif

            return result;
        }
    }
}
