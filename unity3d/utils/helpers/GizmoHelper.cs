//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    public static class GizmoHelper
    {
        public static void DrawArrow(Vector3 start, Vector3 end, float size)
        {
            Vector3 diff = (end - start).normalized;
            Vector3 cross = Vector3.Cross(diff,Vector3.up) * size;

            Gizmos.DrawLine(start + cross, end);
            Gizmos.DrawLine(start - cross, end);
            Gizmos.DrawLine(start + cross, start - cross);
        }
    }
}
