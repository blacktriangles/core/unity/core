//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using System.Collections.Generic;

public static class BoundsExtension
{
    public static Vector3 RandomInBounds( this Bounds self, System.Random rng )
    {
        return new Vector3( rng.Next(self.min.x, self.max.x),
                            rng.Next(self.min.y, self.max.y),
                            rng.Next(self.min.z, self.max.z));
    }
} 
