//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;

namespace blacktriangles
{
    public static class RectExtension
    {
        public static bool ConstrainTo( this ref Rect self, Rect parent )
        {
            bool constrained = self.x < parent.xMin ||
                               self.x > parent.xMax ||
                               self.y < parent.yMin ||
                               self.y > parent.yMax;
                                
            self.x = btMath.Clamp(self.x, parent.xMin, parent.xMax - self.width);
            self.y = btMath.Clamp(self.y, parent.yMin, parent.yMax - self.height);

            return constrained;
        }
    }
}
