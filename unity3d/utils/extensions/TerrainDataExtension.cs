//
// blacktriangles (c) 2018
//

using UnityEngine;

namespace blacktriangles
{
    public static class TerrainDataExtension
    {
        //
        // --------------------------------------------------------------------
        //

        public static float[,] GetHeights(this TerrainData self, CardinalDirection dir, int size)
        {
            switch(dir)
            {
            case CardinalDirection.North: return self.GetHeights(0, self.heightmapResolution-size, self.heightmapResolution, size);
            case CardinalDirection.South: return self.GetHeights(0, 0, self.heightmapResolution, size);
            case CardinalDirection.East: return self.GetHeights(self.heightmapResolution-size, 0, size, self.heightmapResolution);
            case CardinalDirection.West: return self.GetHeights(0, 0, size, self.heightmapResolution);
            }

            return null;
        }

        //
        // --------------------------------------------------------------------
        //

        public static void SetHeights(this TerrainData self, float[,] heights, CardinalDirection dir)
        {
            switch(dir)
            {
            case CardinalDirection.North: self.SetHeights(0,self.heightmapResolution-heights.GetLength(0),heights); break;
            case CardinalDirection.South: self.SetHeights(0,0,heights); break;
            case CardinalDirection.East: self.SetHeights(self.heightmapResolution-heights.GetLength(1),0,heights); break;
            case CardinalDirection.West: self.SetHeights(0,0,heights); break;
            }
        }

        //
        // end class //////////////////////////////////////////////////////////
        //
    }
}
