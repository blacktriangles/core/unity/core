//
// (c) BLACKTRIANGLES 2021
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    public static class CharacterControllerExtension
    {
        public static void Teleport( this CharacterController self, Vector3 position)
        {
            self.gameObject.SetActive(false);
            self.transform.position = position;
            self.gameObject.SetActive(true);
        }
    }
}
