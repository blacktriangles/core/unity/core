//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

#if UNITY_EDITOR
//#define BT_DEBUG
#endif

using System.Collections.Generic;
using UnityEngine;

namespace blacktriangles
{
    //
    // ------------------------------------------------------------------------
    //
    
    public static class PathGenerator
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        [System.Serializable]
        public struct Settings
        {
            public bool ordinalDir;
        }

        //
        // --------------------------------------------------------------------
        //

        public class PathfindingResult
        {
            public List<IPathNode> path;
            #if BT_DEBUG
            public SearchNode goal;
            public SearchNode[,] computed;
            #endif
        }

        //
        // --------------------------------------------------------------------
        //
        
        public struct SearchResult
        {
            public IPathNode node;
            public IntVec2 index;
        }

        //
        // --------------------------------------------------------------------
        //

        public class SearchNode
        {
            public IPathNode data                               = null;
            public float fscore                                 = 0f;
            public float gscore                                 = 0f;
            public float hscore                                 = 0f;
            public IntVec2 index                                = IntVec2.negone;
            public IntVec2 prev                                 = IntVec2.negone;

            public SearchNode Clone()
            {
                return new SearchNode() {
                    data = data,
                    fscore = fscore,
                    gscore = gscore,
                    hscore = hscore,
                    index = index,
                    prev = prev
                };
            }

            public void Reset()
            {
                data = null;
                fscore = System.Single.PositiveInfinity; 
                gscore = System.Single.PositiveInfinity; 
                hscore = System.Single.PositiveInfinity; 
                index = IntVec2.negone;
                prev = IntVec2.negone;
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        public struct State
        {
            public SearchNode current;
            public List<SearchNode> open;
            public SearchNode[,] searched;
            public SearchNode goal;
            public PathfindingResult result;
        }

        //
        // members ////////////////////////////////////////////////////////////
        //
        
        //
        // public methods /////////////////////////////////////////////////////
        //

        public static SearchResult FindClosestNode(Vector3 pos, IPathNode[,] graph)
        {
            SearchResult result = new SearchResult() {
                node = null,
                index = IntVec2.zero
            };

            // find x component
            float dist = System.Single.PositiveInfinity;
            for(int y = 0; y < graph.GetLength(1); ++y)
            {
                for(int x = 0; x < graph.GetLength(0); ++x)
                {
                    IPathNode node = graph[x,y];
                    if(node != null)
                    {
                        float checkDist = (pos - node.worldpos).sqrMagnitude;
                        if(checkDist < dist)
                        {
                            dist = checkDist;
                            result.index = new IntVec2(x,y);
                            result.node = node;
                        }
                    }
                }
            }

            return result;
        }

        //
        // --------------------------------------------------------------------
        //

        // similar to findclosestnode but searches two points at the same time
        // a minor optimization for a common case for path generation
        public static SearchResult[] FindStartAndEndpoint(Vector3 start, Vector3 end, IPathNode[,] graph)
        {
            // initialize the results with an invalid index
            SearchResult[] result = new SearchResult[2];
            result[0].index.x = -1;
            result[1].index.x = -1;

            // find x components
            float startDist = Mathf.Infinity;
            float endDist = Mathf.Infinity;
            for(int y = 0; y < graph.GetLength(1); ++y)
            {
                for(int x = 0; x < graph.GetLength(0); ++x)
                {
                    IntVec2 idx = new IntVec2(x,y);
                    IPathNode node = graph[x,y];
                    if(node != null)
                    {
                        float checkStartDist = (start - node.worldpos).sqrMagnitude;
                        if(checkStartDist < startDist)
                        {
                            startDist = checkStartDist;
                            result[0].index = idx;
                            result[0].node = node;
                        }

                        float checkEndDist = (end - node.worldpos).sqrMagnitude;
                        if(checkEndDist < endDist)
                        {
                            endDist = checkEndDist;
                            result[1].index = idx;
                            result[1].node = node;
                        }
                    }
                }
            }

            return result;
        }

        //
        // --------------------------------------------------------------------
        //

        public static IEnumerator<State> GeneratePath(Vector3 start, 
                                        Vector3 goal, 
                                        IPathNode[,] graph, 
                                        IPathStrategy strategy)
        {
            SearchResult[] search = FindStartAndEndpoint(start, goal, graph);
            return GeneratePath(search[0].index, search[1].index, graph, strategy);
        }

        //
        // ------------------------------------------------------------------------
        //
        
        public static IEnumerator<State> GeneratePath(IntVec2 startIdx, 
                                                      IntVec2 goalIdx, 
                                                      IPathNode[,] graph, 
                                                      IPathStrategy strategy)
        {
            if( graph == null ||
                strategy == null || 
                !graph.IsValidIndex(startIdx) || !graph.IsValidIndex(goalIdx)
              )
            {
                yield break;
            }

            Settings settings = strategy.settings;
            State state = new State();

            IPathNode start = graph[startIdx.x, startIdx.y];
            IPathNode goal = graph[goalIdx.x, goalIdx.y];

            if(startIdx == goalIdx)
            {
                state.result = new PathfindingResult() {
                    path = new List<IPathNode>() {
                        start
                    }
                };

                yield return state;
                yield break;
            }

            // create a list of nodes to be searched
            state.open = new List<SearchNode>();

            // create an array of nodes already searched
            state.searched = new SearchNode[graph.GetLength(0), graph.GetLength(1)];

            // the found node (if any)
            state.goal = new SearchNode();
            state.goal.Reset();

            //
            // helper action to create and insert a new search node based on
            // the current node and the next index to use.
            //

            System.Action<SearchNode, IntVec2> OpenIndex = (SearchNode current, IntVec2 nextIdx)=>{
                if(graph.IsValidIndex(nextIdx))
                {
                    IPathNode node = graph[nextIdx.x, nextIdx.y];
                    if(node != null)
                    {
                        SearchNode next = new SearchNode();
                        next.Reset();
                        next.index = nextIdx;
                        next.data = node;
                        next.prev = current == null ? IntVec2.negone : current.index;
                        next.gscore = current == null ? 0f : current.fscore + 1f;
                        next.hscore = current == null ? 0f : strategy.Score(current, next, goal);
                        next.fscore = next.gscore + next.hscore;

                        // only check this node if it has a viable fscore and 
                        // that fscore is lower than the one that currently
                        // ends at the goal
                        if(next.fscore < System.Single.PositiveInfinity && next.fscore < state.goal.fscore)
                        {
                            // have we previously visited this node? only continue
                            // if we have a better path
                            SearchNode other = state.searched[nextIdx.x, nextIdx.y];
                            if(other == null || next.gscore < other.gscore)
                            {
                                state.searched[nextIdx.x, nextIdx.y] = next;
                                if(nextIdx == goalIdx)
                                {
                                    state.goal = next;
                                }
                                else
                                {
                                    state.open.SortedInsert(next, (lhs,rhs)=>{
                                        return lhs.fscore < rhs.fscore;
                                    });
                                }
                            }
                        }
                    }
                }
            };

            //
            // add our start node to the open list
            //

            OpenIndex(null, startIdx);

            //
            // iterate through all nodes
            //

            while(state.open.Count > 0 && state.goal.data == null)
            {
                // get the lowest scored node in the open list
                state.current = state.open.Pop();

                // search neighbors
                if(settings.ordinalDir)
                {
                    foreach(OrdinalDirection dir in EnumUtility.GetValues<OrdinalDirection>())
                    {
                        IntVec2 nextIdx = state.current.index + dir;
                        OpenIndex(state.current, nextIdx);
                        yield return state;
                    }
                }
                else
                {
                    foreach(CardinalDirection dir in EnumUtility.GetValues<CardinalDirection>())
                    {
                        IntVec2 nextIdx = state.current.index + dir;
                        OpenIndex(state.current, nextIdx);
                        yield return state;
                    }
                }
            }


            //
            // walk backwards over the discovered path and populate the return list
            //

            state.result = new PathfindingResult();
            if(state.goal != null)
            {
                state.result.path = new List<IPathNode>() { state.goal.data };

                SearchNode iter = state.goal;
                while(iter != null && state.searched.IsValidIndex(iter.prev))
                {
                    iter = state.searched[iter.prev.x, iter.prev.y];
                    if(iter != null && iter.data != null)
                    {
                        state.result.path.Push(iter.data);
                    }
                }
            }

            //
            // return all the search nodes back to the pool
            //

            #if BT_DEBUG
            state.result.goal = state.goal.Clone();
            state.result.computed = new SearchNode[state.searched.GetLength(0), state.searched.GetLength(1)];
            for(int y = 0; y < state.searched.GetLength(1); ++y)
            {
                for( int x = 0; x < state.searched.GetLength(0); ++x)
                {
                    // make sure you reset before returning it, or else the next
                    // taker may get unintended variables set
                    SearchNode node = state.searched[x,y];
                    if(node != null)
                    {
                        state.result.computed[x,y] = node.Clone();
                    }
                }
            }
            #endif

            yield return state;
        }

        //
        // debug functions ////////////////////////////////////////////////////
        //

        #if UNITY_EDITOR
        public static void DrawGizmos(PathfindingResult path)
        {
            #if BT_DEBUG
            //
            // draw computed path
            //
            DrawLineage(path.goal, path.computed, Color.green, 1f);
            
            //
            // draw all nodes and edges
            //
            DrawSearchNodes(path.computed);
            #endif
        }

        //
        // --------------------------------------------------------------------
        //

        public static void DrawGizmos(State state)
        {
            if(state.current != null && state.current.data != null)
            {
                Gizmos.color = Color.blue;
                Gizmos.DrawCube(state.current.data.worldpos, Vector3.one * 2f);
            }

            if(state.open != null)
            {
                Gizmos.color = Color.yellow;
                foreach(SearchNode node in state.open)
                {
                    if(node != null & node.data != null)
                    {
                        Gizmos.DrawCube(node.data.worldpos, Vector3.one * 1.5f);
                    }
                }
            }

            DrawSearchNodes(state.searched);

            if(state.goal != null && state.goal.data != null)
            {
                Gizmos.color = Color.green;
                Gizmos.DrawCube(state.goal.data.worldpos, Vector3.one * 2f);
                DrawLineage(state.goal, state.searched, Color.green, 1f);
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public static void DrawLineage(SearchNode node, SearchNode[,] graph, Color color, float size)
        {
            if(node == null) return;

            while(node != null && node.data != null && graph.IsValidIndex(node.prev))
            {
                SearchNode prev = graph[node.prev.x, node.prev.y];
                if(prev != null & prev.data != null)
                {
                    Gizmos.color = color;
                    GizmoHelper.DrawArrow(prev.data.worldpos, node.data.worldpos, size);
                }

                node = prev;
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public static void DrawSearchNodes(SearchNode[,] nodes)
        {
            if(nodes == null) return;
            for(int y = 0; y < nodes.GetLength(1); ++y)
            {
                for(int x = 0; x < nodes.GetLength(0); ++x)
                {
                    SearchNode node = nodes[x,y];
                    if(node != null && node.data != null)
                    {
                        Gizmos.color = Color.black;
                        Gizmos.DrawCube(node.data.worldpos, Vector3.one);

                        if(nodes.IsValidIndex(node.prev))
                        {
                            SearchNode prev = nodes[node.prev.x, node.prev.y];
                            if(prev != null && prev.data != null)
                            {
                                GizmoHelper.DrawArrow(prev.data.worldpos, node.data.worldpos, 0.5f);
                            }
                        }
                    }
                }
            }
        }
        #endif
    }
}
