//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

namespace blacktriangles
{
    public interface IPathStrategy
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        PathGenerator.Settings settings                         { get; }

        //
        // interface methods //////////////////////////////////////////////////
        //

        float Score(PathGenerator.SearchNode last, 
                    PathGenerator.SearchNode next, 
                    IPathNode goal);
    }
}
