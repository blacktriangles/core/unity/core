//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    public class TacticsNavigationGraph
        : BaseNavigationGraph
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public TacticsPathStrategy pathStrategy;

        public override IPathStrategy strategy                  { get { return pathStrategy; } }

        //
        // protected methods //////////////////////////////////////////////////
        //
        
        protected override IPathNode[,] CreateGraph(IntVec2 dims)
        {
            return new TacticsPathNode[dims.x,dims.y];
        }

        //
        // --------------------------------------------------------------------
        //

        protected override IPathNode CreateNode(Vector3 worldpos)
        {
            return new TacticsPathNode() {
                worldpos = worldpos
            };
        }
    }
}

