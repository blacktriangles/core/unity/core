//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    [System.Serializable]
    public class TacticsPathStrategy
        : IPathStrategy
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public PathGenerator.Settings settings                  { get { return _settings; } }
        public PathGenerator.Settings _settings;

        //
        // IPathStrategy //////////////////////////////////////////////////////
        //

        public float Score(PathGenerator.SearchNode last, PathGenerator.SearchNode next, IPathNode goal)
        {
            TacticsPathNode lastNode = last.data as TacticsPathNode;
            TacticsPathNode nextNode = next.data as TacticsPathNode;

            float jumpHeight = Mathf.Max(0, nextNode.height - lastNode.height);
            if(jumpHeight > 3.5f) return System.Single.PositiveInfinity;

    
            float distFromGoal = (goal.worldpos - nextNode.worldpos).sqrMagnitude;

            return jumpHeight + distFromGoal * distFromGoal;
        }
    }
}
