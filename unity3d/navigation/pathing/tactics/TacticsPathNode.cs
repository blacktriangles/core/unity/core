//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    [System.Serializable]
    public class TacticsPathNode
        : IPathNode
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public Vector3 worldpos                                 { get { return _worldPos; } set { _worldPos = value; } }
        public float height                                     { get { return worldpos.y; } }

        [SerializeField] private Vector3 _worldPos;
        
    }
}
