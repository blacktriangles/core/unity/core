//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Collections.Generic;
using UnityEngine;

namespace blacktriangles
{
    public abstract class BaseNavigationGraph
        : MonoBehaviour
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        [System.Serializable]
        public struct ScanSettings
        {
            public float nodeSize;
            public IntVec2 scanArea;
            public Vector3 offset;
            public float scanDistance;
            public LayerMask pathable;
            public LayerMask unpathable;

            public GameObject debug;
        }
        
        //
        // members ////////////////////////////////////////////////////////////
        //

        [Header("Runtime")]
        public ScanSettings settings;
        public Bounds bounds                                    { get { return GetBounds(); } }

        public abstract IPathStrategy strategy                  { get; }
        private IPathNode[,] nodes                              = null;

        #if UNITY_EDITOR
        [Header("Debug")]
        public bool enableBoundsViz      = true;
        public bool enableNodeViz        = false;
        #endif

        //
        // public methods /////////////////////////////////////////////////////
        //

        public PathGenerator.PathfindingResult GeneratePath(Vector3 start, Vector3 goal)
        {
            if(nodes == null) return null;
            IEnumerator<PathGenerator.State> iter = PathGenerator.GeneratePath(start, goal, nodes, strategy);

            PathGenerator.PathfindingResult result = null;
            while(iter.MoveNext())
            {
                if(iter.Current.result != null)
                {
                    result = iter.Current.result;
                }
            }

            return result;
        }

        //
        // --------------------------------------------------------------------
        //

        [ContextMenu("Scan")]
        public void Scan()
        {
            nodes = CreateGraph(settings.scanArea);

            Vector2 halfSize = settings.scanArea * settings.nodeSize / 2f;

            for(int y = 0; y < settings.scanArea.y; ++y)
            {
                for(int x = 0; x < settings.scanArea.x; ++x)
                {
                    Vector3 startPos = settings.offset + new Vector3(x*settings.nodeSize, settings.scanDistance, y*settings.nodeSize) - halfSize.ToVector3XZ();
                    RaycastHit hit;
                    if(Physics.Raycast(startPos, Vector3.down, out hit, settings.scanDistance+1f, settings.pathable + settings.unpathable))
                    {
                        int layer = 1 << hit.collider.gameObject.layer;
                        if((layer & settings.pathable.value) == layer)
                        {
                            nodes[x,y] = CreateNode(hit.point);
                        }
                    }
                }
            }
        }

        //
        // protected methods //////////////////////////////////////////////////
        //

        protected abstract IPathNode[,] CreateGraph(IntVec2 dims);

        //
        // --------------------------------------------------------------------
        //

        protected abstract IPathNode CreateNode(Vector3 worldpos);

        //
        // private methods ////////////////////////////////////////////////////
        //
        
        private Bounds GetBounds()
        {
            return new Bounds() {
                center = transform.position + settings.offset,
                size = new Vector3(settings.scanArea.x * settings.nodeSize, settings.scanDistance + 1f, settings.scanArea.y * settings.nodeSize),
            };
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        #if UNITY_EDITOR
        protected virtual void OnDrawGizmosSelected()
        {
            //
            // draw bounding box
            //

            if(enableBoundsViz)
            {
                Bounds b = bounds;
                Gizmos.color = Color.green;
                Gizmos.DrawWireCube(b.center, b.size);
            }

            if(nodes == null || !enableNodeViz) return;
            foreach(IPathNode node in nodes)
            {
                if(node != null)
                {
                    Gizmos.color = Color.blue;
                    Gizmos.DrawCube(node.worldpos, Vector3.one*0.5f);
                }
            }
        }
        #endif
    }
}
