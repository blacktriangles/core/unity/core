//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Collections.Generic;
using UnityEngine;

namespace blacktriangles
{
    public static class NavigationMeshGenerator
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        [System.Serializable]
        public struct Config
        {
            public float maxWalkableAngle;
        }

        //
        // --------------------------------------------------------------------
        //

        [System.Serializable]
        public class TransformMesh
        {
            public Mesh mesh;
            public Transform xform;
        }

        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public static List<TransformMesh> GenerateWalkableSurfaces(IEnumerable<TransformMesh> meshes, Config config)
        {
            var walkable = new List<TransformMesh>();
            foreach(TransformMesh mesh in meshes)
            {
                TransformMesh walksurf = GenerateWalkableSurface(mesh, config);
                if(walksurf != null)
                {
                    walkable.Add(walksurf);
                }
            }

            return walkable;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public static TransformMesh GenerateWalkableSurface(TransformMesh xmesh, Config config)
        {
            #if USE_HASHSET
                var vertices = new OrderedHashSet<Vector3>();
            #else
                var vertices = new List<Vector3>();
            #endif

            var triangles = new List<int>();

            Transform xform = xmesh.xform;
            Mesh mesh = xmesh.mesh;

            float maxWalkAngle = Mathf.Cos(config.maxWalkableAngle/180f*Mathf.PI);
            int triCount = mesh.triangles.Length / 3;
            for(int tri = 0; tri < triCount; ++tri)
            {
                int triIdx = tri * 3;
                int vidx0 = mesh.triangles[triIdx];
                int vidx1 = mesh.triangles[triIdx+1];
                int vidx2 = mesh.triangles[triIdx+2];

                Vector3 v0 = mesh.vertices[vidx0];
                Vector3 v1 = mesh.vertices[vidx1];
                Vector3 v2 = mesh.vertices[vidx2];

                Vector3 v0world = xform.TransformPoint(v0);
                Vector3 v1world = xform.TransformPoint(v1);
                Vector3 v2world = xform.TransformPoint(v2);

                Vector3 norm = btMath.CalcTriangleNormal(v0world, v1world, v2world);
                if(norm.y > maxWalkAngle)
                {
                    #if USE_HASHSET
                        triangles.Add(vertices.Add(v0));
                        triangles.Add(vertices.Add(v1));
                        triangles.Add(vertices.Add(v2));
                    #else
                        vertices.Add(v0);
                        triangles.Add(vertices.Count-1);
                        vertices.Add(v1);
                        triangles.Add(vertices.Count-1);
                        vertices.Add(v2);
                        triangles.Add(vertices.Count-1);
                    #endif
                }
            }

            TransformMesh result = null;
            if(vertices.Count > 0 && triangles.Count > 0)
            {
                Mesh newMesh = new Mesh();
                newMesh.vertices = vertices.ToArray();
                newMesh.triangles = triangles.ToArray();
                result = new TransformMesh() {
                    mesh = newMesh,
                    xform = xform
                };
            }
            else
            {
                Dbg.Log("NO WALKABLE SURFACES");
            }

            return result;
        }
    }
}
