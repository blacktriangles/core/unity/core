//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    [ExecuteInEditMode]
    public class Billboard
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public btCamera cam;
        public bool aroundZOnly                                 = false;
        
        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void LateUpdate()
        {
            if(cam == null && BaseSceneManager.instance != null)
            {
                cam = BaseSceneManager.instance.sceneCam;
            }

            if(cam != null)
            {
                Vector3 forward = cam.unityCamera.transform.forward;
                if(aroundZOnly)
                {
                    forward.y = 0f;
                }

                transform.forward = forward;
            }
        }
    }
}
