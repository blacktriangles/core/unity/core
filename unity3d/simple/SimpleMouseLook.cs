//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    public class SimpleMouseLook
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        [SerializeField] Vector2 lookSpeed                      = new Vector2(10f, 10f);
        [SerializeField] Range<float> verticalRange             = new Range<float>(-90f, 90f);

        private Vector2 lookAngles                              = Vector2.zero;
        
        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void LateUpdate()
        {
            Vector2 deltaLook = new Vector2(
                Input.GetAxis("Mouse Horizontal") * lookSpeed.x,
                Input.GetAxis("Mouse Vertical") * lookSpeed.y
            ) * Time.deltaTime;

            lookAngles.x = (lookAngles.x + deltaLook.x) % 360;
            lookAngles.y = verticalRange.Clamp(lookAngles.y + deltaLook.y);

            transform.rotation = Quaternion.Euler(lookAngles.y, lookAngles.x, 0f);
        }
    }
}
