//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    public class SpaceSkybox
        : MonoBehaviour
    {
        //
        // types //////////////////////////////////////////////////////////////////
        //

        [System.Serializable]
        public struct Settings
        {
            [Header("General")]
            public int seed;
            public Material spaceMaterial;

            [Header("Starfield")]
            public int starfieldDims;
            public float starfieldDensity;
            public float starfieldBrightness;
        }

        //
        // members ////////////////////////////////////////////////////////////////
        //

        public Settings settings;
        
        [Header("Read Only")]
        [ReadOnly] public Cubemap starfield;
        [ReadOnly] public Texture2D starfield2d;
        public System.Random rng                                    { get; private set; }

        //
        // public methods /////////////////////////////////////////////////////////
        //

        [ContextMenu("Initialize")]
        public virtual void Initialize()
        {
            // we create a cube map, which is 1x6 of starfield dims
            settings.spaceMaterial.SetFloat("_Seed", settings.seed);
            rng = new System.Random(settings.seed);

            // step 1 create a random starfield cubemap to be used in the shader
            starfield = new Cubemap(settings.starfieldDims, TextureFormat.RGBA32, false);
            int pixelCount = settings.starfieldDims * settings.starfieldDims;

            starfield2d = null;
            
            // for each side
            CubemapFace[] faces = EnumUtility.GetValues<CubemapFace>();
            foreach(CubemapFace face in faces)
            {
                if(face == CubemapFace.Unknown) continue;
                Color[] pixels = new Color[settings.starfieldDims*settings.starfieldDims];
                int count = (int)btMath.Floor(settings.starfieldDensity * pixelCount);
                for(int i = 0; i < count; ++i)
                {
                    int idx = (int)btMath.Floor(rng.NextFloat() * pixelCount);
                    float color = btMath.Log(1 - rng.NextFloat()) * -settings.starfieldBrightness;
                    pixels[idx] = new Color(color, color, color, 1f);
                }

                starfield.SetPixels(pixels, face, 0);
                if(starfield2d == null)
                {
                    starfield2d = new Texture2D(settings.starfieldDims, settings.starfieldDims);
                    starfield2d.SetPixels(pixels);
                }
            }

            starfield.Apply(false);
            settings.spaceMaterial.SetTexture("_Starfield", starfield);

            starfield2d.Apply(false);
            settings.spaceMaterial.SetTexture("_Starfield2d", starfield2d);
        }

        //
        // editor /////////////////////////////////////////////////////////////
        //

        #if UNITY_EDITOR
        [ContextMenu("Save Cubemap")]
        private void SaveCubemap()
        {
            UnityEditor.AssetDatabase.CreateAsset(starfield, "Assets/Starfield RNG.cubemap");
            UnityEditor.AssetDatabase.CreateAsset(starfield2d, "Assets/Starfield2d RNG.asset");
        }
        #endif
        
    }
}
