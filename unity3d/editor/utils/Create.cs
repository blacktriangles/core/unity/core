//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEditor;
using System;
using System.IO;

namespace blacktriangles
{
    public static class Create
    {
        //
        // utility functions //////////////////////////////////////////////////
        //

        public static string GetDefaultAssetPath( string name, string extension )
        {
            string result = AssetDatabase.GetAssetPath( Selection.activeObject );
            if( result == System.String.Empty )
            {
                result = "Assets";
            }
            else if( Path.GetExtension( result ) != System.String.Empty )
            {
                result = result.Replace( Path.GetFileName( AssetDatabase.GetAssetPath( Selection.activeObject )), System.String.Empty );
            }

            result = AssetDatabase.GenerateUniqueAssetPath( result + "/" + name + "." + extension );

            return result;
        }

        //
        // create /////////////////////////////////////////////////////////////
        //
        
        public static T Asset<T>()
            where T : ScriptableObject
        {
            string path = GetDefaultAssetPath( typeof(T).Name, "asset" );
            return Asset<T>(path, true);
        }

        //
        // --------------------------------------------------------------------
        //

        public static T Asset<T>( System.String path )
            where T : ScriptableObject
        {
            return Asset<T>( path, true );
        }

        //
        // --------------------------------------------------------------------
        //

        public static T Asset<T>( System.String path, bool selectNewAsset )
            where T : ScriptableObject
        {
            FileUtility.EnsureDirectoryExists( System.IO.Path.GetDirectoryName( path ) );
            T asset = ScriptableObject.CreateInstance<T>();
            Asset( (UnityEngine.Object)asset, path, selectNewAsset );
            return asset;
        }

        //
        // --------------------------------------------------------------------
        //

        public static ScriptableObject Asset( System.Type type )
        {
            string path = GetDefaultAssetPath( type.ToString(), "asset" );
            return Asset( type, path, true );
        }

        //
        // --------------------------------------------------------------------
        //

        public static ScriptableObject Asset( System.Type type, string path, bool selectNewAsset )
        {
            ScriptableObject asset = ScriptableObject.CreateInstance( type );
            if( asset != null )
            {
                Asset( (UnityEngine.Object)asset, path, selectNewAsset );
            }

            return asset;
        }

        //
        // --------------------------------------------------------------------
        //

        public static void Asset( UnityEngine.Object asset, string path, bool selectNewAsset )
        {
            AssetDatabase.CreateAsset( asset, path );
            AssetDatabase.SaveAssets();
            if( selectNewAsset )
            {
                EditorUtility.FocusProjectWindow();
                Selection.activeObject = asset;
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public static string TexturePNG( Texture2D texture, string pathHint, bool selectNewAsset )
        {
            string dir = "Assets/";
            string name = "PackedMask.png";

            if(pathHint != System.String.Empty)
            {
                dir = System.IO.Path.GetDirectoryName(pathHint);
                name = System.IO.Path.GetFileName(pathHint);
            }

            string path = EditorUtility.SaveFilePanel("Save Texture", dir, name, "png");
            if(path.Length > 0)
            {
                byte[] pngData = ImageConversion.EncodeToPNG(texture);
                if(pngData != null)
                {
                    System.IO.File.WriteAllBytes(path, pngData);
                    AssetDatabase.Refresh();
                }
            }
            return path;
        }
    }
}
