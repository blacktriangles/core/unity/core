//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace blacktriangles
{
    public static class EditorSelection
    {
        //
        // --------------------------------------------------------------------
        //
        
        public static T[] GetSelection<T>(SelectionMode mode = SelectionMode.Assets)
            where T: UnityEngine.Object
        {
            return System.Array.ConvertAll<Object,T>(Selection.GetFiltered(typeof(T), mode), (obj)=>{
                return (T)obj;
            });
        }

        //
        // --------------------------------------------------------------------
        //

        public static string GetSelectedFolderPath()
        {
            string path = AssetDatabase.GetAssetPath(Selection.activeObject);
            if(path == System.String.Empty)
            {
                path = "Assets";
            }
            else if(System.IO.Path.GetExtension(path) != System.String.Empty)
            {
                path = System.IO.Path.GetDirectoryName(path);
            }

            return path;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public static List<T> GetInFolderPopup<T>(SelectionMode mode = SelectionMode.Assets, bool recurse = true, List<T> reuslt = null)
            where T: UnityEngine.Object
        {
            string path = EditorUtility.OpenFolderPanel("Select a folder", "Assets", "");
            path = FileUtility.MakePathRelativeToAssetDir(path);
            if(path != System.String.Empty)
            {
                return GetInFolder<T>(path, mode, recurse);
            }

            return new List<T>();
        }

        //
        // --------------------------------------------------------------------
        //
        
        public static List<T> GetInFolder<T>(string dir, SelectionMode mode = SelectionMode.Assets, bool recurse = false, List<T> result = null)
            where T: UnityEngine.Object
        {
            if(result == null) result = new List<T>();

            string[] files = System.IO.Directory.GetFiles(dir);
            foreach(string file in files)
            {
                T loaded = (T)AssetDatabase.LoadAssetAtPath(file, typeof(T));
                if(loaded != null)
                {
                    result.Add(loaded);
                }
            }

            // get subdirs
            if(recurse)
            {
                string[] dirs = System.IO.Directory.GetDirectories(dir);
                foreach(string d in dirs)
                {
                    GetInFolder<T>(d, mode, recurse, result);
                }
            }

            return result;
        }

        //
        // --------------------------------------------------------------------
        //

        public static List<T> GetInSelectedFolder<T>(SelectionMode mode = SelectionMode.Assets, bool recurse = false)
            where T: UnityEngine.Object
        {
            string dir = GetSelectedFolderPath();
            return GetInFolder<T>(dir, mode, recurse);
        }
    }
}
