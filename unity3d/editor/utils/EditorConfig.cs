//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEditor;

namespace blacktriangles
{
    public static class EditorConfig
    {
        // members /////////////////////////////////////////////////////////////
        public static readonly string kAssetDir                 = System.IO.Path.GetFullPath( "." ); 
        public static readonly string kRootDir                  = GetRootDir(); 
        public static readonly string kIconDir                  = FileUtility.PathCombineSystemSeparator( kRootDir, "common", "interface", "icons" );
        public static readonly string kTextureDir               = FileUtility.PathCombineSystemSeparator( kRootDir, "unity3d", "utils", "editor", "art", "textures" );
        public static readonly string kTemplatesDir             = FileUtility.PathCombineSystemSeparator( kRootDir, "templates~" );
        public static readonly string kProjectSettingsDir       = FileUtility.PathCombineSystemSeparator( kRootDir, "unity3d", ".projectsettings" );

        // public methods //////////////////////////////////////////////////////
        public static Texture2D GetIcon( string name )
        {
            return GetTexture( kIconDir, name );
        }

        public static Texture2D GetTexture( string name )
        {
            return GetTexture( kTextureDir, name );
        }

        private static Texture2D GetTexture( string directory, string fileName )
        {
            string path = FileUtility.PathCombine( directory, fileName + ".png" );
            Texture2D result = AssetDatabase.LoadAssetAtPath( path, typeof( Texture2D ) ) as Texture2D;

            if( result == null )
            Debug.LogError( "Failed to load Texture at " + path );

            return result;
        }

        // private methods /////////////////////////////////////////////////////
        private static string GetRootDir()
        {
            System.IO.DirectoryInfo searchDir = new System.IO.DirectoryInfo(kAssetDir);
            System.IO.FileInfo[] files = searchDir.GetFiles( "*.BT_CORE_ROOT", System.IO.SearchOption.AllDirectories );
            Dbg.Assert( files.Length == 1, "Could not find a unique .BT_CORE_ROOT file" );
            return System.IO.Path.GetDirectoryName( FileUtility.MakePathRelativeToAssetDir(files[0].FullName) );
        }
    }
}
