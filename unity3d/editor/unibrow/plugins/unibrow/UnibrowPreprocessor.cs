//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEditor;

namespace blacktriangles
{
    public class UnibrowPreprocessor_v2
        : AssetPostprocessor
    {
        // constants //////////////////////////////////////////////////////////

        // members ////////////////////////////////////////////////////////////
        private uint _version                                   = 2;

        // unity callbacks ////////////////////////////////////////////////////
        private static void OnPostprocessAllAssets( string[] importedAssets
                                            , string[] deletedAssets
                                            , string[] movedAssets
                                            , string[] movedFromAssetPaths )
        {
            foreach( string importedAsset in importedAssets )
            {
                if( System.IO.Path.GetExtension( importedAsset ) == ".brow" )
                {
                    Render( importedAsset );
                }
            }
        }

        // public methods /////////////////////////////////////////////////////
        public override int GetPostprocessOrder()
        {
            return 0;
        }

        public override uint GetVersion()
        {
            return _version;
        }

        // private methods ////////////////////////////////////////////////////
        private static JsonObject LoadBrowFile( string assetPath )
        {
            string jsonTxt = System.IO.File.ReadAllText( assetPath );
            JsonObject json = JsonObject.FromString( jsonTxt );

            string templateFile = json.GetField<string>( "#template" );
            if( System.String.IsNullOrEmpty( templateFile ) == false )
            {
                string dir = System.IO.Path.GetDirectoryName( assetPath );
                json["#browPath"] = assetPath;
                json["#templatePath"] = System.IO.Path.Combine( dir, templateFile );

                string relativeOutputFilePath = json.GetField<string>( "#outputPath" );
                string outputPath = System.IO.Path.Combine( dir, relativeOutputFilePath );
                json["#outputPath"] = outputPath;

                if( System.IO.File.Exists( outputPath ) )
                {
                    string backupPath = System.IO.Path.Combine( "Backups/Unibrow", System.IO.Path.GetFileName( relativeOutputFilePath ) );
                    FileUtility.EnsureDirectoryExists( System.IO.Path.GetDirectoryName( backupPath ) );

                    bool overwrite = true;
                    System.IO.File.Copy( outputPath, backupPath, overwrite );
                }

            }
            else
            {
                Debug.LogError( "Could not find #template field in json for brow file " + assetPath );
            }

            return json;
        }

        private static JsonObject Render( string assetPath )
        {
            JsonObject json = LoadBrowFile( assetPath );

            string template = System.IO.File.ReadAllText( json.GetField<string>("#templatePath") );
            string result = Unibrow.Render( template, json );
            System.IO.File.WriteAllText( json.GetField<string>( "#outputPath" ), result );
            AssetDatabase.Refresh();

            return json;
        }
    }
}
