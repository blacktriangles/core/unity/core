//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
// Using modified bezier curve drawing code originally written by Linusmartensson
// http://forum.unity3d.com/threads/drawing-lines-in-the-editor.71979/
//
//=============================================================================

using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace blacktriangles
{
    public static partial class btGui
    {
        // constants ///////////////////////////////////////////////////////////
        public const float kNormalButtonSize                = 18f;
        public const float kRemoveButtonSize                = kNormalButtonSize;

        //
        // glyph ###############################################################
        //
        public static void Glyph( Texture2D icon, float size = kNormalButtonSize )
        {
            GUILayout.Label( icon, GUILayout.Width( size ), GUILayout.Height( size ) );
        }

        //
        // button ##############################################################
        //
        public static bool Button( Texture2D icon, string text )
        {
            return Button( icon, text, GUIStyles.buttonNormal );
        }

        public static bool Button( string text )
        {
            return Button( text, GUIStyles.buttonNormal );
        }

        public static bool Button( Texture2D icon, float size = kNormalButtonSize )
        {
            return Button( icon, GUIStyles.buttonNormal, size );
        }

        public static bool Button( Texture2D icon, GUIStyle style, float size = kNormalButtonSize )
        {
            return Button( new GUIContent( icon ), style, GUILayout.Width( size ), GUILayout.Height( size ) );
        }

        public static bool Button( string text, GUIStyle style, float size = kNormalButtonSize )
        {
            return Button( new GUIContent( text ), style, GUILayout.Height( size ) );
        }

        public static bool Button( Texture2D icon, string text, GUIStyle style, float size = kNormalButtonSize )
        {
            return Button( new GUIContent( " " + text, icon ), style, GUILayout.Height( size ) );
        }

        public static bool Button( GUIContent content, GUIStyle style, params GUILayoutOption[] opts )
        {
            return GUILayout.Button( content, style, opts );
        }
    }
}
