//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
// Using modified bezier curve drawing code originally written by Linusmartensson
// http://forum.unity3d.com/threads/drawing-lines-in-the-editor.71979/
//
//=============================================================================

using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace blacktriangles
{
    public static partial class btGui
    {
        //
        // quick list field ####################################################
        //
        public static void QuickListField<T>( string title, List<T> list )
        {
            QuickListField<T>(
                title, 
                list, 
                (T t)=>{
                    return btGui.ObjectReflectionGUI<T>(t);
                },
                ()=>{
                    return default(T);
                },
                GUIStyles.buttonNormal
            );
        }

        //
        // --------------------------------------------------------------------
        //
        
        public static void QuickListField<T>( string title, List<T> list, System.Func<T,T> guiCallback, System.Func<T> createCallback, GUIStyle buttonStyle )
        {
            GUILayout.BeginVertical();
                if( title != System.String.Empty )
                {
                    GUILayout.Label( title );
                }

                int deadItem = -1;
                for( int i = 0; i < list.Count; ++i )
                {
                    GUILayout.BeginHorizontal(GUIStyles.lightbox);
                        list[i] = guiCallback( list[i] );
                        if( GUILayout.Button( EditorIcons.X, buttonStyle, GUILayout.Width( 18f ), GUILayout.Height( 18f ) ) )
                        {
                            deadItem = i;
                        }
                    GUILayout.EndHorizontal();
                }

                if( deadItem >= 0 )
                {
                    list.RemoveAt( deadItem );
                }

                GUILayout.BeginHorizontal();
                    if( GUILayout.Button( "Clear" ) )
                    {
                        list.Clear();
                    }

                    GUILayout.FlexibleSpace();

                    if( createCallback != null )
                    {
                        if( GUILayout.Button( EditorIcons.Plus, buttonStyle, GUILayout.Width( 18f ), GUILayout.Height( 18f ) ) )
                        {
                            list.Add( createCallback() );
                        }
                    }
                GUILayout.EndHorizontal();
            GUILayout.EndVertical();
        }

        //
        // get null class ######################################################
        //
        public static T GetNullClass<T>()
            where T: class
        {
            return null;
        }

        //
        // get new class ######################################################
        //
        public static T GetNewClass<T>()
            where T: new()
        {
            return new T();
        }
    }
}
