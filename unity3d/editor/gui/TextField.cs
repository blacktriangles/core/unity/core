//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
// Using modified bezier curve drawing code originally written by Linusmartensson
// http://forum.unity3d.com/threads/drawing-lines-in-the-editor.71979/
//
//=============================================================================

using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace blacktriangles
{
    public static partial class btGui
    {
        //
        // textfield ###########################################################
        //
        public static string TextField( string str )
        {
            return GUILayout.TextField( str );
        }

        public static string TextField( GUIContent label, string str )
        {
            if( label == null )
            {
                return TextField( str );
            }

            GUILayout.BeginHorizontal();
                GUILayout.Label( label );
                string result = TextField( str );
            GUILayout.EndHorizontal();
            return result;
        }

        public static string SearchField( string str )
        {
            return SearchField( null, str );
        }

        public static string SearchField( GUIContent label, string str )
        {
            GUILayout.BeginHorizontal();
                str = TextField( label, str );
                if( str.Length > 0 && Button( EditorIcons.X ) )
                {
                    str = System.String.Empty;
                }
                Glyph( EditorIcons.Search, 12f );
            GUILayout.EndHorizontal();

            return str;
        }
    }
}
