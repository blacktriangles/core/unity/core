//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
// Using modified bezier curve drawing code originally written by Linusmartensson
// http://forum.unity3d.com/threads/drawing-lines-in-the-editor.71979/
//
//=============================================================================

using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace blacktriangles
{
    public static partial class btGui
    {
        //
        // serialize object field ##############################################
        //
        public static bool SerializedObjectField( bool expanded, string label, SerializedObject obj, GUIStyle containerStyle )
        {
            GUILayout.BeginVertical( containerStyle );

                expanded = Foldout( expanded, label );

                if( expanded )
                {
                    if( obj == null )
                    {
                        EditorGUILayout.ObjectField( label, null, typeof( UnityEngine.Object ), false );
                    }
                    else
                    {
                        int originalIndent = EditorGUI.indentLevel;
                        SerializedProperty prop = obj.GetIterator();
                        bool working = prop.NextVisible( true );
                        while( working )
                        {
                            EditorGUI.indentLevel = prop.depth + originalIndent + 1;
                            EditorGUILayout.PropertyField( prop );
                            working = prop.NextVisible( prop.hasVisibleChildren && prop.isExpanded );
                        }

                        EditorGUI.indentLevel = originalIndent;
                    }
                }

            GUILayout.EndVertical();

            return expanded;
        }
    }
}
