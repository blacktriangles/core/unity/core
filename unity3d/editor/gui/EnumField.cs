//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEditor;
using UnityEngine;

namespace blacktriangles
{
    public static partial class btGui
    {
        public static T EnumField<T>( string label, T selection )
            where T: System.Enum
        {
            string[] names = EnumUtility.GetNames<T>();
            return EnumUtility.Convert<T>(
                EditorGUILayout.Popup(label, EnumUtility.Convert<T,int>(selection), names)
            );
        }
    }
}
