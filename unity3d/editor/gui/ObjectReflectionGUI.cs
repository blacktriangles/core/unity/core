//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
// Using modified bezier curve drawing code originally written by Linusmartensson
// http://forum.unity3d.com/threads/drawing-lines-in-the-editor.71979/
//
//=============================================================================

using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace blacktriangles
{
    public static partial class btGui
    {
        //
        // calculate height ###################################################
        //

        public static float CalculateReflectedHeight(object obj)
        {
            return CalculateReflectedHeight(obj, System.String.Empty);
        }

        //
        // --------------------------------------------------------------------
        //

        public static float CalculateReflectedHeight(object obj, string label)
        {
            float kLineHeight = EditorGUIUtility.singleLineHeight+0.5f;
            if(obj == null) return kLineHeight;
            System.Type type = obj.GetType();
            float result = 0;
            if( 
                type == typeof( Bounds ) ||
                type == typeof( Color ) ||
                type == typeof( AnimationCurve ) ||
                type.IsEnum ||
                type == typeof( float ) || type == typeof( double ) || type == typeof( decimal ) ||
                type == typeof( int ) ||
                typeof( UnityEngine.Object ).IsAssignableFrom( type ) ||
                type == typeof( Rect ) ||
                type == typeof( System.String ) ||
                type == typeof( bool ) ||
                type == typeof( Vector2 ) ||
                type == typeof( Vector3 ) ||
                type == typeof( Vector4 )
            )
            {
                result = kLineHeight;
            }
            else if( type.IsArray )
            {
                System.Array arr = (System.Array)obj;
                if( arr != null && arr.Length > 0)
                {
                    object item = arr.GetValue(0);
                    result += CalculateReflectedHeight(item) * arr.Length;
                }
            }
            else
            {
                FieldInfo[] fields = type.GetFields();
                foreach( FieldInfo field in fields )
                {
                    bool isPublic = field.IsPublic;
                    bool isSerializeField = field.GetCustomAttributes( typeof(UnityEngine.SerializeField), true ).Length > 0;
                    bool isNonSerialized = field.GetCustomAttributes( typeof(System.NonSerializedAttribute), true ).Length > 0;
                    bool isStatic = field.IsStatic;
                    if( isStatic == false && isNonSerialized == false && ( isSerializeField || isPublic ) )
                    {
                        result += CalculateReflectedHeight(field.GetValue(obj));
                    }
                }
            }

            return result;
        }
        
        //
        // object reflection gui ###############################################
        //

        public static T ObjectReflectionGUI<T>(T obj)
        {
            return (T)ObjectReflectionGUI((object)obj);
        }

        public static object ObjectReflectionGUI(object obj)
        {
            object result = obj;
            Rect position = EditorGUILayout.GetControlRect();
            ObjectReflectionGUI(position, ref result);
            return result;
        }

        //
        // --------------------------------------------------------------------
        //

        public static float ObjectReflectionGUI(Rect position, ref object obj )
        {
            return ObjectReflectionGUI( position, System.String.Empty, ref obj );
        }

        //
        // --------------------------------------------------------------------
        //

        public static float ObjectReflectionGUI( Rect position, string label, ref object obj )
        {
            return ObjectReflectionGUI( position, label, ref obj, null, null );
        }

        //
        // --------------------------------------------------------------------
        //

        public static float ObjectReflectionGUI( Rect position, string label, ref object obj, object DEBUG_parent, System.Type _type )
        {
            float kLineHeight = EditorGUIUtility.singleLineHeight;
            if( obj == null && _type == null )
            {
                EditorGUI.LabelField( position, label, "[null]" );
                return kLineHeight;
            }

            float result = 0f;
            System.Type type = _type ?? obj.GetType();
            if( type == typeof( Bounds ) )
            {
                obj = (object)EditorGUI.BoundsField(position, label, (Bounds)obj);
                result += kLineHeight;
            }
            else if( type == typeof( Color ) )
            {
                obj = (object)EditorGUI.ColorField(position, label, (Color)obj);
                result += kLineHeight;
            }
            else if( type == typeof( AnimationCurve ) )
            {
                obj = (object)EditorGUI.CurveField(position, label, (AnimationCurve)obj);
                result += kLineHeight;
            }
            else if( type.IsEnum )
            {
                obj = (object)EditorGUI.EnumPopup(position, label, (System.Enum)obj);
                result += kLineHeight;
            }
            else if( type == typeof( float ) || type == typeof( double ) || type == typeof( decimal ) )
            {
                obj = (object)EditorGUI.FloatField(position, label, (float)System.Convert.ToSingle(obj) );
                result += kLineHeight;
            }
            else if( type == typeof( int ) )
            {
                obj = (object)EditorGUI.IntField(position, label, (int)obj );
                result += kLineHeight;
            }
            else if( typeof( UnityEngine.Object ).IsAssignableFrom( type ) )
            {
                obj = (object)EditorGUI.ObjectField(position, label, (UnityEngine.Object)obj, type, true );
                result += kLineHeight;
            }
            else if( type == typeof( Rect ) )
            {
                obj = (object)EditorGUI.RectField(position, label, (Rect)obj );
                result += kLineHeight;
            }
            else if( type == typeof( System.String ) )
            {
                obj = (object)EditorGUI.TextField(position, label, (string)obj );
                result += kLineHeight;
            }
            else if( type == typeof( bool ) )
            {
                obj = (object)EditorGUI.Toggle(position, label, (bool)obj);
                result += kLineHeight;
            }
            else if( type == typeof( Vector2 ) )
            {
                obj = (object)EditorGUI.Vector2Field(position, label, (Vector2)obj );
                result += kLineHeight;
            }
            else if( type == typeof( Vector3 ) )
            {
                obj = (object)EditorGUI.Vector3Field(position, label, (Vector3)obj );
                result += kLineHeight;
            }
            else if( type == typeof( Vector4 ) )
            {
                obj = (object)EditorGUI.Vector4Field(position, label, (Vector4)obj );
                result += kLineHeight;
            }
            else if( type.IsArray )
            {
                if(label != System.String.Empty)
                {
                    EditorGUI.LabelField(position, label);
                    result += kLineHeight;
                    position.y += kLineHeight;
                }

                EditorGUI.indentLevel += 1;

                System.Array arr = (System.Array)obj;
                if( arr != null )
                {
                    for( int i = 0; i < arr.Length; ++i )
                    {
                        object item = arr.GetValue( i );
                        ObjectReflectionGUI(position, System.String.Empty, ref item, obj, item.GetType() );
                        arr.SetValue( item, i );
                        result += kLineHeight;
                        position.y += kLineHeight;
                    }

                    EditorGUI.indentLevel -= 1;
                }

                obj = (object)arr;
            }
            else
            {
                if(label != System.String.Empty)
                {
                    EditorGUI.LabelField(position, label);
                    result += kLineHeight;
                    position.y += kLineHeight;
                }

                EditorGUI.indentLevel += 1;

                FieldInfo[] fields = type.GetFields();
                foreach( FieldInfo field in fields )
                {
                    bool isPublic = field.IsPublic;
                    bool isSerializeField = field.GetCustomAttributes( typeof(UnityEngine.SerializeField), true ).Length > 0;
                    bool isNonSerialized = field.GetCustomAttributes( typeof(System.NonSerializedAttribute), true ).Length > 0;
                    bool isStatic = field.IsStatic;
                    if( isStatic == false && isNonSerialized == false && ( isSerializeField || isPublic ) )
                    {
                        // when using Component references on prefabs, it passes the game object around,
                        // so we need to manually GetComponent to get the type we want out of it.
                        object fieldValue = field.GetValue(obj);
                        float offset = ObjectReflectionGUI( position, field.Name, ref fieldValue, obj, field.FieldType );
                        position.y += offset;
                        result += offset;

                        if(field.GetValue(obj) != fieldValue)
                        {
                            if(fieldValue != null)
                            {
                                System.Type resultType = fieldValue.GetType();
                                System.Type desiredType = field.FieldType;
                                if(resultType != desiredType && resultType == typeof(GameObject))
                                {
                                    GameObject go = (GameObject)fieldValue;
                                    fieldValue = go.GetComponent(desiredType);
                                }
                            }

                            field.SetValue( obj, fieldValue );

                            if(obj is UnityEngine.Object uobj)
                            {
                                EditorUtility.SetDirty(uobj);
                            }
                        }
                    }
                }

                EditorGUI.indentLevel -=1;
            }

            return result;
        }
    }
}
