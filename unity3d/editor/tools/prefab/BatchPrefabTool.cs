//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEditor;

namespace blacktriangles
{
    public static class BatchPrefabTool
    {
        // constants //////////////////////////////////////////////////////////
        private const string kTitle                             = "Tools/blacktriangles/Prefab/Batch Prefab Creator";

        // menu items /////////////////////////////////////////////////////////
        [MenuItem(kTitle)]
        private static void CreatePrefabs()
        {
            string prefabDir = EditorUtility.SaveFolderPanel( kTitle, "Assets", System.String.Empty );
            if( prefabDir.Length > 0 )
            {
                prefabDir = FileUtility.MakePathRelativeToAssetDir( prefabDir );
                Object[] objects = Selection.GetFiltered( typeof( GameObject ), SelectionMode.TopLevel | SelectionMode.Editable | SelectionMode.ExcludePrefab );


                float current = 0f;
                foreach( GameObject go in objects )
                {
                    EditorUtility.DisplayProgressBar( System.String.Format( "Creating Prefab {0}/{1}", current, objects.Length ), go.name, current++/objects.Length );
                    string path = FileUtility.PathCombine(prefabDir, go.name + ".prefab");
                    PrefabUtility.SaveAsPrefabAsset(go, path);
                }

                EditorUtility.ClearProgressBar();

                Debug.Log( System.String.Format( "Created {0} prefabs.", objects.Length.ToString() ) );
            }

            AssetDatabase.Refresh();
        }
    }
}
