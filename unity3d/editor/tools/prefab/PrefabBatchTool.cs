//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEditor;
using UnityEngine;

namespace blacktriangles
{
    public class PrefabBatchTool
        : EditorWindow
    {
        //
        // constants //////////////////////////////////////////////////////////
        //

        public const string kMenuPath                           = "Tools/blacktriangles/Prefab/Batch Edit Tool";
        public const string kTitle                              = "Prefab Batch Tool";

        //
        // members ////////////////////////////////////////////////////////////
        //

        private bool selectedUnfolded                           = false;

        private bool scaleUnfolded                              = false;
        private float scale                                     = 1.0f;

        private bool materialUnfolded                           = false;
        private Material material                               = null;

        //
        // constructor ////////////////////////////////////////////////////////
        //

        [MenuItem(kMenuPath)]
        private static void OpenWindow()
        {
            PrefabBatchTool window = GetWindow<PrefabBatchTool>();
            window.titleContent = new GUIContent(kTitle);
            window.Initialize();
        }

        //
        // --------------------------------------------------------------------
        //

        private void Initialize()
        {
        }
        
        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void OnEnable()
        {
            Initialize();
        }

        //
        // --------------------------------------------------------------------
        //

        protected virtual void OnGUI()
        {
            btGui.OnGUI();

            selectedUnfolded = EditorGUILayout.Foldout(selectedUnfolded, "Selected");
            if(selectedUnfolded)
            {
                GUILayout.BeginVertical(GUI.skin.box);
                var targets = Selection.objects;
                foreach(GameObject target in targets)
                {
                    if(target != null)
                    {
                        GUILayout.Label(target.name);
                    }
                }
                GUILayout.EndVertical();
            }

            scaleUnfolded = EditorGUILayout.Foldout(scaleUnfolded, "Scale Tool");
            if(scaleUnfolded)
            {
                GUILayout.BeginVertical( GUI.skin.box );

                scale = EditorGUILayout.FloatField("Scale", scale);

                if(GUILayout.Button("Apply"))
                {
                    foreach(GameObject selection in Selection.objects)
                    {
                        if(selection != null)
                        {
                            var children = selection.GetComponentsInChildren<Transform>();
                            foreach(Transform child in children)
                            {
                                child.localPosition *= scale;
                            }
                        }
                    }
                }
                GUILayout.EndVertical();
            }

            materialUnfolded = EditorGUILayout.Foldout(materialUnfolded, "Material Tool");
            if(materialUnfolded)
            {
                GUILayout.BeginVertical(GUI.skin.box);
                material = EditorGUILayout.ObjectField(
                        "Material", 
                        material as UnityEngine.Object,
                        typeof(Material),
                        false
                    ) as Material;

                if(GUILayout.Button("Apply"))
                {
                    foreach(GameObject selection in Selection.objects)
                    {
                        if(selection != null)
                        {
                            Renderer[] renderers = selection.GetComponentsInChildren<Renderer>();
                            foreach(Renderer rend in renderers)
                            {
                                rend.material = material;
                            }
                        }
                    }
                }
                GUILayout.EndVertical();
            }

            if(GUILayout.Button("Save Prefab"))
            {
                foreach(GameObject selection in Selection.objects)
                {
                    if(selection != null)
                    {
                        selection.UpdatePrefab();
                    }
                }
            }
        }
        //
        // private methods ////////////////////////////////////////////////////
        //
    }
}
