//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEditor;
using System.Text.RegularExpressions;

using System.Collections.Generic;

namespace blacktriangles
{
    public class BatchRenameWindow
        : EditorWindow
    {
        // members //////////////////////////////////////////////////////////////
        private const string kTitle                             = "Batch Renamer";
        private const string kMenuPath                          = "Tools/blacktriangles/Common/Batch Rename Window";

        public string findPattern;
        public string replacePattern;

        private List<GameObject> gos                            = new List<GameObject>();
        private Vector2 scrollPos                               = Vector2.zero;

        // constructor / destructor ///////////////////////////////////////////
        [MenuItem( kMenuPath )]
        private static void OpenWindow()
        {
            BatchRenameWindow window = GetWindow<BatchRenameWindow>();
            window.titleContent = new GUIContent( kTitle );
            window.Initialize();
        }

        private void Initialize()
        {
            gos = new List<GameObject>();
        }

        // unity callbacks ////////////////////////////////////////////////////
        protected virtual void OnEnable()
        {
            Initialize();
        }

        protected virtual void OnGUI()
        {
            btGui.OnGUI();
            GUILayout.BeginVertical( GUI.skin.box );
                scrollPos = GUILayout.BeginScrollView( scrollPos );
                    btGui.QuickListField<GameObject>(
                            "GameObjects",
                            gos,
                            (g)=>{ return btGui.ThinObjectField( System.String.Empty, g, true); },
                            btGui.GetNullClass<GameObject>,
                            GUIStyles.buttonNormal
                        );
                GUILayout.EndScrollView();

                  if( GUILayout.Button( "Get From Selection" ) )
                  {
                      gos.Clear();
                      foreach( GameObject selection in Selection.objects )
                      {
                          if( selection != null )
                          {
                              gos.Add( selection );
                          }
                      }
                  }

            findPattern = EditorGUILayout.TextField( "Find Pattern", findPattern );
            replacePattern = EditorGUILayout.TextField( "Replace Pattern", replacePattern );

            GUILayout.EndVertical();

            if( GUILayout.Button( "Rename" ) )
            {
                RenameAll( gos.ToArray() );
            }
        }

        private void RenameAll( GameObject[] gameObjects )
        {
            foreach( GameObject go in gameObjects )
            {
                go.name = Regex.Replace(go.name, findPattern, replacePattern);
                EditorUtility.SetDirty(go);
            }
        }
    }
}
