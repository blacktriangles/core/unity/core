//
// (c) BLACKTRIANGLES 2021
// http://www.blacktriangles.com
//

using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

namespace blacktriangles
{
    public class MaskSplitter
        : EditorWindow
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        public enum Channel
        {
            Red,
            Green,
            Blue,
            Alpha
        }

        [System.Serializable]
        public struct UseChannel
        {
            public Channel read;
            public Channel write;
        }

        //
        // constants //////////////////////////////////////////////////////////
        //

        public const string kMenuPath                           = "Tools/blacktriangles/Material/Mask Splitter";
        public const string kTitle                              = "Mask Splitter";

        //
        // members ////////////////////////////////////////////////////////////
        //

        private Texture2D input                                 = null;
        private List<UseChannel> useChannels                    = new List<UseChannel>();
        
        //
        // constructor ////////////////////////////////////////////////////////
        //

        [MenuItem(kMenuPath)]
        private static void OpenWindow()
        {
            MaskSplitter window = GetWindow<MaskSplitter>();
            window.titleContent = new GUIContent(kTitle);
            window.Initialize();
        }

        //
        // --------------------------------------------------------------------
        //

        private void Initialize()
        {
        }
        
        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void OnEnable()
        {
            Initialize();
        }

        //
        // --------------------------------------------------------------------
        //

        protected virtual void OnGUI()
        {
            btGui.OnGUI();

            btGui.QuickListField<UseChannel>("Use Channels", useChannels);
        }

        //
        // private methods ////////////////////////////////////////////////////
        //
    }
}
