//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEditor;
using UnityEngine;

namespace blacktriangles
{
    public class MaskMaker
        : EditorWindow
    {
        //
        // types //////////////////////////////////////////////////////////////
        //
        
        private struct Input
        {
            public bool useTexture;
            public Texture2D texture;
            [Range(0f,1f)] public float color;
            public bool unfolded;
            public Vector2Int size;

            public static Input Default {
                get {
                    return new Input() {
                        useTexture = true,
                        texture = null,
                        color = 0f,
                        unfolded = true,
                        size = Vector2Int.zero
                    };
                }
            }

            public Color[] GetPixels()
            {
                if(useTexture)
                {
                    return texture.GetPixels(0);
                }
                else
                {
                    Color[] result = new Color[size.x*size.y];
                    result.Fill(new Color(color, color, color, color));
                    return result;
                }
            }
        }
        
        //
        // constants //////////////////////////////////////////////////////////
        //

        public const string kMenuPath                           = "Tools/blacktriangles/Material/Mask Maker";
        public const string kTitle                              = "Mask Maker";

        //
        // members ////////////////////////////////////////////////////////////
        //

        private Input metallic                                  = Input.Default;
        private Input ao                                        = Input.Default;
        private Input bump                                      = Input.Default;
        private Input smoothness                                = Input.Default;
        private static string lastPath                          = System.String.Empty;
        
        //
        // constructor ////////////////////////////////////////////////////////
        //

        [MenuItem(kMenuPath)]
        private static void OpenWindow()
        {
            MaskMaker window = GetWindow<MaskMaker>();
            window.titleContent = new GUIContent(kTitle);
            window.Initialize();
        }

        //
        // --------------------------------------------------------------------
        //

        private void Initialize()
        {
        }
        
        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void OnEnable()
        {
            Initialize();
        }

        //
        // --------------------------------------------------------------------
        //

        protected virtual void OnGUI()
        {
            btGui.OnGUI();

            DrawInput("Metallic", ref metallic);
            DrawInput("AO", ref ao);
            DrawInput("Bump", ref bump);
            DrawInput("Smoothness", ref smoothness);

            Vector2Int size = Vector2IntUtil.Max(
                Vector2IntUtil.Max(
                    Vector2IntUtil.Max(metallic.size, ao.size), 
                    bump.size
                ), 
                smoothness.size
            );

            EditorGUILayout.Vector2IntField("Size", size);

            if(GUILayout.Button("Create"))
            {
                if(size.x == 0 || size.y == 0)
                {
                    Dbg.Error($"Dims should not be 0! {size}");
                }
                else
                {
                    Texture2D result = new Texture2D(size.x, size.y);

                    metallic.size = size;
                    ao.size = size;
                    bump.size = size;
                    smoothness.size = size;

                    Color[] metalcol = metallic.GetPixels();
                    Color[] aocol = ao.GetPixels();
                    Color[] bumpcol = bump.GetPixels();
                    Color[] smoothcol = smoothness.GetPixels();

                    Color[] newcols = new Color[size.x*size.y];

                    for(int i = 0; i < newcols.Length; ++i)
                    {
                        newcols[i] = new Color(
                            metalcol[i].r,
                            aocol[i].r,
                            bumpcol[i].r,
                            smoothcol[i].r
                        );
                    }

                    result.SetPixels(newcols);
                    result.Apply();
                    lastPath = Create.TexturePNG(result, lastPath, true);
                }
            }
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private void DrawInput(string title, ref Input input)
        {
            input.unfolded = EditorGUILayout.Foldout(input.unfolded, title);
            if(input.unfolded)
            {
                GUILayout.BeginVertical(GUI.skin.box);
                input.useTexture = EditorGUILayout.Toggle("Use Texture", input.useTexture);
                if(input.useTexture)
                {
                    input.texture = btGui.ThinObjectField<Texture2D>("Texture", input.texture, false);
                }
                else
                {
                    input.color = EditorGUILayout.Slider("Color", input.color, 0f, 1f);
                }
                GUILayout.EndVertical();
            }

            input.size.x = 0;
            input.size.y = 0;
            if(input.useTexture)
            {
                if(input.texture == null)
                {
                    EditorGUILayout.HelpBox($"{title} Requires Texture", MessageType.Error);
                }
                else
                {
                    input.size.x = input.texture.width;
                    input.size.y = input.texture.height;
                }
            }
        }
    }
}
