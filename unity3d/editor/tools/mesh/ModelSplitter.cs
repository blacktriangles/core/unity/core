//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEditor;

using System.Collections.Generic;

namespace blacktriangles
{
    public class ModelSplitter
        : EditorWindow
    {
        //
        // constants //////////////////////////////////////////////////////////
        //

        private const string kMenuPath                          = "Tools/blacktriangles/Mesh/Model Prefab Splitter";
        private const string kTitle                             = "Model Splitter";

        private GameObject model                                = null;
        private GameObject rigRoot                              = null;

        //
        // constructor / initializer //////////////////////////////////////////
        //

        [MenuItem( kMenuPath )]
        private static void OpenWindow()
        {
            ModelSplitter window = GetWindow<ModelSplitter>();
            window.titleContent = new GUIContent( kTitle );
            window.Initialize();
        }

        //
        // -------------------------------------------------------------------
        //

        private void Initialize()
        {
            model = null;
            rigRoot = null;
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void OnEnable()
        {
            Initialize();
        }

        //
        // --------------------------------------------------------------------
        //

        protected virtual void OnGUI()
        {
            btGui.OnGUI();

            model = EditorGUILayout.ObjectField( "Model To Split", model, typeof( GameObject ), true ) as GameObject;
            rigRoot = EditorGUILayout.ObjectField( "Rig Root", rigRoot, typeof( GameObject ), true ) as GameObject;

            if( model == null || rigRoot == null )
            {
                EditorGUILayout.HelpBox( "Set a model to split, or add a rigRoot to multisplit.  RigRoots name must match in all multisplit models", MessageType.Info );
                return;
            }
            else if( rigRoot.transform.IsChildOf(model.transform) == false)
            {
                EditorGUILayout.HelpBox( "The rig must be a subobject of the mesh you are attempting to split.", MessageType.Error );
                return;
            }
            else
            {
                System.Func<GameObject,GameObject> guiCallback = (go)=>{ return EditorGUILayout.ObjectField( go, typeof(GameObject), true) as GameObject; };

                if( GUILayout.Button( "Split" ) )
                {
                    GameObject split = new GameObject();
                    split.name = "SPLIT";
                    Split( split, model, rigRoot.name );
                    EditorUtility.ClearProgressBar();
                }
            }
        }

        //
        // --------------------------------------------------------------------
        //

        private void Split( GameObject parent, GameObject splitee, string rigRootName ) 
        { 
            GameObject split = parent; 
            int count = 0; 
            List<GameObject> destroy = new List<GameObject>(); 
            foreach( Transform child in splitee.transform )
            { 
                EditorUtility.DisplayProgressBar( "Splitting Model", child.name, count++/(float)splitee.transform.childCount ); 
                if( child.gameObject.name != rigRootName ) 
                { 
                    // clone the original 
                    GameObject newGo = GameObject.Instantiate( splitee, Vector3.zero, Quaternion.identity ) as GameObject; 
                    newGo.name = child.name; 
                    newGo.transform.SetParent( split.transform ); 
                    // mark objects for destroy 
                    foreach( Transform newChild in newGo.transform ) 
                    { 
                        if( newChild.name != rigRootName && newChild.name != newGo.name ) 
                        { 
                            destroy.Add( newChild.gameObject ); 
                        } 
                    } 
                } 
            } 

            // destroy objects
            for( int i = 0; i < destroy.Count; ++i )
            {
                GameObject.DestroyImmediate( destroy[i] );
            }

            destroy = null;
        }

        //
        // end class //////////////////////////////////////////////////////////
        //
    }
}
