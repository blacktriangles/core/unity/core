//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEditor;
using UnityEditor.Animations;
using UnityEngine;

namespace blacktriangles
{
    public class MechanimAnimationGenerator
        : EditorWindow
    {
        //
        // members //////////////////////////////////////////////////////////////
        //

        private const string kTitle                             = "Mechanim Animation Generator";
        private const string kMenuPath                          = "Tools/blacktriangles/Animation/Mechanim Animation Generator";

        private AnimatorController controller;

        //
        // constructor / destructor ///////////////////////////////////////////
        //

        [MenuItem( kMenuPath )]
        private static void OpenWindow()
        {
            var window = GetWindow<MechanimAnimationGenerator>();
            window.titleContent = new GUIContent( kTitle );
            window.Initialize();
        }

        //
        // --------------------------------------------------------------------
        //
        
        private void Initialize()
        {
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void OnGUI()
        {
            btGui.OnGUI();

            controller = EditorGUILayout.ObjectField("Controller", controller, typeof(AnimatorController), false) as AnimatorController;

            if(controller != null && GUILayout.Button("Generate"))
            {
                Generate();
            }
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private void Generate()
        {
            string folder = EditorUtility.SaveFolderPanel( kTitle, "Assets/", System.String.Empty );
            if(folder.Length > 0)
            {
                folder = FileUtility.MakePathRelativeToAssetDir(folder);
                foreach(AnimatorControllerLayer layer in controller.layers)
                {
                    foreach(ChildAnimatorState child in layer.stateMachine.states)
                    {
                        AnimatorState state = child.state;

                        if(state.motion is BlendTree btree)
                        {
                            foreach(ChildMotion cmot in btree.children)
                            {
                                Motion mot = cmot.motion;

                                AnimationClip clip = new AnimationClip();
                                AssetDatabase.CreateAsset(clip, FileUtility.PathCombine(folder, mot.name + ".anim"));
                            }
                        }
                        else
                        {
                            AnimationClip clip = new AnimationClip();
                            AssetDatabase.CreateAsset(clip, FileUtility.PathCombine(folder, state.name + ".anim"));
                        }
                    }
                }

                AssetDatabase.Refresh();
            }
        }
    }
}
