//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;

namespace blacktriangles
{
    public class ProjectConfigWindow
        : EditorWindow
    {
        // constants //////////////////////////////////////////////////////////
        private const string kMenuPath                          = "Tools/blacktriangles/Config/Project Configuration";
        private const string kTitle                             = "Project Configuration Tool";

        // types ///////////////////////////////////////////////////////////////
        private class TemplateItem
        {
            public DirectoryInfo info;
            public bool enabled;
        }

        // members /////////////////////////////////////////////////////////////
        private List<TemplateItem> items = new List<TemplateItem>();
        private string importTo = "scripts/";
        private string nspace = "blacktriangles.Namespace";
        private bool configureProjectSettings = true;

        // constructor / destructor ///////////////////////////////////////////
        [MenuItem( kMenuPath )]
        private static void OpenWindow()
        {
            ProjectConfigWindow window = GetWindow<ProjectConfigWindow>();
            window.titleContent = new GUIContent( kTitle );
            window.Initialize();
        }

        private void Initialize()
        {
            items.Clear();
            string[] paths = Directory.GetDirectories( EditorConfig.kTemplatesDir );
            foreach( string path in paths )
            {
                TemplateItem item = new TemplateItem();
                item.info = new DirectoryInfo(path);
                item.enabled = false;
                items.Add( item );
            }
        }

        // unity callbacks ////////////////////////////////////////////////////
        protected virtual void OnEnable()
        {
            Initialize();
        }

        protected virtual void OnGUI()
        {
            btGui.OnGUI();
            configureProjectSettings = EditorGUILayout.Toggle( "Configure Project Settings", configureProjectSettings );
            nspace = EditorGUILayout.TextField( "Namespace", nspace );
            GUILayout.Label( "Templates" );
            GUILayout.BeginVertical(GUIStyles.darkbox);
                foreach( TemplateItem item in items )
                {
                    item.enabled = EditorGUILayout.Toggle( item.info.Name, item.enabled );
                }
            GUILayout.EndVertical();

            
            if( GUILayout.Button( "Configure" ) )
            {
                if( configureProjectSettings ) 
                {
                    blacktriangles.Editor.ProjectSettings.Configure();
                }
                foreach( TemplateItem item in items )
                {
                    if( item.enabled )
                    {
                        FileUtility.CopyDirectory( item.info.FullName, 
                                                    Path.Combine( "Assets", Path.Combine ( importTo, item.info.Name ) ),
                                                    ReplaceNamespace );
                    }
                }
            }
        }

        // private methods ////////////////////////////////////////////////////
        private void ReplaceNamespace( System.IO.FileInfo info )
        {
            if( System.IO.Path.GetExtension(info.FullName) == ".cs" )
            {
                FileUtility.FindAndReplaceTextFile( info.FullName, "blacktriangles.Template", nspace );
            }
        }
    }
}
