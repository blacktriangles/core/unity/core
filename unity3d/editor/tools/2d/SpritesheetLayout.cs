//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEditor;
using blacktriangles;

using System.Collections.Generic;

namespace blacktriangles.Tools2d
{
    public partial class SpritesheetLayout
        : IJsonSerializable
    {
        // members ////////////////////////////////////////////////////////////
        public string name                                        = "New Spritesheet Layout";
        public List<SpritesheetAnimation> animations            = new List<SpritesheetAnimation>();

        public Texture2D spritesheet                            { get { return _spritesheet; } set { SetSpritesheet( value ); } }
        public Sprite[] sprites                                    { get; private set; }
        public Texture2D[] spriteTextures                        { get; private set; }

        public bool isSpritesheetLoaded                            { get { return spritesheet != null && sprites != null && spriteTextures != null; } }
        public bool isSpritesheetValid                            { get { return isSpritesheetLoaded && sprites.Length > 0 && spriteTextures.Length > 0; } }

        private Texture2D _spritesheet                            = null;

        // constructor / initializer //////////////////////////////////////////
        public SpritesheetLayout()
        {
            spritesheet = null;
            sprites = null;
            spriteTextures = null;
        }

        public static SpritesheetLayout CreateFromPath( string path )
        {
            SpritesheetLayout result = null;
            if( System.IO.File.Exists( path ) )
            {
                string jsonString = System.IO.File.ReadAllText( path );
                JsonObject json = JsonObject.FromString( jsonString );
                if( json != null )
                {
                    result = new SpritesheetLayout();
                    result.FromJson( json );
                }
            }

            return result;
        }

        public static SpritesheetLayout CreateWithDialog()
        {
            SpritesheetLayout result = null;

            string path = EditorUtility.OpenFilePanel( "Load Animation Layout", System.String.Empty, "json" );
            if( System.String.IsNullOrEmpty( path ) == false )
            {
                result = CreateFromPath( path );
            }

            return result;
        }

        // public methods /////////////////////////////////////////////////////
        public void SetSpritesheet( Texture2D newSpritesheet )
        {
            if( _spritesheet != newSpritesheet )
            {
                _spritesheet = newSpritesheet;
                UpdateSprites();
            }
        }

        public Sprite GetSpriteAt( int index )
        {
            Sprite result = null;
            if( sprites != null && sprites.IsValidIndex( index ) )
                result = sprites[index];

            return result;
        }

        public Texture2D GetTextureAt( int index )
        {
            Texture2D result = null;
            if( spriteTextures != null && spriteTextures.IsValidIndex( index ) )
                result = spriteTextures[index];

            return result;
        }

        public void Save( string path )
        {
            string dir = System.IO.Path.GetDirectoryName( path );
            FileUtility.EnsureDirectoryExists( dir );

            JsonObject json = ToJson();
            System.IO.File.WriteAllText( path, json.ToString() );
        }

        public bool SaveWithDialog()
        {
            bool result = false;
            string path = EditorUtility.SaveFilePanel( "Save Animation Layout", System.String.Empty, "Default", "json" );
            if( System.String.IsNullOrEmpty( path ) == false )
            {
                Save( path );
            }

            return result;
        }

        public void CreateAnimations( EditorCurveBinding binding, string outputDirectory )
        {
            if( sprites == null )
            {
                Debug.LogError( "Cannot create animations without first binding a spritesheet.  Use SetSpritesheet or [layout].spritesheet to set which spritesheet to use." );
            }

            int count = animations.Count;
            for( int i = 0; i < count; ++i )
            {
                SpritesheetAnimation anim = animations[i];
                if( anim.duration > 0 )
                {
                    AnimationClip clip = new AnimationClip();
                    List<ObjectReferenceKeyframe> keyframes = new List<ObjectReferenceKeyframe>();
                    float frameTime = 0f;
                    foreach( SpritesheetAnimationFrame frameData in anim.frames )
                    {
                        if( sprites.IsValidIndex( frameData.index ) == false )
                        {
                            Debug.LogError( System.String.Format( "Could not create keyframe for animation {0}, frame index {1} is invalid for this spritesheet.", anim.name, frameData.index ) );
                        }
                        else
                        {
                            ObjectReferenceKeyframe keyframe = new ObjectReferenceKeyframe();
                            keyframe.time = frameTime;
                            keyframe.value = sprites[ frameData.index ];
                            keyframes.Add( keyframe );
                            frameTime += frameData.duration;
                        }
                    }

                    SpritesheetAnimationFrame lastFrame = anim.frames[ anim.frames.Count-1 ];
                    ObjectReferenceKeyframe lastKeyframe = new ObjectReferenceKeyframe();
                    lastKeyframe.time = frameTime;
                    lastKeyframe.value = sprites[ lastFrame.index ];
                    keyframes.Add( lastKeyframe );

                    AnimationUtility.SetObjectReferenceCurve( clip, binding, keyframes.ToArray() );
                    string clipPath = System.IO.Path.Combine( outputDirectory, anim.name + ".asset" );
                    Create.Asset( clip, clipPath, false );
                }
            }
        }

        public JsonObject ToJson()
        {
            JsonObject result = new JsonObject();
            result["name"] = name;
            result["animations"] = animations;
            return result;
        }

        public void FromJson( JsonObject json )
        {
            SpritesheetAnimation[] defs = json.GetField<SpritesheetAnimation[]>( "animations" );
            name = json.GetField<string>("name");
            animations = new List<SpritesheetAnimation>( defs );
        }

        // private methods ////////////////////////////////////////////////////
        private void UpdateSprites()
        {
            if( spritesheet != null )
            {
                Sprite[] resultSprites = null;
                spriteTextures = SpriteEditorUtility.GetAllSpriteTexturesInTexture( spritesheet, out resultSprites );
                sprites = resultSprites;
            }
        }
    }
}
