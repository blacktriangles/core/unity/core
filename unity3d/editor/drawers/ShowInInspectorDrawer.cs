//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace blacktriangles
{
    [CustomPropertyDrawer(typeof(ShowInInspectorAttribute))]
    public class ShowInInspectorDrawer
        : PropertyDrawer
    {
    }
}
