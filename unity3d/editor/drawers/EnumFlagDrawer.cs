//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using System;
using System.Reflection;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace blacktriangles
{
	[CustomPropertyDrawer(typeof(EnumFlagAttribute))]
	public class EnumFlagDrawer 
        : BasePropDrawer
    {
        //
        // unity callbacks ////////////////////////////////////////////////////
        //
        
		public override void OnGUI (Rect position, SerializedProperty property, GUIContent label) 
        {
			EnumFlagAttribute attr = attribute as EnumFlagAttribute;

            EditorGUI.BeginChangeCheck();

            string[] enumNames = System.Enum.GetNames(attr.enumType);
            property.intValue = EditorGUI.MaskField(position, label, property.intValue, enumNames);

            bool changed = EditorGUI.EndChangeCheck();
            if(changed)
            {
                EditorUtility.SetDirty(property.serializedObject.targetObject);
            }
		}
	}
}
