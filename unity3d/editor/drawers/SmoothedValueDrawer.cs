//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace blacktriangles
{
    [CustomPropertyDrawer(typeof(BaseSmoothValue), true)]
    public class BaseSmoothValueDrawer
        : PropDrawer<BaseSmoothValue>
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        private float lineHeight                                = 0f;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override float GetPropertyHeight(SerializedProperty property,
                                                GUIContent label)
        {
            lineHeight = base.GetPropertyHeight(property, label);
            float baseHeight = lineHeight;
            return baseHeight;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public override void OnGUI(Rect position,
                                   SerializedProperty property,
                                   GUIContent label)
        {
            float kHeight = lineHeight;
            position.height = kHeight - 2f;
            position.width /= 3f;

            string title = char.ToUpper(property.name[0]) + property.name.Substring(1);
            EditorGUI.LabelField(position, title);
            position.x += position.width;

            BaseSmoothValue val = GetValue(property);

            EditorGUI.LabelField(position, val.smoothedRaw.ToString());
            position.x += position.width;

            EditorGUI.LabelField(position, val.lastRaw.ToString());
        }
    }
}
