//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Collections.Generic;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace blacktriangles
{
    [CustomPropertyDrawer(typeof(BaseTimestampedValue), true)]
    public class TimestampedValueDrawer
        : PropDrawer<BaseTimestampedValue>
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override void OnGUI(Rect position,
                                   SerializedProperty property,
                                   GUIContent label)
        {
            position.width /= 3f;

            string title = char.ToUpper(property.name[0]) + property.name.Substring(1);
            EditorGUI.LabelField(position, title);
            position.x += position.width;

            BaseTimestampedValue val = GetValue(property);

            EditorGUI.LabelField(position, val.baseValue.ToString());
            position.x += position.width;

            EditorGUI.LabelField(position, val.elapsed.ToString());
        }
    }
}
