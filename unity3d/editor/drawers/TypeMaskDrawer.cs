//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEditor;
using UnityEngine;

namespace blacktriangles
{
    [CustomPropertyDrawer(typeof(TypeMaskAttribute))]
    public class TypeMaskDrawer
        : PropDrawer<int>
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        public override void OnGUI(Rect position, SerializedProperty prop, GUIContent label)
        {
            TypeMaskAttribute attr = attribute as TypeMaskAttribute;
            if(attr == null) return;

            EditorGUI.BeginChangeCheck();

            prop.intValue = EditorGUI.MaskField(position, label, prop.intValue, attr.names);

            if(EditorGUI.EndChangeCheck())
            {
                EditorUtility.SetDirty(prop.serializedObject.targetObject);
            }
        }
    }
}
