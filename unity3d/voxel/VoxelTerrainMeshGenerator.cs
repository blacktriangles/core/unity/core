//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    public class VoxelTerrainMeshGenerator
        : MonoBehaviour
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        public struct Triangle
        {
            public Vector3 v0;
            public Vector3 v1;
            public Vector3 v2;
        };
        
        //
        // members ////////////////////////////////////////////////////////////
        //

        public ComputeShader triangulate;

        public Vector3Int dims                                  = new Vector3Int(128,128,128);
        public Texture2D map0;

        private ComputeBuffer triangleBuffer                    = null;
        private ComputeBuffer map0Buffer                        = null;
        private Triangle[] triangles;


        //
        // public methods /////////////////////////////////////////////////////
        //

        public void Release()
        {
            if(triangleBuffer != null)
            {
                triangleBuffer.Release();
                triangleBuffer = null;
            }

            if(map0Buffer != null)
            {
                map0Buffer.Release();
                map0Buffer = null;
            }
        }

        [Button("Run Compute")]
        public void Run()
        {
            Release();

            //
            // create buffers
            //

            int pointCount = dims.x * dims.y * dims.z;
            int voxelCount = (dims.x-1) * (dims.y-1) * (dims.z-1);
            int triangleBufferSize = voxelCount * 5;

            int map0BufferSize = map0.width * map0.height;
            
            triangleBuffer = new ComputeBuffer(triangleBufferSize, sizeof(float)*3*3, ComputeBufferType.Append);
            map0Buffer = new ComputeBuffer(map0BufferSize, sizeof(float));

            //
            // Set Buffers
            //

            int kernelIdx = triangulate.FindKernel("Triangulate");
            triangulate.SetBuffer(kernelIdx, "triangles", triangleBuffer);

            //
            // Execute
            //

            triangulate.Dispatch(kernelIdx, dims.x, dims.y, dims.z);

            //
            // get data
            //

            triangles = new Triangle[triangleBufferSize];
            triangleBuffer.GetData(triangles);
            for(int i = 0; i < triangleBufferSize; ++i)
            {
                Triangle tri = triangles[i];
                Dbg.Log($"Index ({tri.v0.x},{tri.v0.y},{tri.v0.z})");
            }
        }
        
        //
        // unity callbacks ////////////////////////////////////////////////////
        //
    }
}
