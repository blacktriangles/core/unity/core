//
// (C) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using blacktriangles;
using System.Collections;
using UnityEngine;

namespace blacktriangles
{
    public class AudioFadeIn
        : MonoBehaviour
    {
        //
        // members  ///////////////////////////////////////////////////////////
        //

        public AudioSource[] sources                            = null;
        public AnimationCurve volume                            = null;
        public float timeStep                                   = 0.01f;

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Start()
        {
            StartCoroutine(FadeIn());
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private IEnumerator FadeIn()
        {
            float elapsed = 0.0f;
            float duration = volume.keys[volume.length-1].time;

            while(elapsed < duration)
            {
                foreach(AudioSource source in sources)
                {
                    source.volume = volume.Evaluate(elapsed);
                }

                elapsed += timeStep;
                yield return new WaitForSeconds(timeStep);
            }

            foreach(AudioSource source in sources)
            {
                source.volume = volume.Evaluate(duration);
            }
        }
    };
}
