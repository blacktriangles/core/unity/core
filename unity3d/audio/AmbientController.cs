//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    public class AmbientController
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public AudioCrossFader source;
        public float fadeTime                                   = 0.5f;
        public bool indoors                                     { get; private set; }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void OnTriggerEnter(Collider other)
        {
            AmbientZone zone = other.GetComponent<AmbientZone>();
            if(zone != null)
            {
                indoors = zone.indoors;
                source.CrossFade(zone.ambience, fadeTime);
            }
        }
    }
}
