//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    public class AmbientZone
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public AudioClip ambience;
        public bool indoors                                     = false;
        
    }
}
