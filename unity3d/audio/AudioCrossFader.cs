//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Collections;
using UnityEngine;

namespace blacktriangles
{
    public class AudioCrossFader
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public AudioSource primary;
        public AudioSource secondary;

        private bool isCrossFading = false;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void Play(AudioClip clip)
        {
            primary.clip = clip;
        }

        //
        // --------------------------------------------------------------------
        //

        public void CrossFade(AudioClip clip, float time)
        {
            if(primary.clip != clip && !isCrossFading)
            {
                Dbg.Log("Primary Clip {0} | New Clip {1}", primary.clip.name, clip.name);
                StartCoroutine(CrossFadeCoroutine(clip, time));
            }
        }

        //
        // private methods ////////////////////////////////////////////////////
        //
        
        private IEnumerator CrossFadeCoroutine(AudioClip clip, float time)
        {
            isCrossFading = true;
            primary.volume = 1f;
            secondary.volume = 0f;
            secondary.clip = clip;
            secondary.Play();

            var wait = new WaitForSeconds(0f);
            float elapsed = 0f;
            while(elapsed < time)
            {
                elapsed += Time.deltaTime;
                float perc = elapsed / time;
                primary.volume = 1f - perc;
                secondary.volume = perc;
                yield return wait;
            }

            AudioSource swap = secondary;
            secondary = primary;
            primary = swap;

            secondary.volume = 0f;
            primary.volume = 1f;
            isCrossFading = false;
        }

        //
        // unity callbacks ----------------------------------------------------
        //

        protected virtual void Awake()
        {
            primary.volume = 1f;
            primary.Play();
            secondary.volume = 0f;
            secondary.Stop();
        }
    }
}

