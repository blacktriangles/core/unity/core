//
// (c) BLACKTRIANGLES 2015-2020
// http://www.blacktriangles.com
//

using UnityEngine;
using System.Collections.Generic;

namespace blacktriangles
{
    public class InputState
    {
        //
        // constants ///////////////////////////////////////////////////////////
        //

        public const float kDefaultDoublePressThreshold         = 0.25f;

        //
        // members ////////////////////////////////////////////////////////////
        //

        public List<InputAction> actions                        { get; private set; }

        public TimestampedValue<bool> wasKeyPressed             { get { return GetWasKeyPressed(); } }
        public TimestampedValue<bool> isKeyDown                 { get { return GetIsKeyDown(); } }
        public TimestampedValue<bool> wasKeyReleased            { get { return GetWasKeyReleased(); } }

        //
        // constructor / initializer //////////////////////////////////////////
        //

        public InputState()
        {
            actions = new List<InputAction>();
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void Update()
        {
            GetKeyPressed();
            GetKey();
            GetKeyReleased();
        }

        //
        // --------------------------------------------------------------------
        //
        
        public float GetAxis()
        {
            float result = 0f;
            foreach( InputAction action in actions )
            {
                result += action.GetAxis();
            }

            return result;
        }

        //
        // --------------------------------------------------------------------
        //

        public bool GetKeyPressed()
        {
            bool result = false;
            foreach( InputAction action in actions )
            {
                result = result || action.GetKeyPressed();
                if( result ) break;
            }

            return result;
        }

        //
        // --------------------------------------------------------------------
        //


        public bool GetKeyDouble( float doublePressThreshold = kDefaultDoublePressThreshold)
        {
            bool result = false;
            foreach( InputAction action in actions )
            {
                result = result || action.GetKeyDouble( doublePressThreshold );
                if( result ) break;
            }

            return result;
        }

        //
        // --------------------------------------------------------------------
        //

        public bool GetKey()
        {
            bool result = false;
            foreach( InputAction action in actions )
            {
                result = result || action.GetKey();
                if( result ) break;
            }

            return result;
        }

        //
        // --------------------------------------------------------------------
        //

        public bool GetKeyReleased()
        {
            bool result = false;
            foreach( InputAction action in actions )
            {
                result = result || action.GetKeyReleased();
                if( result ) break;
            }

            return result;
        }

        //
        // private methods /////////////////////////////////////////////////////
        //
        private TimestampedValue<bool> GetWasKeyPressed()
        {
            TimestampedValue<bool> result = new TimestampedValue<bool>(false);
            foreach( InputAction action in actions )
            {
                if( action.wasKeyPressed.val
                    && ( result == null || result.elapsed < action.wasKeyPressed.elapsed) )
                {
                    result.Set(action.wasKeyPressed);
                }
            }

            return result;
        }

        //
        // --------------------------------------------------------------------
        //

        private TimestampedValue<bool> GetIsKeyDown()
        {
            TimestampedValue<bool> result = new TimestampedValue<bool>(false);
            foreach( InputAction action in actions )
            {
                if( action.isKeyDown.val
                    && ( result == null || result.elapsed < action.isKeyDown.elapsed) )
                {
                    result = action.isKeyDown;
                }
            }

            return result;
        }

        //
        // --------------------------------------------------------------------
        //

        private TimestampedValue<bool> GetWasKeyReleased()
        {
            TimestampedValue<bool> result = new TimestampedValue<bool>(false);
            foreach( InputAction action in actions )
            {
                if( action.wasKeyReleased.val
                    && ( result == null || result.elapsed < action.wasKeyReleased.elapsed) )
                {
                    result = action.wasKeyReleased;
                }
            }

            return result;
        }

    }
}
