//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace blacktriangles
{
    public class InputRouter<ActionTypeEnum>
    {
        // members ////////////////////////////////////////////////////////////////
        public System.Type actionType                            { get { return typeof(ActionTypeEnum); } }
        private Dictionary<ActionTypeEnum,InputState> states    = new Dictionary<ActionTypeEnum,InputState>();

        public Vector2 mousePos                                 { get { return UnityEngine.Input.mousePosition; } }
        public Vector2 screenMousePos                           { get { return new Vector2( mousePos.x - (Screen.width/2.0f), (Screen.height/2.0f) - mousePos.y ); } }
        public Vector2 screenMousePosNormalized                 { get { return new Vector2( screenMousePos.x / Screen.width, screenMousePos.y / Screen.height ).normalized; } }

        // public methods /////////////////////////////////////////////////////////
        public void Update()
        {
            foreach( InputState state in states.Values )
            {
                state.Update();
            }
        }

        public void Bind( ActionTypeEnum type, InputAction action )
        {
            InputState state = null;
            if( states.TryGetValue( type, out state ) == false )
            {
                state = new InputState();
            }

            state.actions.Add( action );
            states[type] = state;
        }

        public void Unbind( ActionTypeEnum type )
        {
            states.Remove( type );
        }

        public float GetAxis( ActionTypeEnum type )
        {
            InputState state = this[type];
            if( state == null ) return 0.0f;
            return state.GetAxis();
        }

        public bool GetKeyPressed( ActionTypeEnum type )
        {
            InputState state = this[type];
            if( state == null ) return false;
            return state.GetKeyPressed();
        }

        public bool GetKeyDouble( ActionTypeEnum type )
        {
            InputState state = this[type];
            if( state == null ) return false;
            return state.GetKeyDouble();
        }

        public bool GetKey( ActionTypeEnum type )
        {
            InputState state = this[type];
            if( state == null ) return false;
            return state.GetKey();
        }

        public bool GetKeyReleased( ActionTypeEnum type )
        {
            InputState state = this[type];
            if( state == null ) return false;
            return state.GetKeyReleased();
        }

        public double GetKeyPressedElapsed( ActionTypeEnum type )
        {
            InputState state = this[type];
            if( state == null ) return 0.0f;
            return state.wasKeyPressed.elapsed;
        }

        public double GetKeyElapsed( ActionTypeEnum type )
        {
            InputState state = this[type];
            if( state == null ) return 0.0f;
            return state.isKeyDown.elapsed;
        }

        public double GetKeyReleasedElapsed( ActionTypeEnum type )
        {
            InputState state = this[type];
            if( state == null ) return 0.0f;
            return state.wasKeyReleased.elapsed;
        }

        // operators //////////////////////////////////////////////////////////
        public InputState this[ System.Enum type ]
        {
            get
            {
                ActionTypeEnum action = ConvertActionType( type );
                return this[action];
            }
        }

        public InputState this[ ActionTypeEnum type ]
        {
            get
            {
                InputState result = null;
                states.TryGetValue( type, out result );
                return result;
            }
        }

        // private methods ////////////////////////////////////////////////////
        private ActionTypeEnum ConvertActionType( System.Enum type )
        {
            return (ActionTypeEnum)((object)type);
        }
    }
}
