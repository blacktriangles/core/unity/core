//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    public class Ragdoll
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public Animator animator                                = null;
        public RagdollBone[] ragdollBones                       = null;
        public RagdollBone root                                 { get { return ragdollBones?[0]; } }

        public float collideScale                               = 0.001f;

        //
        // public methods /////////////////////////////////////////////////////
        //

        [ContextMenu("Go Ragdoll")]
        public void GoRagdoll()
        {
            animator.enabled = false;
            if(ragdollBones.Length > 0)
            {
                foreach(RagdollBone rb in ragdollBones)
                {
                    if(rb != null)
                    {
                        rb.GoRagdoll();
                    }
                }
            }
        }

        //
        // editor methods /////////////////////////////////////////////////////
        //

        #if UNITY_EDITOR
        [ContextMenu("Generate Ragdoll")]
        public void GenerateRagdoll()
        {
            ragdollBones = GetComponentsInChildren<RagdollBone>();
            foreach(RagdollBone bone in ragdollBones)
            {
                bone.GenerateBone(collideScale);
            }
        }

        [ContextMenu("Clear")]
        public void Clear()
        {
            ragdollBones = GetComponentsInChildren<RagdollBone>();
            foreach(RagdollBone bone in ragdollBones)
            {
                bone.Clear();
            }
        }
        #endif
    }
}
