//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    public class RandomPoolSpawn
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public GameObjectPool pool                              = null;
        public float minDt                                      = 1f;
        public float maxDt                                      = 2f;

        private float nextTrigger                               = 0f;

        //
        // public methods /////////////////////////////////////////////////////
        //

        //
        // private methods ////////////////////////////////////////////////////
        //
        
        private void Trigger()
        {
            pool.Take(transform.position, transform.rotation, null);
        }
        
        //
        // unity callbacks/////////////////////////////////////////////////////
        //

        protected virtual void Update()
        {
            if(Time.time > nextTrigger)
            {
                Trigger();
                nextTrigger = Time.time + btRandom.Range(minDt, maxDt);
            }
        }
    }
}
