//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using System.Collections;

namespace blacktriangles
{
    public class MoveForward
        : MonoBehaviour
    {
        // members ////////////////////////////////////////////////////////////
        public float speed                                        = 1f;
        public bool is2d                                        = false;
        private Rigidbody rb                                    = null;
        private Rigidbody2D rb2d                                = null;

        // unity callbacks ////////////////////////////////////////////////////
        protected virtual void Awake()
        {
            rb = GetComponent<Rigidbody>();
            rb2d = GetComponent<Rigidbody2D>();
        }

        protected virtual void Update()
        {
            if( rb == null && rb2d == null )
            {
                transform.localPosition += transform.forward * Time.deltaTime * speed;
            }
        }

        protected virtual void FixedUpdate()
        {
            Vector3 newPos = transform.localPosition + ( transform.forward * Time.fixedDeltaTime * speed );
            if( rb != null )
            {
                rb.MovePosition( newPos );
            }
            else if( rb2d != null )
            {
                rb2d.MovePosition( newPos );
            }
        }
    }
}
