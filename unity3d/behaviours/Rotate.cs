//=============================================================================
//
// (C) BLACKTRIANGLES 2016
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using UnityEngine;

namespace blacktriangles
{
    public class Rotate
        : MonoBehaviour
    {
        //
        // members /////////////////////////////////////////////////////////////
        //

        public Vector3 axis                                     = Vector3.up;
        public float speed                                      = 1.0f;

        //
        // unity callbacks /////////////////////////////////////////////////////
        //

        protected virtual void Update()
        {
            transform.Rotate( axis, speed * Time.deltaTime );
        }
    }
}
