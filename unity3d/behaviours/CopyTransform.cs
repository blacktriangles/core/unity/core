//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    public class CopyTransform
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public Transform target;
        public bool copyPosition                                = false;
        public bool copyRotation                                = true;
        [ShowIf("copyRotation")] public bool globalRotation     = false;
        public bool copyScale                                   = false;

        //
        // unity callbacks ////////////////////////////////////////////////////
        //
        
        protected virtual void LateUpdate()
        {
            if(copyPosition)
                transform.position = target.position;

            if(copyRotation)
            {
                if(globalRotation)
                    transform.localRotation = target.rotation;
                else
                    transform.localRotation = target.localRotation;
            }

            if(copyScale)
                transform.localScale = target.localScale;
        }
    }
}
