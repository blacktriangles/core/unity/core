//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace blacktriangles
{
    [RequireComponent(typeof(Collider))]
    public class DestroyOnCollide
        : MonoBehaviour
    {
        // events /////////////////////////////////////////////////////////////
        public delegate void DestroyedCallback( DestroyOnCollide obj, GameObject other, Collision collision );
        public event DestroyedCallback OnDestroyed;

        // members ////////////////////////////////////////////////////////////
        public bool onTriggers                                  = false;
        public LayerMask ignoreLayers                           = default(LayerMask);
        public List<GameObject> ignoreObjects = new List<GameObject>();
        public float delay                                      = 0.0f;

        public Collider selfCollider                   { get; private set; }

        // unity callbacks ////////////////////////////////////////////////////
        protected virtual void Start()
        {
            selfCollider = gameObject.GetComponent<Collider>();
        }

        protected virtual void OnCollisionEnter( Collision collision )
        {
            CheckDestroy( collision.gameObject, collision );
        }

        protected virtual void OnTriggerEnter( Collider collider )
        {
            if( selfCollider == null ) return;
            if( onTriggers || selfCollider.isTrigger)
            {
                if( onTriggers || collider.isTrigger == false )
                {
                    CheckDestroy( collider.gameObject );
                }
            }
        }

        protected virtual void OnCollisionEnter2D( Collision2D collision )
        {
            CheckDestroy( collision.gameObject );
        }

        protected virtual void OnTriggerEnter2D( Collider2D collider )
        {
            if( onTriggers )
            {
                CheckDestroy( collider.gameObject );
            }
        }

        // private methods ////////////////////////////////////////////////////
        private void CheckDestroy( GameObject other, Collision collision = null )
        {
            if( ((1 << other.layer) & ignoreLayers.value) != 0 ) return;
            if( ignoreObjects.Contains(other) ) return;

            StartCoroutine( DoDestroy( delay, other, collision ) );
        }

        private IEnumerator DoDestroy( float delay, GameObject other, Collision collision )
        {
            yield return new WaitForSeconds( delay );
            Destroy( gameObject );
            if( OnDestroyed != null )
            {
                OnDestroyed( this, other, collision);
            }
        }
    }
}
