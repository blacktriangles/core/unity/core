//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;

namespace blacktriangles
{
    public class CoroutineManager
        : MonoBehaviour
    {
        // members ////////////////////////////////////////////////////////////
        public static CoroutineManager instance                    { get; private set; }

        // unity callbacks ////////////////////////////////////////////////////
        protected virtual void Awake()
        {
            if( instance != null )
                Debug.LogError( "Multiple CoroutineManagers running!" );

            instance = this;
        }
    }
}
