//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using blacktriangles;
using UnityEngine;

namespace blacktriangles
{
    public class Follow
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public Transform target                                 = null;
        public float maxRange                                   = 3.0f;

        //
        // public static methods //////////////////////////////////////////////
        //
        
        public static Vector3 Track(Vector3 pos, Vector3 target, float max)
        {
            Vector3 diff = target - pos;
            Vector3 result = Vector3.zero;

            float dist2 = diff.sqrMagnitude;
            float max2 = max * max;
            if(max > 0)
            {
                float perc = Mathf.Min(1.0f, dist2/max2);
                result = diff * perc;
            }

            return result;
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void FixedUpdate()
        {
            if(target == null) return;

            Vector3 delta = Track(transform.position, target.position, maxRange);

            transform.position += delta * Time.fixedDeltaTime;
        }
        
    }
}
