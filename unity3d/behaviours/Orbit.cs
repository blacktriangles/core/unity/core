//=============================================================================
//
// (C) BLACKTRIANGLES 2016
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using UnityEngine;

namespace blacktriangles
{
    public class Orbit
        : MonoBehaviour
    {
        // members /////////////////////////////////////////////////////////////
        public float distance                                   = 1.0f;
        public float orbitSpeed                                 = 1.0f;
        public float offset                                     = 0.0f;
        public Transform origin                                 = null;
        public Vector3 axis                                     = Vector3.up;
        public bool lookAtOrigin                                = false;

        //
        // unity callbacks /////////////////////////////////////////////////////
        //
        protected virtual void Update()
        {
            axis = axis.normalized;
            Quaternion rotation = Quaternion.AngleAxis(((Time.time+offset)%360f)*orbitSpeed, axis);
            Vector3 newPos = (rotation * Vector3.forward)*distance;
            if( origin != null )
            {
                transform.localPosition = origin.position + newPos;
            }
            else
            {
                transform.localPosition = newPos;
            }

            if(lookAtOrigin)
            {
                transform.LookAt(origin.position);
            }
        }
    }
}
