//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Collections;
using UnityEngine;

namespace blacktriangles
{
    public class RandomizeLine
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public LineRenderer line;

        public Vector3 minScale = Vector3.zero;
        public Vector3 maxScale = Vector3.one;
        public Vector3 timeScale = Vector3.one;

        public Vector3 startpos                                 = Vector3.zero;
        public Vector3 endpos                                   = new Vector3(0f, 10f, 0f);
        public int segmentCount                                 = 10;

        public float mutateTime = 0.1f;

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Start()
        {
            StartCoroutine(Mutate());
        }

        private IEnumerator Mutate()
        {
            var wait = new WaitForSeconds(mutateTime);
            while(true)
            {
                Vector3[] positions = new Vector3[segmentCount];
                for(int i = 0; i < segmentCount; ++i)
                {
                    Vector3 neutralPos = (endpos - startpos) / segmentCount * i;

                    positions[i] = neutralPos;
                    positions[i] = new Vector3(
                        Mathf.Sin(Time.time * timeScale.x) * btRandom.Range(minScale.x, maxScale.x),
                        Mathf.Sin(Time.time * timeScale.y) * btRandom.Range(minScale.y, maxScale.y),
                        Mathf.Sin(Time.time * timeScale.z) * btRandom.Range(minScale.z, maxScale.z)
                    ) + neutralPos;
                }

                positions[0] = startpos;
                positions[positions.Length-1] = endpos;

                line.SetPositions(positions);

                yield return wait;
            }
        }
    }
}
