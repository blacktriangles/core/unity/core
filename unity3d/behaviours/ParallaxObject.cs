//
// (c) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//

using UnityEngine;
using System.Collections;

namespace blacktriangles
{
    public class ParallaxObject 
        : MonoBehaviour
        , ITarget
    {
        //
        // members /////////////////////////////////////////////////////////////
        //

        [SerializeField] private Transform startTarget          = null;
        public Transform target                                 { get; private set; }

        [Header("Keep At Range")]
        public bool keepAtRangeEnabled                          = true;
        [ShowIf("keepAtRangeEnabled")] public FloatRange rangeDelta = new FloatRange( -1.0f, 1.0f );
        public FloatRange range                                 { get; private set; }

        [Header("Follow")]
        public bool followEnabled                               = true;
        [ShowIf("followEnabled"), Range(0,1)] public float followSpeed      = 0.5f; 
        
        private Vector3 lastPos                                 = Vector3.zero;

        //
        // constructor / destructor ////////////////////////////////////////////
        //

        public void SetTarget( Transform _target )
        {
            target = _target;
            lastPos = target.position;
            Vector3 startDiff = lastPos - transform.position;
            float startDist = startDiff.magnitude;
            range = new FloatRange( startDist + rangeDelta.min, startDist + rangeDelta.max );
        }

        //
        // unity callbacks /////////////////////////////////////////////////////
        //

        protected virtual void Start()
        {
            if( startTarget != null )
            {
                SetTarget( startTarget );
            }
        }

        //
        // --------------------------------------------------------------------
        //

        protected virtual void Update()
        {
            if( target != null )
            {
                // follow motion ///////////////////////////////////////////
                if( followEnabled )
                {
                    Vector3 moveDiff = target.position - lastPos;
                    lastPos = target.position;
                    transform.localPosition += moveDiff * followSpeed;
                }

                // update keep at range ////////////////////////////////////
                if( keepAtRangeEnabled )
                {
                    Vector3 diff = transform.position - target.position;
                    float dist = diff.magnitude;
                    float distClamp = range.Clamp(dist);
                    Vector3 targetPos = target.position + ( diff.normalized * distClamp );
                    transform.localPosition = Vector3.Slerp(transform.position, targetPos, dist/distClamp );
                }
            }
        }
    }
}
