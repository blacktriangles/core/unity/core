//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using System.Collections;

namespace blacktriangles
{
    public class RelativeMotion
        : MonoBehaviour
        , ITarget
    {
        // members /////////////////////////////////////////////////////////////
        public Transform target                                 { get; private set; }
        [SerializeField] private Transform startTarget          = null;

        public Bounds targetBounds;

        // constructor / destructor ////////////////////////////////////////////
        public void Start()
        {
            if( startTarget != null )
            {
                SetTarget( startTarget );
            }
        }

        public void SetTarget( Transform _target )
        {
            target = _target;
            targetBounds.center = target.transform.position;
        }

        // unity callbacks /////////////////////////////////////////////////////
        protected void Update()
        {
            if( target != null && targetBounds.Contains( target.transform.position ) == false )
            {
                Vector3 closestPoint = targetBounds.ClosestPoint( target.transform.position );
                Vector3 diff = target.transform.position - closestPoint;
                transform.position += diff;
                targetBounds.center += diff;
            }
        }
    }
}
