//
// (c) BLACKTRIANGLES 2021
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    [System.Serializable]
    public class BiomeMaterials
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        [System.Serializable]
        public struct Layer
        {
            [Range(0f,1f)] public float min;
            [Range(0f,1f)] public float max;
            public AnimationCurve blend;
            public TerrainLayer material;
        }
        
        //
        // members ////////////////////////////////////////////////////////////
        //

        public NoiseData noise;
        public Layer[] layers;
        
    }
}
