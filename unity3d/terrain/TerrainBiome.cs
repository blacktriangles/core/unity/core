//
// (c) BLACKTRIANGLES 2021
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    [CreateAssetMenu(fileName="Terrain Biome", menuName="blacktriangles/Terrain/Biome")]
    public class TerrainBiome
        : ScriptableObject
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public NoiseData terrain;
        public BiomeMaterials materials;
    }
}
