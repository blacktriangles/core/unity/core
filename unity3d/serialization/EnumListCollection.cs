//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Collections;
using System.Collections.Generic;

namespace blacktriangles
{
    public class EnumListCollection<EnumType, Type>
        : EnumList<EnumType, List<Type>>
        , IEnumerable<Type>
        where EnumType : System.Enum
    {
        public EnumListCollection()
        {
            EnumType[] types = EnumUtility.GetValues<EnumType>();
            foreach(EnumType type in types)
            {
                this[type] = new List<Type>();
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void AddToAll(Type type)
        {
            foreach(List<Type> list in items)
            {
                list.Add(type);
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public new IEnumerator<Type> GetEnumerator()
        {
            foreach(List<Type> list in items)
            {
                if(list != null)
                {
                    foreach(Type t in list)
                    {
                        yield return t;
                    }
                }
            }
        }

        //
        // --------------------------------------------------------------------
        //

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
