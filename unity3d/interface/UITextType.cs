//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace blacktriangles
{
    [System.Serializable]
    public class UITextType
    {
        // types //////////////////////////////////////////////////////////////
        [System.Serializable]
        public struct Paragraph
        {
            [TextArea] public string text;
            public float speed;
        }

        // static public methods //////////////////////////////////////////////
        static public IEnumerator TypeParagraphAsync( TextMeshProUGUI target, Paragraph paragraph )
        {
            if( paragraph.speed > 0f )
            {
                foreach( char c in paragraph.text )
                {
                    target.text += c;
                    yield return new WaitForSeconds( 0.05f / paragraph.speed );
                }
            }
            else
            {
                target.text += paragraph.text;
            }
        }

        public static IEnumerator TypeParagraphsAsync( TextMeshProUGUI target, IEnumerable<Paragraph> paragraphs )
        {
            foreach( Paragraph paragraph in paragraphs )
            {
                yield return TypeParagraphAsync( target, paragraph );
            }
        }
    }
}
 
