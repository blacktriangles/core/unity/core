//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace blacktriangles
{
    public class UISubmitter
        : UIElement
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        [System.Serializable]
        public class SubmitEvent
            : UnityEvent<string>
        {
        }
        
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public SubmitEvent submit;
        public TMP_InputField tmp_input;
        public InputField input;

        //
        // private methods ////////////////////////////////////////////////////
        //
        
        private void OnEndEdit(string text)
        {
            if(Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
            {
                submit.Invoke(text);
            }
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected override void Awake()
        {
            base.Awake();
            if(input != null)
            {
                input.onEndEdit.AddListener(OnEndEdit);
            }

            if(tmp_input != null)
            {
                tmp_input.onEndEdit.AddListener(OnEndEdit);
            }
        }
    }
}

