//
// (c) BLACKTRIANGLES 2016
// http://www.blacktriangles.com
//

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using blacktriangles;

using System.Collections;

namespace blacktriangles
{
    [RequireComponent( typeof( RectTransform ) )]
    public class UIElement
        : MonoBehaviour
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        public delegate void ShowCallback();
        public ShowCallback OnShow;

        //
        // --------------------------------------------------------------------
        //

        public delegate void HideCallback();
        public HideCallback OnHide;

        //
        // --------------------------------------------------------------------
        //

        public enum AnimState
        {
            None,
            FadeIn,
            FadeOut
        };

        //
        // members ////////////////////////////////////////////////////////////
        //

        public RectTransform rectTransform                      { get; private set; }
        public CanvasGroup canvasGroup                          { get; private set; }
        public UIElementStateSet state                          { get { return _state; } }
        public AnimState animState                              { get; private set; }
        public bool isInitialized                               { get; private set; }

        [Header("UIElement")]
        [SerializeField] private UIElementStateSet _state       = new UIElementStateSet();

        private IEnumerator fadeCoroutine                       = null;
        private RectTransform parentRect                        = null;

        //
        // constructor / initializer //////////////////////////////////////////
        //

        public virtual void Initialize()
        {
            isInitialized = true;
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public virtual void Show()
        {
            Show(0f, null);
        }

        //
        // --------------------------------------------------------------------
        //

        public virtual void Show( float time, System.Action callback )
        {
            Refresh();
            StartShow();
            Fade( true, time, callback );
        }

        //
        // --------------------------------------------------------------------
        //

        public virtual IEnumerator ShowAsync( float time = 0.0f )
        {
            yield return FadeAsync( true, time );
        }

        //
        // --------------------------------------------------------------------
        //
        
        public virtual void Hide()
        {
            Hide(0f, null);
        }

        //
        // --------------------------------------------------------------------
        //

        public virtual void Hide( float time, System.Action callback )
        {
            StartHide();
            Fade( false, time, callback );
        }

        //
        // --------------------------------------------------------------------
        //
        
        public virtual IEnumerator HideAsync( float time = 0.0f )
        {
            yield return FadeAsync( false, time );
        }

        //
        // --------------------------------------------------------------------
        //

        public virtual void Fade( bool fadeIn, float time, System.Action callback )
        {
            if( fadeIn && animState == AnimState.FadeIn ) return;
            if( !fadeIn && animState == AnimState.FadeOut ) return;
            if( fadeCoroutine != null )
            {
                StopCoroutine(fadeCoroutine);
            }

            fadeCoroutine = FadeAsync( fadeIn, time, callback );
            StartCoroutine( fadeCoroutine );
        }

        //
        // --------------------------------------------------------------------
        //

        public virtual IEnumerator FadeAsync( bool fadeIn, float time, System.Action callback = null )
        {
            if( fadeIn && animState == AnimState.FadeIn ) yield break;
            if( !fadeIn && animState == AnimState.FadeOut ) yield break;

            animState = fadeIn ? AnimState.FadeIn : AnimState.FadeOut;
            canvasGroup.blocksRaycasts = fadeIn;
            canvasGroup.interactable = fadeIn;

            if( canvasGroup == null )
            {
                gameObject.SetActive( fadeIn );
            }
            else
            {
                if( time.IsApproximately( 0.0f ) )
                {
                    canvasGroup.alpha = fadeIn ? 1.0f : 0.0f;
                }
                else
                {
                    yield return FadeCoroutine( fadeIn, time, callback );
                }
            }

            animState = AnimState.None;
            _state.visible.active = fadeIn;
            if( callback != null ) callback();
            NotifyFadeComplete(fadeIn);
        }

        //
        // --------------------------------------------------------------------
        //

        public void RefreshUI()
        {
            Refresh();
        }

        //
        // --------------------------------------------------------------------
        //

        public virtual bool Refresh()
        {
            state.RefreshColors();
            return true;
        }

        //
        // --------------------------------------------------------------------
        //

        public virtual void Select()
        {
            SelectGameObject( gameObject );
        }

        //
        // --------------------------------------------------------------------
        //

        public virtual void SelectGameObject( GameObject go )
        {
            StartCoroutine( SelectGameObjectAtEndOfFrame( go ) );
        }

        //
        // --------------------------------------------------------------------
        //

        public bool ConstrainToParent(float maxOverflow = 0f)
        {
            Vector2 pos = rectTransform.anchoredPosition;
            rectTransform.pivot = new Vector2(0.5f, 0.5f);
            Rect windowRect = new Rect(pos.x,
                                       pos.y,
                                       rectTransform.rect.width, 
                                       rectTransform.rect.height);

            float overflowx = maxOverflow * windowRect.width;
            float overflowy = maxOverflow * windowRect.height;

            Rect container = parentRect.rect;
            container.x -= rectTransform.rect.x + overflowx;
            container.y -= rectTransform.rect.y + overflowy;
            container.width += overflowx * 2.0f;
            container.height += overflowy * 2.0f;

            bool constrained = windowRect.ConstrainTo(container);

            rectTransform.anchoredPosition = windowRect.position;

            return constrained;
        }

        //
        // protected methods //////////////////////////////////////////////////
        //

        protected virtual void StartShow()
        {
        }

        //
        // --------------------------------------------------------------------
        //

        protected virtual void StartHide()
        {
        }

        //
        // --------------------------------------------------------------------
        //
        

        protected virtual void OnStartAnimation()
        {
        }

        //
        // --------------------------------------------------------------------
        //

        protected virtual void OnEndAnimation()
        {
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Awake()
        {
            rectTransform = GetComponent<RectTransform>();
            canvasGroup = GetComponent<CanvasGroup>();
        }

        //
        // --------------------------------------------------------------------
        //

        protected virtual void Start()
        {
            parentRect = rectTransform.parent.GetComponent<RectTransform>();
        }

        //
        // --------------------------------------------------------------------
        //
        
        protected virtual void NotifyOnShow()
        {
            if(OnShow != null)
            {
                OnShow();
            }
        }

        //
        // --------------------------------------------------------------------
        //

        protected virtual void NotifyOnHide()
        {
            if(OnHide != null)
            {
                OnHide();
            }
        }
        
        //
        // private methods ////////////////////////////////////////////////////
        //

        private IEnumerator FadeCoroutine( bool fadeIn, float time, System.Action callback )
        {
            OnStartAnimation();

            time *= !fadeIn ? canvasGroup.alpha : 1.0f - canvasGroup.alpha;
            float start = canvasGroup.alpha;
            float end = fadeIn ? 1f : 0f;

            float elapsed = 0f;
            while( elapsed < time )
            {
                elapsed += Time.deltaTime;
                float perc = elapsed / time;
                canvasGroup.alpha = Mathf.Lerp( start, end, perc );
                yield return new WaitForEndOfFrame();
            }

            canvasGroup.alpha = end;
            OnEndAnimation();
            if( callback != null )
            {
                callback();
            }
        }

        //
        // --------------------------------------------------------------------
        //

        private IEnumerator SelectGameObjectAtEndOfFrame( GameObject go )
        {
            yield return new WaitForEndOfFrame();
            EventSystem.current.SetSelectedGameObject( null );
            EventSystem.current.SetSelectedGameObject( go );
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private void NotifyFadeComplete(bool fadeIn)
        {
            if(fadeIn)
            {
                NotifyOnShow();
            }
            else if(!fadeIn)
            {
                NotifyOnHide();
            }
        }
        
    }
}
