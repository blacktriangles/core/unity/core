//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace blacktriangles
{
    public class UIDragArea
        : UIElement
        , IBeginDragHandler
        , IDragHandler
        , IEndDragHandler
    {
        //
        // events /////////////////////////////////////////////////////////////
        //
        public delegate void StartDrag(PointerEventData ev);
        public delegate void UpdateDrag(PointerEventData ev);
        public delegate void StopDrag(PointerEventData ev);

        public event StartDrag OnStartDrag;
        public event UpdateDrag OnUpdateDrag;
        public event StopDrag OnStopDrag;

        //
        // members ////////////////////////////////////////////////////////////
        //

        public bool isDragging                                  { get; private set; }
        public double startDragTime                             { get; private set; }
        public double dragDt                                    { get { return !isDragging ? 0 : Epoch.now - startDragTime; } } 

        //
        // callbacks //////////////////////////////////////////////////////////
        //

        public void OnBeginDrag(PointerEventData ev)
        {
            isDragging = true;
            startDragTime = Epoch.now;
            StartDrag cb = OnStartDrag;
            if(cb != null)
            {
                cb(ev);
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public void OnDrag(PointerEventData ev)
        {
            UpdateDrag cb = OnUpdateDrag;
            if(cb != null)
            {
                cb(ev);
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public void OnEndDrag(PointerEventData ev)
        {
            isDragging = false;
            StopDrag cb = OnStopDrag;
            if(cb != null)
            {
                cb(ev);
            }
        }
    }
}
 
