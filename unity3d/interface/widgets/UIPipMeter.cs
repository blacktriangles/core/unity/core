//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace blacktriangles
{
    public class UIPipMeter
        : UIElement
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        [Header("References")]
        public UIPip pipPrefab;
        public UIElement container;
        public Sprite filled;
        public Color filledColor;
        public Sprite unfilled;
        public Color unfilledColor;

        [Header("State")]
        public int max;
        public int used;

        private List<UIPip> pips                                = new List<UIPip>();

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void Refresh(int used, int max)
        {
            this.max = btMath.Max(0, max);
            this.used = btMath.Min(this.max, used);

            Refresh();
        }

        //
        // --------------------------------------------------------------------
        //

        public override bool Refresh()
        {
            for(int i = 0; i < max; ++i)
            {
                UIPip pip = null;
                if(pips.IsValidIndex(i))
                {
                    pip = pips[i];
                }

                if(pip == null)
                {
                    pip = Instantiate(pipPrefab, Vector3.zero, Quaternion.identity, container.transform);
                    pips.Add(pip);
                }

                pip.filledImage.sprite = filled;
                pip.filledImage.color = filledColor;
                pip.unfilledImage.sprite = unfilled;
                pip.unfilledImage.color = unfilledColor;
                pip.filled = i < used;
                pip.Refresh();
            }

            for(int i = max; i < pips.Count; ++i)
            {
                Destroy(pips[i].gameObject);
            }

            pips.SetLength(max);

            return true;
        }
    }
}
