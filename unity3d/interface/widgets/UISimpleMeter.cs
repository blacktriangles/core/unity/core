//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;
using UnityEngine.UI;

namespace blacktriangles
{
    public class UISimpleMeter
        : UIElement
    {
        //
        // membmers ///////////////////////////////////////////////////////////
        //
        
        public Image slider;

        public float progress                                   { get { return GetValue(); } set { SetValue(value); } }

        private float val;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void SetValue(float val)
        {
            this.val = val;
            Refresh();
        }

        //
        // --------------------------------------------------------------------
        //

        public float GetValue()
        {
            return val;
        }

        //
        // --------------------------------------------------------------------
        //

        public override bool Refresh()
        {
            bool result = base.Refresh();
            if(slider != null)
            {
                slider.fillAmount = progress;
            }

            return result;
        }
    }
}
