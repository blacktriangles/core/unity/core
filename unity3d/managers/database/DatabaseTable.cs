//
// (C) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using blacktriangles;
using UnityEngine;

namespace blacktriangles
{
    [System.Serializable]
    public abstract class DatabaseTable
    {
        //
        // static /////////////////////////////////////////////////////////////
        //
        
        public static T[] LoadAll<T>( string path )
            where T: UnityEngine.Object
        {
            return Resources.LoadAll( path, typeof( T ) ).ConvertAll<T,Object>();
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public abstract void Initialize(Database db);
    };
}
