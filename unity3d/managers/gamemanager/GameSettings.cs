//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    [System.Serializable]
    public struct GameSettings
    {
        //
        // types //////////////////////////////////////////////////////////////
        //
        
        [System.Serializable]
        public struct Mouse
        {
            public bool invertX;
            public bool invertY;
            public Vector2 speed;
        };

        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public Mouse mouse;

        //
        // default ////////////////////////////////////////////////////////////
        //
        
        public static GameSettings Default
        {
            get
            {
                GameSettings result = new GameSettings() {
                    mouse = new Mouse() {
                        invertX = false,
                        invertY = true,
                        speed = Vector2.one
                    },
                };

                return result;
            }
        }
    };
}
