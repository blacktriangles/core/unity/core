//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace blacktriangles
{
    public class DialogManager
        : MonoBehaviour
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        [System.Serializable]
        public struct RequestData
        {
            public UIDialog dialogInstance;
            public UIDialog.Request dialogRequest;
        }
        
        //
        // members ////////////////////////////////////////////////////////////
        //

        public UIDialog activeDialog                            { get; private set; }
        [SerializeField] private UIDialog[] dialogs             = null;
        private Queue<RequestData> requests                     = new Queue<RequestData>();

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void Request( string dialogName, UIDialog.Request request )
        {
            RequestData newRequest = new RequestData() {
                dialogInstance = SelectDialog(dialogName),
                dialogRequest = request
            };

            Request(newRequest);
        }

        //
        // --------------------------------------------------------------------
        //

        public void Request( RequestData request )
        {
            if( activeDialog == null )
            {
                activeDialog = request.dialogInstance;
                if( activeDialog == null ) return;
                activeDialog.Open( request.dialogRequest );
            }
            else
            {
                requests.Enqueue( request );        
            }
        }
        
        //
        // callbacks //////////////////////////////////////////////////////////
        //

        public void OnDialogOpen( UIDialog dialog, UIDialog.Request request )
        {
        }

        public void OnDialogClose( UIDialog dialog, UIDialog.Request request, UIDialog.Response response )
        {
            activeDialog = null;
            if( requests.Count > 0 )
            {
                Request( requests.Dequeue() );
            }
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Start()
        {
            foreach( UIDialog dialog in dialogs )
            {
                dialog.Initialize( this );
            }
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private UIDialog SelectDialog( string name )
        {
            return dialogs.Find( (d)=>{ return d.name == name; } );
        }
    }
}
 
