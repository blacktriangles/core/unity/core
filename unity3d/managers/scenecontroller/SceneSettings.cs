//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    [System.Serializable]
    public struct SceneSettings
    {
        //
        // types //////////////////////////////////////////////////////////////
        //
        
        [System.Serializable]
        public struct Mouse
        {
            public CursorLockMode cursorMode;
            public bool visible;
        };

        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public Mouse mouse;

        //
        // static methods /////////////////////////////////////////////////////
        //

        public static SceneSettings Default
        {
            get
            {
                return new SceneSettings() {
                    mouse = new Mouse() {
                        cursorMode = CursorLockMode.None,
                        visible = true,
                    },
                };
            }
        }
        

    }
}
