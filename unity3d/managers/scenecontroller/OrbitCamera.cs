//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using blacktriangles;
using UnityEngine;

namespace blacktriangles
{
    public class OrbitCamera
        : btCamera
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        [Header("Orbit Settings")]
        public float moveSpeed                                  = 30.0f;
        public float turnSpeed                                  = 90.0f;
        public float zoomSpeed                                  = 10.0f;
        public Vector3 target                                   { get { return transTarget != null ? transTarget.position : vec3Target; } }

        [SerializeField] private FloatRange zoomRange           = new FloatRange(2.0f, 5.0f);
        [SerializeField] private FloatRange tiltRange           = new FloatRange(0.0f, 180.0f);

        [SerializeField] private Vector3 angles                 = Vector3.zero;
        [SerializeField] private Vector3 fixedOffset            = Vector3.up;
        private Vector3 vec3Target                              = Vector3.zero;
        [SerializeField, ReadOnly] private Transform transTarget= null;
        private Vector3 track                                   = Vector3.zero;

        //
        // public methods /////////////////////////////////////////////////////
        //
        
        public void SetTarget(Vector3 position)
        {
            vec3Target = position;
            track = vec3Target;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void SetTarget(Transform transform)
        {
            transTarget = transform;
            track = transTarget.position;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void Zoom(float delta)
        {
            angles.z += delta * zoomSpeed;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void Move(Vector2 delta)
        {
            angles.x += delta.x;
            angles.y += delta.y;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public Vector3 SetAngles(Vector3 angles)
        {
            this.angles = CleanAngles(angles);
            return this.angles;
        }

        //
        // --------------------------------------------------------------------
        //

        public Vector3 GetAngles()
        {
            return angles;
        }

        //
        // --------------------------------------------------------------------
        //

        public Vector3 CleanAngles(Vector3 a)
        {
            a.x = a.x % 360.0f;
            a.y = tiltRange.Clamp(a.y);
            a.z = zoomRange.Clamp(a.z);
            return a;
        }

        //
        // btCamera ///////////////////////////////////////////////////////////
        //
        
        public override void UpdateCamera(float dt)
        {
            angles = CleanAngles(angles); 
            Quaternion rot = Quaternion.AngleAxis(angles.x, Vector3.up);
            Quaternion tilt = Quaternion.AngleAxis(-angles.y, Vector3.left);
            Quaternion comb = rot * tilt;
            transform.rotation = comb;

            Vector3 moveDir = comb * Vector3.back;
            Vector3 offset = (moveDir * angles.z) + transform.TransformDirection(fixedOffset);
            transform.position = target + offset;
        }

        //
        // --------------------------------------------------------------------
        //
    }
}
