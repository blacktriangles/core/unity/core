//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    public class ShoulderCamera
        : btCamera
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        [System.Serializable]
        public struct Offset 
        {
            public Vector3 fixedOffset;
            public float range;

            public Offset Lerp(Offset dst, float perc)
            {
                return new Offset() {
                    fixedOffset = Vector3.Lerp(fixedOffset, dst.fixedOffset, perc),
                    range = btMath.Lerp(range, dst.range, perc)
                };
            }
        }

        //
        // --------------------------------------------------------------------
        //

        private class TransitionData
        {
            public Offset start;
            public Offset end;
            public float duration                               = 0f;
            public float elapsed                                = 0f;

            public Offset Lerp() 
            {
                if(duration <= 0f)
                    return end;

                return start.Lerp(end, elapsed/duration);
            }
        }
        
        
        //
        // members ////////////////////////////////////////////////////////////
        //

        [Header("Shoulder Camera")]
        public float tilt                                       = 0f;

        public Transform target                                 { get; private set; }
        public Offset offset                                    = new Offset();
        
        private TransitionData tansition                          = null;

        //
        // public methods /////////////////////////////////////////////////////
        //
        
        public void SetTarget(Transform newTarget)
        {
            target = newTarget;
        }

        //
        // ------------------------------------------------------------------------
        //
        
        public void Transition(Offset newOffset, float duration)
        {
            tansition = new TransitionData() {
                start = offset,
                end = newOffset,
                duration = duration,
                elapsed = 0f
            };
        }

        //
        // btCamera ///////////////////////////////////////////////////////////
        //
        
        public override void UpdateCamera(float dt)
        {
            base.UpdateCamera(dt);

            if(target == null)
                return;

            if(tansition != null)
            {
                tansition.elapsed += dt;
                offset = tansition.Lerp();
                if(tansition.elapsed >= tansition.duration)
                {
                    offset = tansition.end;
                    tansition = null;
                }
            }

            Quaternion tiltRot = Quaternion.AngleAxis(tilt, Vector3.right);

            transform.rotation = target.rotation * tiltRot;
            transform.position = 
                target.TransformDirection(offset.fixedOffset) + target.position - (transform.forward * offset.range);

        }
    }
}
