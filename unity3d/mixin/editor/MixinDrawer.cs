//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace blacktriangles
{
    [CustomPropertyDrawer(typeof(MixinAttribute))]
    public class MixinDrawer
        : PropDrawer<string>
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        private static Dictionary<string, object> cache         = new Dictionary<string, object>();

        private float lineHeight                                = 0;
        
        public float calculatedHeight                           = 0;
        
        //
        // public methods /////////////////////////////////////////////////////
        //

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            if(lineHeight <= 0f)
            {
                lineHeight = base.GetPropertyHeight(property, label);
            }

            return calculatedHeight;
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        public override void OnGUI(Rect position, SerializedProperty prop, GUIContent label)
        {
            EditorGUI.BeginChangeCheck();

            MixinAttribute attr = attribute as MixinAttribute;
            string key = prop.serializedObject.targetObject.GetInstanceID() + '|' + prop.propertyPath;

            float height = 0;
            if(!cache.ContainsKey(key))
            {
                height += lineHeight;
                int sel = EditorGUI.Popup(position, -1, attr.names);
                if(attr.types.IsValidIndex(sel))
                {
                    cache[key] = System.Activator.CreateInstance(attr.types[sel]);
                }
            }

            if(cache.ContainsKey(key))
            {
                object obj = cache[key];
                height += btGui.ObjectReflectionGUI(position, ref obj);
            }

            if(height != calculatedHeight)
            {
                calculatedHeight = height;
                EditorUtility.SetDirty(prop.serializedObject.targetObject);
                Repaint(prop);
            }

            if(EditorGUI.EndChangeCheck())
            {
                EditorUtility.SetDirty(prop.serializedObject.targetObject);
            }
        }
    }
}
