//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    public class MixinAttribute
        : PropertyAttribute
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public System.Type[] types                              { get; private set; }
        public string[] names                                   { get; private set; }

        //
        // constructor / initializer  /////////////////////////////////////////
        //
        
        public MixinAttribute(System.Type baseType)
        {
            #if UNITY_EDITOR
                System.Type serializable = typeof(System.SerializableAttribute);
                SetTypes(AssemblyUtility.CollectAllTypes((t)=>{
                    return (
                        !t.IsAbstract && 
                        baseType.IsAssignableFrom(t) &&
                        t.IsDefined(serializable, true)
                    );
                }));
            #endif
        }

        //
        // --------------------------------------------------------------------
        //

        public MixinAttribute(System.Type[] _types)
        {
            SetTypes(_types);
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private void SetTypes(System.Type[] _types)
        {
            #if UNITY_EDITOR
            types = _types;
            names = new string[types.Length];
            for(int idx = 0; idx < types.Length; ++idx)
            {
                names[idx] = types[idx].Name;
            }
            #endif
        }
    }
}
