//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

namespace blacktriangles.Graph
{
    public class SleepNode
        : StateNode
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        [Input] public float durationSec                        = 1.0f;

        private float accumilator                               = 0.0f;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override void OnEnter(StateNode prev)
        {
            base.OnEnter(prev);
            accumilator = 0.0f;
        }

        //
        // --------------------------------------------------------------------
        //
        
        
        public override void OnUpdate(float dt)
        {
            accumilator += dt;
            if(accumilator > durationSec)
            {
                Complete();
            }
        }
    }
}
