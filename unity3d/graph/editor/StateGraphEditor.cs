//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using blacktriangles;

namespace blacktriangles.Graph
{
    [CustomNodeGraphEditor(typeof(StateGraph))]
    public class StateGraphEditor
        : XNodeEditor.NodeGraphEditor
    {
        public override string GetNodeMenuName(System.Type type)
        {
            if(type.Namespace == "blacktriangles.Graph")
            {
                return type.Name;
            }

            return null;
        }
    }
}
