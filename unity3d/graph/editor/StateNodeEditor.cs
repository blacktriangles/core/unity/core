//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using blacktriangles;
using UnityEngine;
using UnityEditor;
using XNode;
using XNodeEditor;

namespace blacktriangles.Graph
{
    [CustomNodeEditor(typeof(StateNode))]
    public class StateNodeEditor
        : XNodeEditor.NodeEditor
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        private GUIStyle defaultStyle                           = null;
        private GUIStyle activeStyle                            = null;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override void OnBodyGUI() 
        {
            base.OnBodyGUI();
            OnActiveGUI();
        }

        //
        // --------------------------------------------------------------------
        //

        protected void OnActiveGUI()
        {
            if(defaultStyle == null)
            {
                defaultStyle = XNodeEditor.NodeEditorResources.styles.nodeHeader;
            }

            if(activeStyle == null)
            {
                activeStyle = new GUIStyle(defaultStyle);
                activeStyle.normal.background = GUIStyles.orange;
                activeStyle.normal.textColor = Color.white;
            }

            StateNode node = target as StateNode;

            if(node.startNode)
            {
                GUILayout.Label("[START]", activeStyle, GUILayout.Height(30));
            }

            if(node.active)
            {
                GUILayout.Label("[ACTIVE]", activeStyle, GUILayout.Height(30));
            }
        }
    }
}
