//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using blacktriangles;
using UnityEngine;
using UnityEditor;
using XNode;
using XNodeEditor;

namespace blacktriangles.Graph
{
    [CustomNodeEditor(typeof(CompareNode))]
    public class CompareNodeEditor
        : StateNodeEditor
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        private SerializedProperty lhsprop = null;
        private SerializedProperty rhsprop = null;
        
        //
        // public methods /////////////////////////////////////////////////////
        //

        public override void OnBodyGUI() 
        {
            serializedObject.Update();
            if(lhsprop == null) lhsprop = serializedObject.FindProperty("lhs");
            if(rhsprop == null) rhsprop = serializedObject.FindProperty("rhs");

            CompareNode cnode = target as CompareNode;
            
            NodePort enterPort = cnode.GetInputPort("enter");
            NodePort equalsPort = cnode.GetOutputPort("exit");
            NodePort greaterPort = cnode.GetOutputPort("exitGreaterThan");
            NodePort lessPort = cnode.GetOutputPort("exitLessThan");

            NodeEditorGUILayout.PortField(new GUIContent("Enter"), enterPort);
            NodeEditorGUILayout.PropertyField(lhsprop);
            NodeEditorGUILayout.PropertyField(rhsprop);
            NodeEditorGUILayout.PortField(new GUIContent("Equals"), equalsPort);
            NodeEditorGUILayout.PortField(new GUIContent("Greater Than"), greaterPort);
            NodeEditorGUILayout.PortField(new GUIContent("Less Than"), lessPort);

            serializedObject.ApplyModifiedProperties();

            OnActiveGUI();
        }
    }
}
