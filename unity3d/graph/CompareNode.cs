//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

namespace blacktriangles.Graph
{
    public class CompareNode
        : StateNode
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        [Input] public float lhs                                = 0.0f;
        [Input] public float rhs                                = 0.0f;

        [Output] public NoValue exitGreaterThan;
        [Output] public NoValue exitLessThan;

        //
        // public methods /////////////////////////////////////////////////////
        //
        
        public override void OnUpdate(float dt)
        {
            if(lhs == rhs)
            {
                Complete();
            }
            else if(lhs < rhs)
            {
                Complete("exitLessThan");
            }
            else if(lhs > rhs)
            {
                Complete("exitGreaterThan");
            }
        }
    }
}
