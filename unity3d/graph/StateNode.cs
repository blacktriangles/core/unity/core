//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using blacktriangles;
using System.Reflection;
using UnityEngine;

namespace blacktriangles.Graph
{
    [System.Serializable]
    public abstract class StateNode
        : BaseNode
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public new StateGraph graph                             { get { return base.nodeGraph as StateGraph; } }
        public bool startNode                                   { get { return graph.nodes[0] == this; } }
        public bool active                                      { get { return graph.current == this; } }

        [Input] public NoValue start;
        [Output] public NoValue complete;

        //
        //
        // public methods /////////////////////////////////////////////////////
        //

        public void DoUpdate(float deltaTime)
        {
            OnUpdate(deltaTime);
        }

        //
        // --------------------------------------------------------------------
        //

        public abstract void OnUpdate(float deltaTime);

        //
        // ------------------------------------------------------------------------
        //

        [ContextMenu("Force Complete")]
        public void Complete()
        {
            Complete("complete");
        }

        //
        // --------------------------------------------------------------------
        //
       
        public void Complete(string portName)
        {
            Dbg.Assert(graph != null, "Graph is not a scene graph?");
            if(graph.current != this)
            {
                Dbg.Error("Complete called on a non active state?");
                return;
            }

            XNode.NodePort exitPort = GetOutputPort(portName);

            StateNode next = null;
            if(exitPort.IsConnected)
            {
                next = exitPort.Connection.node as StateNode;
            }

            OnComplete(next, exitPort);

            if(next)
            {
                graph.current = next;
                next.OnEnter(this);
            }
            else
            {
                graph.current = null;
            }
        }

        //
        // --------------------------------------------------------------------
        //

        [ContextMenu("Make Start Node")]
        public void SetActiveNode()
        {
            graph.nodes.Remove(this);
            graph.nodes.Insert(0, this);
        }
        

        //
        // --------------------------------------------------------------------
        //

        public virtual void OnEnter(StateNode prev)
        {
            System.Type type = GetType();
            FieldInfo[] fields = type.GetFields();
            foreach(FieldInfo field in fields)
            {
                object val = null;
                if(GetInputValue(field.Name, out val))
                {
                    field.SetValue(this, val);
                }
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        public virtual void OnComplete(StateNode next, XNode.NodePort exitPortUsed)
        {
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        [ContextMenu("Force Update")]
        private void ForceUpdate()
        {
            DoUpdate(0.0f);
        }
    }
}
