//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using blacktriangles;
using UnityEngine;

namespace blacktriangles.Graph
{
    [CreateAssetMenu(fileName="New StateGraph", menuName="blacktriangles/StateGraph")]
    public class StateGraph
        : BaseGraph
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public StateNode current;                                

        private bool started                                    = false;

        //
        // constructor / initializer //////////////////////////////////////////
        //
        
        public override void Initialize()
        {
            started = false;
            current = nodes[0] as StateNode;
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public T GetNode<T>()
            where T: StateNode
        {
            return nodes.Find((node)=>{
                return node.GetType() == typeof(T);
            }) as T;
        }

        //
        // --------------------------------------------------------------------
        //
        

        public void Start()
        {
            Dbg.Assert(isInstance, "Attempting to start a StateGraph that is a prefab, not an instance.  Instantiate it before using it");
            if(current != null)
            {
                current.OnEnter(null);
            }

            started = true;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public bool DoUpdate(float deltaTime)
        {
            bool result = current != null;

            if(result && started)
            {
                current.DoUpdate(deltaTime);
            }

            return result;
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //
    }
}
