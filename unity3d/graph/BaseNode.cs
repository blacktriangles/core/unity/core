//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using blacktriangles;
using System;
using System.Reflection;
using UnityEngine;
using XNode;

namespace blacktriangles.Graph
{
    [System.Serializable]
    public abstract class BaseNode
        : XNode.Node
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        [System.Serializable]
        public struct NoValue
        {
        }

        //
        // members ////////////////////////////////////////////////////////////
        //

        public XNode.NodeGraph nodeGraph                        { get { return base.graph; } }
        public new BaseGraph graph                              { get { return nodeGraph as BaseGraph; } }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public bool GetInputValue(string fieldName, out object value)
        {
            bool result = false;

            value = null;
            NodePort port = GetInputPort(fieldName);
            if(port != null && port.IsConnected)
            {
                result = true;
                value = port.GetInputValue();
            }

            return result;
        }

        //
        // ------------------------------------------------------------------------
        //
        
        public override object GetValue(NodePort port)
        {
            object result = null;
            FieldInfo fld = GetType().GetField(port.fieldName);
            if(fld != null)
            {
                result = fld.GetValue(this);
            }

            return result;
        }

        //
        // protected methods //////////////////////////////////////////////////
        //

        protected override void Init()
        {
            base.Init();
            AddDynamicPorts();
        }

        //
        // --------------------------------------------------------------------
        //
        
        protected virtual void AddDynamicPorts()
        {
        }

        //
        // --------------------------------------------------------------------
        //
        
        protected void AddInput(System.Type type, string fieldName)
        {
            if(!HasPort(fieldName))
            {
                AddDynamicInput(type, fieldName: fieldName);
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        protected void AddOutput(System.Type type, string fieldName)
        {
            if(!HasPort(fieldName))
            {
                AddDynamicOutput(type, fieldName: fieldName);
            }
        }
    }
}

