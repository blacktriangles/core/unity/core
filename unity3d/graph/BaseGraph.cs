//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using blacktriangles;
using UnityEngine;

namespace blacktriangles.Graph
{
    public abstract class BaseGraph
        : XNode.NodeGraph
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public bool isInstance                                  { get { return _isInstance; } set { _isInstance = value; } }

        [SerializeField, ReadOnly] bool _isInstance             = false;

        //
        // constructor / initializer //////////////////////////////////////////
        //
        
        public static T FromPrefab<T>(T prefab)
            where T : BaseGraph
        {
            T result = prefab.Copy() as T;
            result.isInstance = true;
            result.Initialize();
            return result;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public virtual void Initialize()
        {
        }
    }
}
