//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using blacktriangles;

namespace blacktriangles.Graph
{
    public class UIDialogNode
        : StateNode
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        [Output] public NoValue exitCancel;

        public UIDialog dialog;
        public UIDialog.Request request;
        
        //
        // StateNode //////////////////////////////////////////////////////////
        //

        public override void OnEnter(StateNode prev)
        {
            request.callback = (UIDialog dialog, UIDialog.Request request, UIDialog.Response response)=>{
                switch(response)
                {
                    case UIDialog.Response.Confirm: Complete(); break;
                    case UIDialog.Response.Cancel: Complete("exitCancel"); break;
                }
            };

            dialog.OpenAsync(request);

            base.OnEnter(prev);
        }

        //
        // --------------------------------------------------------------------
        //

        public override void OnUpdate(float dt)
        {
        }
    }
}
