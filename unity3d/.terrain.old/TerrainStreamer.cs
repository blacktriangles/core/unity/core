//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Collections;
using UnityEngine;

namespace blacktriangles
{
    [ExecuteInEditMode]
    public class TerrainStreamer
        : MonoBehaviour
        , ITerrainDetailPoolProvider
    {
        //
        // constants //////////////////////////////////////////////////////////
        //

        private const int kTileCount                            = 3;
        
        //
        // members ////////////////////////////////////////////////////////////
        //

        [Header("State")]
        public Vector2Int position                              = Vector2Int.zero;
        public bool initialized                                 { get; private set; }
        public bool busy                                        { get; private set; }

        [Header("Generator")]
        public int seed                                         = 0;
        public TerrainBiome biome                               = null;
        public TerrainGenerator generatorPrefab                 = null;
        

        public GameObjectPools detailPool                       { get { return _detailPool; } }
        [SerializeField] private GameObjectPools _detailPool    = new GameObjectPools();
        private TerrainGenerator[,] terrains = new TerrainGenerator[kTileCount, kTileCount];

        //
        // public methods /////////////////////////////////////////////////////
        //
        
        public IEnumerator Initialize()
        {

            if(busy) yield break;
            
            double start = Epoch.now;

            initialized = false;
            busy = true;
            if(terrains != null)
            {
                for(int x = 0; x < kTileCount; ++x)
                {
                    for(int y = 0; y < kTileCount; ++y)
                    {
                        TerrainGenerator generator = terrains[x,y];
                        if(generator != null)
                        {
                            generator.ClearDetails();
                        }

                        generator = UnityUtils.SafeDestroy(generator);
                        generator = Instantiate(generatorPrefab) as TerrainGenerator;

                        generator.transform.SetParent(transform);
                        Vector3Int offset = new Vector3Int(x-1,0,y-1);
                        generator.Initialize(this, biome, offset, seed);
                        generator.name = $"[Tile]({offset.x},{offset.z})";
                        terrains[x,y] = generator;

                        yield return generator.GenerateAsync();
                    }
                }
            }

            initialized = true;
            busy = false;

            Dbg.Log($"Built world in {Epoch.now-start}");
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Awake()
        {
            initialized = false;
            busy = false;
        }

        //
        // editor /////////////////////////////////////////////////////////////
        //

        #if UNITY_EDITOR

        [ContextMenu("Generate")]
        private void EditorGenerate()
        {
            if(busy) return;
            StartCoroutine(Initialize());
        }
        #endif
        
    }
}
