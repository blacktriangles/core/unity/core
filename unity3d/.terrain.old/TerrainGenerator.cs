//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace blacktriangles
{
    public interface ITerrainDetailPoolProvider
    {
        GameObjectPools detailPool                              { get; }
    }

    //
    // ------------------------------------------------------------------------
    //

    public class TerrainGenerator
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public Terrain terrain                                  { get; private set; }
        public TerrainData data                                 { get; private set; }
        public TerrainBiome biome                               { get; private set; }

        public Vector3 size                                     { get { return biome.preset.size; } }

        [Header("Settings")]
        public Vector3Int offset;
        public Vector3 worldOffset;

        [Header("Runtime")]
        private int seed;
        private System.Random rnd;
        private GameObjectPools detailPool                      = null;
        private List<TerrainLayer> layers                       = null;
        private List<GameObject> details                        = new List<GameObject>();
        private double kMinFrameTime                            = 1.0;

        
        //
        // public methods /////////////////////////////////////////////////////
        //
        
        public void Initialize(ITerrainDetailPoolProvider poolProvider, TerrainBiome _biome, Vector3Int _offset, int _seed)
        {
            detailPool = poolProvider.detailPool;
            biome = _biome;
            seed = _seed;
            rnd = new System.Random(seed);
            data = Instantiate(biome.preset);
            
            //
            // setup global offset
            //

            offset = _offset;

            worldOffset = new Vector3(
                _offset.x * data.size.x - data.size.x/2f - 1,
                0f,
                _offset.z * data.size.z - data.size.z/2f - 1
            );

            //
            // create and position patch
            //

            terrain = Terrain.CreateTerrainGameObject(data).GetComponent<Terrain>();
            transform.localPosition = worldOffset;
            terrain.transform.SetParent(transform);
            terrain.transform.localPosition = Vector3.zero;
            terrain.transform.localRotation = Quaternion.identity;
            
            //
            // get terrain layers
            //

            layers = biome.AllLayers();
            data.terrainLayers = layers.ToArray();
        }

        //
        // --------------------------------------------------------------------
        //

        public void ClearDetails()
        {
            foreach(GameObject detail in details)
            {
                detailPool.Return(detail);
            }
            details.Clear();
        }

        //
        // --------------------------------------------------------------------
        //

        public IEnumerator GenerateAsync()
        {
            yield return GenerateGeometry();
            yield return GenerateLayers();
            yield return GenerateDetails();
            kMinFrameTime = 1.0 / 60.0;
        }

        //
        // private methods ////////////////////////////////////////////////////
        //
        
        private IEnumerator GenerateGeometry()
        {
            double dt = Epoch.now;
            int res = biome.preset.heightmapResolution;
            float[,] heights = new float[res,res];
            for(int x = 0; x < res; ++x)
            {
                for(int y = 0; y < res; ++y)
                {
                    Vector3 pos = new Vector3(x-offset.x,0f,y-offset.z) + (offset*res);
                    float sample = 0f;
                    foreach(PerlinNoise noise in biome.geometry.layers)
                    {
                        sample += noise.Sample(pos.ToVector2XZ());
                    }
                    sample = btMath.Clamp(sample, 0f, 1f);
                    heights[y,x] = sample;

                    if(Epoch.now - dt > kMinFrameTime)
                    {
                        yield return null;
                        dt = Epoch.now;
                    }
                }
            }

            data.SetHeights(0,0,heights);
        }

        //
        // --------------------------------------------------------------------
        //
        
        private IEnumerator GenerateLayers()
        {
            double dt = Epoch.now;
            int count = layers.Count;
            int res = biome.preset.alphamapResolution;

            Vector2 step = new Vector2(
                biome.preset.size.x / res,
                biome.preset.size.z / res
            );

            float[,,] splats = new float[res,res,count];
            foreach(TerrainBiome.LayerPass pass in biome.layers.passes)
            {
                int aboveIdx = layers.IndexOf(pass.above.texture);
                int belowIdx = layers.IndexOf(pass.below.texture);

                Dbg.Assert(aboveIdx >= 0 && aboveIdx < count, "Invalid above idx {aboveIdx}");
                Dbg.Assert(belowIdx >= 0 && belowIdx < count, "Invalid below idx {belowIdx}");
                
                for(int x = 0; x < res; ++x)
                {
                    for(int y = 0; y < res; ++y)
                    {
                        Vector2 pos = new Vector2(
                            worldOffset.x + x*step.x,
                            worldOffset.z + y*step.y
                        );
                        float samp = pass.noise.Sample(pos);

                        //
                        // set splats
                        //

                        float f = samp * pass.strength;
                        splats[y,x,aboveIdx] += f;
                        splats[y,x,belowIdx] += pass.strength - f;

                        if(Epoch.now - dt > kMinFrameTime)
                        {
                            yield return null;
                            dt = Epoch.now;
                        }
                    }
                }
            }

            data.SetAlphamaps(0,0,splats);
        }

        //
        // --------------------------------------------------------------------
        //

        private IEnumerator GenerateDetails()
        {
            double dt = Epoch.now;
            int res = biome.layers.detailResolution;
            Vector2 step = new Vector2(
                biome.preset.size.x / res,
                biome.preset.size.z / res
            );

            Vector3 jittermin = new Vector3(-step.x/2f, 0f, -step.y/2f);
            Vector3 jittermax = new Vector3(step.x/2f, 0f, step.y/2f);

            foreach(TerrainBiome.LayerPass pass in biome.layers.passes)
            {
                for(int x = 0; x < res; ++x)
                {
                    for(int y = 0; y < res; ++y)
                    {
                        Vector3 pos = new Vector3(
                            x * step.x,
                            0f,
                            y * step.y
                        );
                        float samp = pass.noise.Sample(worldOffset.ToVector2XZ()+pos.ToVector2XZ());

                        GenerateDetails(pass.below.details, pos, jittermin, jittermax, 1f-samp);
                        GenerateDetails(pass.above.details, pos, jittermin, jittermax, samp);
                        if(Epoch.now - dt > kMinFrameTime)
                        {
                            yield return null;
                            dt = Epoch.now;
                        }
                    }
                }
            }
        }

        //
        // --------------------------------------------------------------------
        //

        private void GenerateDetails(
                TerrainBiome.Detail[] details, 
                Vector3 worldpos, 
                Vector3 jittermin, 
                Vector3 jittermax, 
                float samp
            )
        {
            foreach(TerrainBiome.Detail detail in details)
            {
                int density = (int)Mathf.Round(detail.density*samp);
                for(int i = 0; i < density; ++i)
                {
                    Vector3 pos = worldpos + Vector3Extension.Random(jittermin, jittermax, rnd);
                    Vector2 interp = new Vector2(
                        pos.x / terrain.terrainData.size.x,
                        pos.z / terrain.terrainData.size.z
                    );

                    pos.y = terrain.terrainData.GetInterpolatedHeight(interp.x, interp.y);
                    Quaternion rot = Quaternion.AngleAxis(rnd.Next(0f,360f), Vector3.up);
                    GameObject det = detailPool.Take(detail.prefab, pos, rot, transform);
                    this.details.Add(det);
                }
            }
        }
    }
}
