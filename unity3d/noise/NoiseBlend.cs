//
// (c) BLACKTRIANGLES 2021
// http://www.blacktriangles.com
//

namespace blacktriangles
{
    public enum NoiseBlendType
    {
        Multiply,
        Divide,
        Add,
        Subtract,
        Average,
        Screen,
        Overlay
    }

    //
    // ------------------------------------------------------------------------
    //

    public static class NoiseBlend
    {
        static public float Blend(float lhs, float rhs, NoiseBlendType type, float intensity)
        {
            float result = 0f;
            switch(type)
            {
                //
                // ----------------------------------------------------------------
                //
                
                case NoiseBlendType.Multiply:
                    result = lhs*rhs;
                break;

                //
                // ----------------------------------------------------------------
                //

                case NoiseBlendType.Divide:
                    if(rhs <= 0f)
                    {
                        result = 0f;
                    }
                    else
                    {
                        result = lhs/rhs;
                    }
                break;

                //
                // ----------------------------------------------------------------
                //

                case NoiseBlendType.Add:
                    result = lhs+rhs;
                break;

                //
                // ----------------------------------------------------------------
                //

                case NoiseBlendType.Subtract:
                    result = lhs-rhs;
                break;

                //
                // ----------------------------------------------------------------
                //

                case NoiseBlendType.Average:
                    result = (lhs+rhs)/2f;
                break;

                //
                // ----------------------------------------------------------------
                //

                case NoiseBlendType.Screen:
                    result = 1-(1-lhs)*(1-rhs);
                break;

                //
                // ----------------------------------------------------------------
                //

                case NoiseBlendType.Overlay:
                    if(lhs < 0.5f)
                    {
                        result = 2*lhs*rhs;
                    }
                    else
                    {
                        result = 1 - 2*(1-lhs)*(1-rhs);
                    }
                break;
            }

            return btMath.Lerp(lhs, btMath.Clamp(result, 0f, 1f), intensity);
        }
    }
}
