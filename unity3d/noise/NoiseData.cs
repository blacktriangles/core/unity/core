//
// (c) BLACKTRIANGLES 2021
// http://www.blacktriangles.com
//

using blacktriangles;
using UnityEngine;

namespace blacktriangles
{
    [CreateAssetMenu(fileName="NoiseData", menuName="blacktriangles/Noise/NoiseData")]
    public class NoiseData
        : ScriptableObject
    {
        //
        // members /////////////////////////////////////////////////////////////
        //
        
        public NoiseLayerData[] layers;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void Seed(int seed)
        {
            foreach(NoiseLayerData layer in layers)
            {
                layer.seed = seed;
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        
        public float Sample(Vector2 pos)
        {
            float last = -1f;
            foreach(NoiseLayerData layer in layers)
            {
                float next = layer.Sample(pos);
                if(last < 0f) 
                    last = next;
                else
                    last = layer.Blend(last, next);
            }

            return last;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public float Sample(Vector3 pos)
        {
            float last = -1f;
            foreach(NoiseLayerData layer in layers)
            {
                float next = layer.Sample(pos);
                if(last < 0f)
                    next = last;
                else
                    last = layer.Blend(last, next);
            }
            return last;
        }
    }
}
