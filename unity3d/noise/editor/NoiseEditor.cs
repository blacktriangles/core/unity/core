//
// (c) BLACKTRIANGLES 2021
// http://www.blacktriangles.com
//

using UnityEditor;
using UnityEngine;

namespace blacktriangles
{
    public class NoiseViewer
        : EditorWindow
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        public enum Dimensions
        {
            Square128,
            Square256,
            Square512,
            Square1024,
            Square2048
        }
        
        //
        // members ////////////////////////////////////////////////////////////
        //

        private NoiseData noise                                 = null;
        private int seed                                        = 0;
        private Dimensions dims                                 = Dimensions.Square512;
        private int size                                        = 512;
        private Texture2D texture                               = null;
        
        private GUIStyle sidebarStyle;
        private Rect textureRect                                = new Rect(0,0,0,0);

        //
        // initialize /////////////////////////////////////////////////////////
        //

        [MenuItem("Tools/blacktriangles/Noise/Noise Editor")]
        private static void ShowWindow()
        {
            NoiseViewer window = GetWindow<NoiseViewer>();
            window.title = "Noise Editor";
            window.Initialize();
            window.Show();
        }

        //
        // --------------------------------------------------------------------
        //

        private void Initialize()
        {
            texture = new Texture2D(512,512);
            minSize = new Vector2(400,300);
        }
        
        //
        // private methods ////////////////////////////////////////////////////
        //
        
        private void RefreshStyles()
        {
            // sidebar sizes
            float sidebarWidth = position.width * 0.40f;
            sidebarStyle = new GUIStyle(GUI.skin.box);
            sidebarStyle.fixedWidth = sidebarWidth;

            // texture rect
            textureRect.x = 5f; 
            textureRect.y = 5f;
            textureRect.width = position.width - sidebarWidth - 10f;
            textureRect.height = position.height - 10f;
        }

        //
        // --------------------------------------------------------------------
        //
        
        private void DrawSidebar()
        {
            noise = EditorGUILayout.ObjectField("Noise Data", noise, typeof(NoiseData), false) as NoiseData;
            if(noise != null)
            {
                seed = EditorGUILayout.IntField("Seed", seed);
                Dimensions newDims = (Dimensions)EditorGUILayout.EnumPopup("Dimensions", dims);
                if(newDims != dims)
                {
                    dims = newDims;
                    switch(newDims)
                    {
                        case Dimensions.Square128: size = 128; break;
                        case Dimensions.Square256: size = 256; break;
                        case Dimensions.Square512: size = 512; break;
                        case Dimensions.Square1024: size = 1024; break;
                        case Dimensions.Square2048: size = 2048; break;
                    }
                    texture = new Texture2D(size, size);
                }

                if(GUILayout.Button("Refresh"))
                {
                    Refresh();
                }
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        private void DrawImage()
        {
            EditorGUI.DrawPreviewTexture(textureRect, texture, null, UnityEngine.ScaleMode.ScaleToFit);
        }

        //
        // --------------------------------------------------------------------
        //
        
        private void Refresh()
        {
            Color[] colors = new Color[size*size];
            for(int y = 0; y < size; ++y)
            {
                for(int x = 0; x < size; ++x)
                {
                    int index = y * size + x;
                    float val = noise.Sample(new Vector2(x,y));
                    colors[index] = new Color(val, val, val, 1f);
                }
            }

            noise.Seed(seed);
            texture.SetPixels(colors);
            texture.Apply();
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //
        
        protected virtual void OnGUI()
        {
            RefreshStyles();
            GUILayout.BeginHorizontal();
                GUILayout.FlexibleSpace();
                GUILayout.BeginVertical(sidebarStyle);
                    DrawSidebar();
                    GUILayout.FlexibleSpace();
                GUILayout.EndVertical();
            GUILayout.EndHorizontal();

            DrawImage();
        }
        
    }
}
