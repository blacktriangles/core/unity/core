//
// (c) BLACKTRIANGLES 2021
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    [System.Serializable]
    public class NoiseLayerData
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        [System.Serializable]
        public struct FractalSettings
        {
            public FastNoiseLite.FractalType fractalType;
            public int octaves;
            public float lacunarity;
            public float gain;
            public float weightedStrength;
            public float pingPongStrength;

            public void Prepare(FastNoiseLite gen)
            {
                gen.SetFractalType(fractalType);
                gen.SetFractalOctaves(octaves);
                gen.SetFractalLacunarity(lacunarity);
                gen.SetFractalGain(gain);
                gen.SetFractalWeightedStrength(weightedStrength);
                gen.SetFractalPingPongStrength(pingPongStrength);
            }
        }

        //
        // --------------------------------------------------------------------
        //

        [System.Serializable]
        public struct CellularSettings
        {
            public FastNoiseLite.CellularDistanceFunction distanceFunction;
            public FastNoiseLite.CellularReturnType returnType;
            public float jitter;

            public void Prepare(FastNoiseLite gen)
            {
                gen.SetCellularDistanceFunction(distanceFunction);
                gen.SetCellularReturnType(returnType);
                gen.SetCellularJitter(jitter);
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        public struct WarpSettings
        {
            public bool enabled;
            public FastNoiseLite.DomainWarpType type;
            public float amp;

            public void Prepare(FastNoiseLite gen)
            {
                gen.SetDomainWarpType(type);
                gen.SetDomainWarpAmp(amp);
            }
        }
        
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        [Header("Base Settings")]
        public string name;
        [HideInInspector] public int seed;
        public FastNoiseLite.NoiseType type;
        public float frequency;

        private bool isFractal                                  { get { return type != FastNoiseLite.NoiseType.Cellular; } }
        private bool isCellular                                 { get { return type == FastNoiseLite.NoiseType.Cellular; } }

        [Header("Noise Settings")]
        [ShowIf("isFractal")] public FractalSettings fractalSettings;
        [ShowIf("isCellular")] public CellularSettings cellularSettings;
        public WarpSettings warpSettings;

        [Header("Blend Settings")]
        public NoiseBlendType blendMode;
        [Range(0f,1f)] public float intensity;

        private FastNoiseLite gen;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public float Sample(Vector2 pos)
        {
            Prepare();
            pos = DomainWarp(pos);
            return (gen.GetNoise(pos.x, pos.y)+1f)/2f;
        }

        //
        // --------------------------------------------------------------------
        //

        public float Sample(Vector3 pos)
        {
            Prepare();
            pos = DomainWarp(pos);
            return (gen.GetNoise(pos.x, pos.y, pos.z)+1f)/2f;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public float Blend(float lhs, float rhs)
        {
            return NoiseBlend.Blend(lhs, rhs, blendMode, intensity);
        }

        //
        // private methods ////////////////////////////////////////////////////
        //
        
        private void Prepare()
        {
            if(gen == null)
            {
                gen = new FastNoiseLite();
            }

            gen.SetSeed(seed);
            gen.SetFrequency(frequency);
            gen.SetNoiseType(type);
            fractalSettings.Prepare(gen);
            cellularSettings.Prepare(gen);
        }

        //
        // --------------------------------------------------------------------
        //

        private Vector2 DomainWarp(Vector2 pos)
        {
            if(warpSettings.enabled)
            {
                warpSettings.Prepare(gen);
                gen.DomainWarp(ref pos.x, ref pos.y);
            }

            return pos;
        }
        
        //
        // --------------------------------------------------------------------
        //

        private Vector3 DomainWarp(Vector3 pos)
        {
            if(warpSettings.enabled)
            {
                warpSettings.Prepare(gen);
                gen.DomainWarp(ref pos.x, ref pos.y, ref pos.z);
            }

            return pos;
        }
        
    }
}
