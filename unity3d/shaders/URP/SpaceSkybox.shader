//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

Shader "blacktriangles/URP/SpaceSkybox"
{
    //
    // properties /////////////////////////////////////////////////////////////
    //
    
    Properties
    {
        _Seed("Seed", Float) = 1.0

        _NebulaDensity("Nebula Density", Int) = 10
        _NebulaSteps("Nebula Steps", Int) = 5

        _Starfield("Star Field", CUBE) = "" {}
    }

    //
    // ########################################################################
    //
    
    CGINCLUDE

    //
    // includes ///////////////////////////////////////////////////////////////
    //

    #include "Lighting.cginc"
    #include "UnityCG.cginc"

    #include "includes/Conversions.cginc"
    #include "includes/NoiseCommon.cginc"
    #include "includes/Perlin.cginc"
    #include "includes/Simplex.cginc"

    //
    // variables //////////////////////////////////////////////////////////////
    //
    
    uniform float _Seed;
    uniform int _NebulaDensity, _NebulaSteps;
    samplerCUBE _Starfield;

    //
    // types //////////////////////////////////////////////////////////////////
    //
    
    // app -> vert
    struct appdata
    {
        float4 vertex : POSITION;
        float3 uv: TEXCOORD0;
    };

    //
    // functions //////////////////////////////////////////////////////////////
    //
    


    //
    // programs ///////////////////////////////////////////////////////////////
    //
    
    // vert -> frag
    struct v2f
    {
        float4 pos: SV_POSITION;
        float3 uv: TEXCOORD0;
        float3 viewDir: TEXCOORD1;
    };

    //
    // vertex program /////////////////////////////////////////////////////////
    //

    v2f vert(appdata v)
    {
        v2f o;
        o.pos = UnityObjectToClipPos(v.vertex);
        o.uv = v.uv;
        o.viewDir = normalize(WorldSpaceViewDir(v.vertex));
        return o;
    }

    //
    // fragment program ///////////////////////////////////////////////////////
    //
    
    float4 frag(v2f i) : COLOR
    {
        float seed = _Seed / 100.1234;
        float4 col = float4(1,0,0,1);

        col = texCUBE(_Starfield, i.viewDir);

        for(int colorIter = 0; colorIter < _NebulaDensity; ++colorIter)
        {
            float3 idx = float3(colorIter+seed, colorIter+100-seed, colorIter/100*seed) * seed;
            float4 color = float4(
                perlin(idx), 
                perlin(idx*123), 
                perlin(idx+100*156), 
                1
            )*0.5;

            for(int iter = 0; iter < _NebulaSteps; ++iter)
            {
                float2 offset = float2(iter + 100, iter - 100) * 0.5 * iter * seed + seed;
                col += max(0, color*perlin(i.viewDir + offset) * 0.05);
            }
        }

        return col;
    }

    ENDCG

    //
    // ########################################################################
    //
    
    SubShader
    {
        Tags 
        { 
            "RenderType" = "Skybox"
            "Queue" = "Background"
            "PreviewType" = "Skybox"
        }

        Pass
        {
            ZWrite Off
            Cull Off
            Fog { Mode Off }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            ENDCG
        }
    }
}
