//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    public class SeededMaterials
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public Material[] materials;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void Randomize(int seed)
        {
            System.Random rnd = new System.Random(seed);
            foreach(Material mat in materials)
            {
                mat.SetFloat("_Seed", rnd.Next());
            }
        }
        
    }
}
