//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    [CreateAssetMenu(fileName="ProceduralSpaceSettings", menuName="blacktriangles/procedural/ProceduralSpaceSetings")]
    public class ProceduralSpaceSettings
        : ScriptableObject
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        [Header("Star")]
        public bool starSize                                    = false;
        [ShowIf("starSize")] public Vector2 starSizeRange       = new Vector2(100,1000);

        public bool starDensity                                 = false;
        [ShowIf("starDensity")] public Vector2 starDensityRange = new Vector2(0.125f,0.2f);

        public bool starColor                                   = false;
        [ShowIf("starColor")] public bool fullRandomStarColor   = false;
        private bool showStarColor                              { get { return starColor && !fullRandomStarColor; } }
        [ShowIf("showStarColor")] public Color starColorA       = Color.white;
        [ShowIf("showStarColor")] public Color starColorB       = Color.yellow;

        [Header("Nebulae")]
        public bool nebulaColor                                 = false;
        [ShowIf("nebulaColor")] public bool fullRandomNebulaColor = false;
        private bool showNebulaColor                            { get { return nebulaColor && !fullRandomNebulaColor; } }
        [ShowIf("showNebulaColor")] public Color nebulaColorA       = Color.black;
        [ShowIf("showNebulaColor")] public Color nebulaColorB       = Color.white;

        public bool nebulaParallaxColor                         = false;
        [ShowIf("nebulaParallaxColor")] public bool fullRandomParallaxColor = false;
        private bool showParallaxColor                          { get { return nebulaParallaxColor && !fullRandomParallaxColor; } }
        [ShowIf("showParallaxColor")] public Color nebulaParallaxColorA = Color.red;
        [ShowIf("showParallaxColor")] public Color nebulaParallaxColorB = Color.blue;

        public bool nebulaStrength                              = false;
        [ShowIf("nebulaStrength")] public Vector2 nebulaStrengthRange = new Vector2(0f, 0.25f);

        public bool nebulaParallax                              = false;
        [ShowIf("nebulaParallax")] public Vector2 nebulaParallaxRange = new Vector2(0f, 0.25f);

        //
        // public methods /////////////////////////////////////////////////////
        //

        public static ProceduralSpaceSettings Randomize()
        {
            ProceduralSpaceSettings settings = ScriptableObject.CreateInstance<ProceduralSpaceSettings>();

            // star
            settings.starSize = true;
            settings.starDensity = true;

            settings.starColor = true;
            settings.fullRandomStarColor = true;
            
            settings.nebulaColor = true;
            settings.fullRandomNebulaColor = true;

            settings.nebulaParallaxColor = true;
            settings.fullRandomParallaxColor = true;

            settings.nebulaStrength = true;
            settings.nebulaParallax = true;
            
            return settings;
        }

        //
        // ------------------------------------------------------------------------
        //

        public void Apply(int seed)
        {
            System.Random rnd = new System.Random(seed);
            Material sky = RenderSettings.skybox;

            sky.SetFloat("_Seed", rnd.Next(0,1000000));

            if(starSize)
            {
                sky.SetFloat("_StarSize", rnd.Next(starSizeRange.x, starSizeRange.y));
            }

            if(starDensity)
            {
                sky.SetFloat("_StarDensity", rnd.Next(starDensityRange.x, starDensityRange.y));
            }

            if(starColor)
            {
                Color color = Color.Lerp(starColorA, starColorB, rnd.Next(0f,1f));
                if(fullRandomStarColor)
                {
                    color = RandomStarColor(rnd);
                }
                sky.SetColor("_StarColor", color);
            }

            if(nebulaColor)
            {
                Color color = Color.Lerp(nebulaColorA, nebulaColorB, rnd.Next(0f,1f));
                if(fullRandomNebulaColor)
                {
                    color = RandomNebulaColor(rnd);
                }
                sky.SetColor("_NebulaColor", color);
            }

            if(nebulaParallaxColor)
            {
                Color color = Color.Lerp(nebulaParallaxColorA, nebulaParallaxColorA, rnd.Next(0f,1f));
                if(fullRandomParallaxColor)
                {
                    color = RandomNebulaColor(rnd);
                }
                sky.SetColor("_NebulaPlxColor", color);
            }

            if(nebulaStrength)
            {
                sky.SetFloat("_NebulaStr", rnd.Next(nebulaStrengthRange.x, nebulaStrengthRange.y));
            }

            if(nebulaParallax)
            {
                sky.SetFloat("_NebulaPlx", rnd.Next(nebulaParallaxRange.x, nebulaParallaxRange.y));
            }
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private static Color RandomColor(System.Random rnd, float min, float max)
        {
            return new Color(rnd.Range(min, max), rnd.Range(min, max), rnd.Range(min,max));
        }

        //
        // --------------------------------------------------------------------
        //
        
        private static Color RandomStarColor(System.Random rnd)
        {
            return RandomColor(rnd, 0.5f, 1f);
        }

        //
        // --------------------------------------------------------------------
        //

        private static Color RandomNebulaColor(System.Random rnd)
        {
            return RandomColor(rnd, 0.1f, 0.4f);
        }
    }
}
