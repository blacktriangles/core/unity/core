//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    public class ProceduralSpace
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public int seed                                         = 0;
        public ProceduralSpaceSettings[] options                = null;
        public bool randomizeOnAwake                            = true;

        [ReadOnly] public ProceduralSpaceSettings settings      = null;
        
        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Awake()
        {
            if(seed == 0)
            {
                seed = btRandom.RandomInt();
            }

            System.Random rnd = new System.Random(seed);
            settings = null;
            if(options != null && options.Length > 0)
            {
                settings = options.Random(rnd);
            }

            if(settings == null)
            {
                settings = ProceduralSpaceSettings.Randomize();
            }

            settings.Apply(seed);
        }
    }
}
