//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

Shader "blacktriangles/Noise"
{
    //
    // properties /////////////////////////////////////////////////////////////
    //
    
    Properties
    {
        _ColorA("Color A", Color) = (.5, .5, .5, 1)
        _ColorB("Color B", Color) = (.1, .1, .1, 1)
        _Factor("Factor", Float) = 1.0
    }

    //
    // ########################################################################
    //
    
    CGINCLUDE

    //
    // includes ///////////////////////////////////////////////////////////////
    //

    #include "Lighting.cginc"
    #include "UnityCG.cginc"

    #include "includes/Conversions.cginc"
    #include "includes/NoiseCommon.cginc"
    #include "includes/Perlin.cginc"

    //
    // variables //////////////////////////////////////////////////////////////
    //
    
    uniform float3 _ColorA, _ColorB;
    uniform float _Factor;

    //
    // types //////////////////////////////////////////////////////////////////
    //
    
    // app -> vert
    struct appdata
    {
        float4 vertex : POSITION;
        float3 tex0: TEXCOORD0;
    };

    //
    // ------------------------------------------------------------------------
    //
    
    // vert -> frag
    struct v2f
    {
        float4 pos: SV_POSITION;
        float3 tex0: TEXCOORD0;
        float3 tex1: TEXCOORD1;
    };

    //
    // vertex program /////////////////////////////////////////////////////////
    //

    v2f vert(appdata v)
    {
        v2f o;
        o.pos = UnityObjectToClipPos(v.vertex);
        o.tex0 = v.tex0;
        o.tex1 = v.vertex.xyz;
        return o;
    }

    //
    // fragment program ///////////////////////////////////////////////////////
    //
    
    float4 frag(v2f i) : COLOR
    {
        float p = perlin(i.tex0*_Factor);

        float3 col = lerp(_ColorA, _ColorB, p);

        return float4(col, 1.0);
    }

    ENDCG

    //
    // ########################################################################
    //
    
    SubShader
    {
        Tags 
        { 
            "RenderType" = "Skybox"
            "Queue" = "Background"
        }

        Pass
        {
            ZWrite Off
            Cull Off
            Fog { Mode Off }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            ENDCG
        }
    }
}
