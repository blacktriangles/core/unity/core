//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    [RequireComponent(typeof(Renderer))]
    public class Dissolver
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
          
        private Material dissolveMat                            = null;
        public Transform target                                 = null;
        public btCamera btcam                                   = null;
        public float decay                                      = 1f;
        public float speed                                      = 1f;

        [ReadOnly] private bool dissolving                      = false;
        [ReadOnly] private float dissolved                      = 0f;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void Dissolve(Transform trg = null)
        {
            dissolving = true;
            if(trg != null)
            {
                target = trg;
            }
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Start()
        {
            Renderer rend = GetComponent<Renderer>();
            dissolveMat = rend.material;

            if(btcam == null)
            {
                btcam = BaseSceneManager.instance.sceneCam;
            }
        }

        //
        // --------------------------------------------------------------------
        //

        protected virtual void Update()
        {
            if(dissolveMat == null) return;

            float distance = 8f;
            if(target != null)
            {
                Vector3 diff = target.position - btcam.transform.position;
                distance = diff.magnitude - 1f;
            }

            if(dissolving)
            {
                dissolved += speed * Time.deltaTime;
                dissolved = btMath.Min(dissolved, 1f);
                if(dissolved >= 1f)
                {
                    dissolving = false;
                }
                dissolveMat.SetFloat("_Dissolve", dissolved);
                dissolveMat.SetFloat("_Distance", distance);
            }
            else if( dissolved > 0f)
            {
                dissolved -= decay * Time.deltaTime;
                dissolved = btMath.Max(dissolved, 0);
                dissolveMat.SetFloat("_Dissolve", dissolved);
                dissolveMat.SetFloat("_Distance", distance);
            }
        }
    }
}
