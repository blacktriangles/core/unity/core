﻿//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

Shader "blacktriangles/Environment (Transparent)" {

    //
    // properties /////////////////////////////////////////////////////////////
    //

	Properties {
        _MainTex ("Albedo", 2D) = "white" {}
        _Transparency ("Transparency", Range(0,1)) = 0.8
        _Emissive ("Emissive", 2D) = "black" {}
        _Dissolve ("Dissolve", Range(0,1)) = 0
        _DissolveScale ("Dissolve Scale", Float ) = 2
        _DissolveColor( "Dissolve Color", Color ) = (1,1,1,1)
        _DissolveEdgeThickness( "Dissolve Edge Thickness", Float ) = 1.0
        _DissolveEdgeIntensity( "Dissolve Edge Intensity", Float ) = 1
        _Distance("Distance", Float ) = 0
    }

    //
    // ########################################################################
    //

    CGINCLUDE

    //
    // includes ///////////////////////////////////////////////////////////////
    //

    #include "includes/NoiseCommon.cginc"
    #include "includes/Perlin.cginc"

    #include "UnityCG.cginc"

    //
    // variables //////////////////////////////////////////////////////////////
    //
    
    sampler2D _MainTex, _Emissive, _DissolveTex, _CameraDepthTexture;
    uniform float _Transparency, _Dissolve, _DissolveScale, _DissolveEdgeIntensity, _DissolveEdgeThickness, _Distance;
    uniform float4 _DissolveColor;

    //
    // types //////////////////////////////////////////////////////////////////
    //

    struct Input
    {
        float2 uv_MainTex;
        float3 worldPos;
        float2 depth;
    };

    //
    // utils //////////////////////////////////////////////////////////////////
    //
       
    ENDCG

    //
    // unity subshader ########################################################
    //
    

	SubShader {
        Tags 
        { 
            "RenderType" = "Transparent"
            "Queue" = "Transparent"
            "IgnoreProjector" = "True"
        }

        ZWrite Off
        Cull Off

		CGPROGRAM
        #pragma surface surf Standard fullforwardshadows addshadow vertex:vert alpha:fade
		#pragma target 3.0
        #pragma exclude_renderers flash

        void vert(inout appdata_full v)
        {
            UNITY_TRANSFER_DEPTH(v.depth);
        }

        //
        // --------------------------------------------------------------------
        //
        
        float mask(float3 worldPos)
        {
            float3 vpos = UnityWorldToViewPos(worldPos);
            float len = step(_Distance, length(vpos));

            float x = smoothstep(0.25,0.75,1-abs(0.5 - vpos.r)/4)-0.5;
            float y = smoothstep(0.25,0.75,1-abs(0.5 - vpos.g)/4)-0.5;
            return step(0.5,x+y);
        }

        //
        // --------------------------------------------------------------------
        //

        void surf(Input IN, inout SurfaceOutputStandard o)
        {
            float4 color = tex2D(_MainTex, IN.uv_MainTex);
            o.Albedo = color.rgb;
            o.Alpha = _Transparency;

            float3 vpos = UnityWorldToViewPos(IN.worldPos);
            float len = step(length(vpos), _Distance);

            float m = mask(IN.worldPos);
            float diss = _Dissolve * m * len;
            float p = diss * perlin(IN.worldPos*_DissolveScale) + diss * 1.7 + 0.3;
            clip(1-p);
            float e = max(0, p-_DissolveEdgeThickness)*10.0;

            float4 emission = tex2D(_Emissive, IN.uv_MainTex);
            float3 glow = emission.rgb * emission.a;
            o.Emission = glow + _DissolveColor * e * _DissolveEdgeIntensity;
        }
        
		ENDCG
	}
	FallBack "Diffuse"
}
