//UNITY_SHADER_NO_UPGRADE
#ifndef BT_COLORMAPLOOKUP_INCLUDED
#define BT_COLORMAPLOOKUP_INCLUDED

void ColorMap_float(
        float4 input,
        float4 color0, // black
        float4 color1, // red
        float4 color2, // green
        float4 color3, // blue
        float4 color4,  // white
        out float4 output
    )
{
    output = input;
    if(input.r <= 0 && input.g <= 0 && input.b <= 0 && input.a > 0) // black
    {
        output = color0;
    }
    else if(input.r > 0.99 && input.g <= 0 && input.b <= 0 && input.a > 0) // red
    {
        output = color1;
    }
    else if(input.r <= 0 && input.g > 0.99 && input.b <= 0 && input.a > 0) // green
    {
        output = color2;
    }
    else if(input.r <= 0 && input.g <= 0 && input.b > 0.99 && input.a > 0) // blue
    {
        output = color3;
    }
    if(input.r > 0.99 && input.g > 0.99 && input.b > 0.99 && input.a > 0) // black
    {
        output = color4;
    }
}

#endif //BT_COLORMAPLOOKUP_INCLUDED
