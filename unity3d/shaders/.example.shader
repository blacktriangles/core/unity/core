//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

Shader "blacktriangles/FlatColorMap"
{
    //
    // properties /////////////////////////////////////////////////////////////
    //
    
    Properties
    {
        _Atlas("Atlas", 2D) = "white" {}
        _Ramp("Ramp", 2D) = "black" {}

        _Color0("Color0", Color) = (0,0,0,1)
        _Color1("Color1", Color) = (1,0,0,1)
        _Color2("Color2", Color) = (0,1,0,1)
        _Color3("Color3", Color) = (0,0,1,1)
    }

    //
    // ########################################################################
    //
    
    CGINCLUDE
    
    //
    // includes ///////////////////////////////////////////////////////////////
    //

    //
    // variables //////////////////////////////////////////////////////////////
    //
    
    sampler2D _Atlas, _Ramp;
    uniform fixed4 _Color0, _Color1, _Color2, _Color3;

    //
    // types //////////////////////////////////////////////////////////////////
    //
    
    struct Input 
    {
        fixed2 uv_Atlas;
	};

    //
    // surface program ////////////////////////////////////////////////////////
    //

    void surf(Input IN, inout SurfaceOutputStandard o)
    {
        fixed4 sampled = tex2D(_Atlas, IN.uv_Atlas);
        fixed3 color = sampled.rgb;

        if(sampled.r <= 0 && sampled.g <= 0 && sampled.b <= 0)
        {
            color = _Color0.rgb;
        }
        else if(sampled.r >= 0 && sampled.g <= 0 && sampled.b <= 0)
        {
            color = _Color1.rgb;
        }
        else if(sampled.r <= 0 && sampled.g >= 0 && sampled.b <= 0)
        {
            color = _Color2.rgb;
        }
        else if(sampled.r <= 0 && sampled.g >= 0 && sampled.b <= 1)
        {
            color = _Color3.rgb;
        }

        o.Albedo = color;
    }

    ENDCG

    //
    // unity subshader ########################################################
    //
    
    SubShader
    {
        Tags 
        { 
            "RenderType" = "Opaque"
            "Queue" = "Geometry"
        }

        Cull Back
        ZWrite On

        CGPROGRAM
        #pragma surface surf Standard fullforwardshadows
        #pragma target 3.0
        #pragma exclude_renderers flash
        ENDCG
    }
    Fallback "Diffuse"
}
