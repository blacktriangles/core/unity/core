//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | hsmith@blacktriangles.com
//
//=============================================================================

using System.Collections;
using System.Collections.Generic;
using System.Net;

namespace blacktriangles.Network
{
    public class TcpServer
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public ConnectionConfig? config                         { get; private set; }
        public NetworkState state                               { get; private set; }
        public TcpListener conn                                 { get; private set; }
        public Beacon beacon                                    { get; private set; }
        public PacketRouter router                              { get; private set; }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void StartHost(int listenPort, byte sessionType = 0, bool useBeacon = false)
        {
            IPEndPoint endpoint = new IPEndPoint(IPAddress.Parse("0.0.0.0"), listenPort);
            StartHost(endpoint, sessionType, useBeacon);
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void StartHost(IPEndPoint localEndpoint, byte sessionType, bool useBeacon)
        {
            router = new PacketRouter();
            ConnectionConfig newConfig = new ConnectionConfig();
            newConfig.localEndpoint = newConfig.remoteEndpoint = localEndpoint;
            config = newConfig;
            InternalStartHost(sessionType, useBeacon);
        }

        //
        // --------------------------------------------------------------------
        //

        public void Update()
        {
            switch(state)
            {
                case NetworkState.Connected:        Connected();    break;
                case NetworkState.Disconnected:     /*noop*/        break;
                case NetworkState.Disconnecting:    Disconnect();   break;
                case NetworkState.Connecting:       /*noop*/        break;
            }
        }


        //
        // --------------------------------------------------------------------
        //

        public void Stop()
        {
            state = NetworkState.Disconnecting;
        }

        //
        // messages ///////////////////////////////////////////////////////////
        //
        
        public void Broadcast(Packet packet, bool simulateLocally = true, System.Predicate<TcpConnection> pred = null)
        {
            conn.Broadcast(packet, pred);

            if(simulateLocally)
            {
                conn.SimulateLocally(packet);
            }
        }

        //
        // private ////////////////////////////////////////////////////////////
        //

        private void InternalStartHost(byte sessionType, bool useBeacon)
        {
            if(config != null)
            {
                conn = new TcpListener();
                conn.Listen(config.Value);
               
                if(useBeacon)
                {
                    DiscoveryData discoveryData = new DiscoveryData() {
                        sessionType = sessionType,
                        address = config.Value.localEndpoint.Address.ToString(),
                        port = config.Value.localEndpoint.Port,
                    };

                    beacon = new Beacon();
                    beacon.Start(discoveryData);
                }

                state = NetworkState.Connected;
            }
        }

        //
        // --------------------------------------------------------------------
        //

        private void Connected()
        {
            if(conn == null) throw new System.InvalidOperationException("Connected, but server is null");

            conn.Update();
            List<RawPacket> rawPackets = conn.TakePackets();
            router.Route(rawPackets);
        }

        //
        // --------------------------------------------------------------------
        //

        private void Disconnect()
        {
            conn.Shutdown();
            state = NetworkState.Disconnected;
        }

        //
        // --------------------------------------------------------------------
        //

        protected virtual void OnComplete()
        {
            config = null;
            state = NetworkState.Disconnected;
        }

        //
        // end class //////////////////////////////////////////////////////////
        //
    }
}
