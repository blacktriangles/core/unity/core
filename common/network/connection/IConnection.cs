//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

namespace blacktriangles.Network
{
    public interface IConnection
    {
        //
        // events #############################################################
        //

        event ConnectionCallbacks.Connect OnConnect;
        event ConnectionCallbacks.Disconnect OnDisconnect;
        event ConnectionCallbacks.Error OnNetworkError;

        //
        // accessors ##########################################################
        //

        Socket socket                                           { get; }
        bool isActive                                           { get; }
        bool isConnected                                        { get; }
        ConnectionConfig? config                                { get; }

        //
        // methods ############################################################
        //

        bool Connect(ConnectionConfig config);
        void Send(Packet packet);
        void Send(IEnumerable<Packet> packets);
        List<RawPacket> TakePackets();
        void Disconnect();
    }
}
 
