//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace blacktriangles.Network
{
    public class UdpConnection
        : BaseConnection
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public UdpClient udpClient                              { get { return client; } }
        public override Socket socket                           { get { return client == null ? null : client.Client; } }
        public override bool isConnected                        { get { return isActive && socket.Connected; } }

        private UdpClient client                                = new UdpClient();

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override bool Connect(ConnectionConfig _config)
        {
            if(isConnected)
            {
                Dbg.Warn("Attempting to Connect a TcpConnection that is already connected");
                return false;
            }

            config = _config;
            socket.Bind(config.Value.localEndpoint);
            if(config.Value.remoteEndpoint != null)
            {
                client.Connect(config.Value.remoteEndpoint);
            }
            client.BeginReceive(OnReceive, null);
            StartThread();

            isActive = true;
            return isActive;
        }

        //
        // --------------------------------------------------------------------
        //

        public void Send(Packet packet, IPEndPoint destination)
        {
            if(config.Value.remoteEndpoint != null)
            {
                throw new System.ArgumentException("Cannot target send when you are bound to a specific destinatino");
            }

            RawPacket raw = packet.Encode();
            byte[] bytes = raw.ToBytes();
            client.Send(bytes, bytes.Length, destination);
        }

        //
        // --------------------------------------------------------------------
        //

        public override void Disconnect()
        {
            base.Disconnect();
            if(client != null)
            {
                client.Close();
                client = null;
            }
        }

        //
        // protected methods //////////////////////////////////////////////////
        //

        protected override void ProcessPackets()
        {
            try
            {
                Queue<Packet> packets = TakeOutgoingPackets();
                if(packets.Count > 0)
                {
                    foreach(Packet packet in packets)
                    {
                        RawPacket rawPacket = packet.Encode();
                        byte[] bytes = rawPacket.ToBytes();
                        client.Send(bytes, bytes.Length);
                    }
                }
            }
            catch(Exception e)
            {
                NotifyError(e.ToString());
            }
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private void OnReceive(IAsyncResult asyncResult)
        {
            if(!isActive) return;
            IPEndPoint endpoint = null;
            byte[] bytes = client.EndReceive(asyncResult, ref endpoint);
            BinaryReader reader = new BinaryReader(new MemoryStream(bytes));

            RawPacket packet = new RawPacket(reader);
            packet.receiver = this;
            packet.senderEndpoint = endpoint;
            AddIncomingPacket(packet);
            client.BeginReceive(OnReceive, null);
        }

        //
        // End Class //////////////////////////////////////////////////////////
        //
    }
}
