//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;

namespace blacktriangles.Network
{
    public class TcpConnection
        : BaseConnection
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public override Socket socket                           { get { return client == null ? null : client.Client; } }
        public override bool isConnected                        { get { return isActive && socket.Connected; } }

        private System.Net.Sockets.TcpClient client             = null;
        private RawPacketBuilder builder                        = new RawPacketBuilder();
        private Queue<Packet> processingPackets                 = null;

        private System.IO.Stream stream                         = null;
        private BinaryReader reader                             = null;
        private BinaryWriter writer                             = null;

        //
        // constructor / finalizer ////////////////////////////////////////////
        //

        public TcpConnection()
        {
        }

        //
        // --------------------------------------------------------------------
        //

        public TcpConnection(System.Net.Sockets.TcpClient _client, ConnectionConfig _config)
        {
            Connect(_client, _config);
        }

        //
        // --------------------------------------------------------------------
        //

        ~TcpConnection()
        {
            Dispose();
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void ManualUpdate()
        {
            ProcessPackets();
        }

        //
        // --------------------------------------------------------------------
        //

        public override bool Connect(ConnectionConfig _config)
        {
            if(socket != null)
            {
                Dbg.Warn("Attempting to Connect a TcpConnection that is already connected");
                return false;
            }

            System.Net.Sockets.TcpClient _client = new System.Net.Sockets.TcpClient();
            _client.Connect(_config.remoteEndpoint);
            return Connect(_client, _config);
        }

        //
        // --------------------------------------------------------------------
        //

        public bool Connect(System.Net.Sockets.TcpClient _client, ConnectionConfig _config)
        {
            client = _client;
            config = _config;
            
            if(!client.Client.Connected)
            {
                throw new System.ArgumentException("Trying to create a TcpConnection with a TcpClient that is not connected to anything");
            }
            
            if(config.Value.useTls)
            {
                SslStream sslstream = new SslStream(
                    client.GetStream(),
                    true,
                    new RemoteCertificateValidationCallback(ValidateServerCertificate)
                );

                try
                {
                    sslstream.AuthenticateAsClient(config.Value.nameOnCertificate);
                    stream = sslstream;
                }
                catch(Exception ex)
                {
                   Dbg.Error("Exception attempting to authenticate as client: {0}", ex.ToString());
                }
            }
            else
            {
                stream = client.GetStream();
            }

            if(stream != null)
            {
                writer = new BinaryWriter(stream);
                reader = new BinaryReader(stream);

                isActive = true;
            }

            StartThread();
            return isActive;
        }

        //
        // --------------------------------------------------------------------
        //

        public override void Disconnect()
        {
            base.Disconnect();
            if(client != null)
            {
                client.Close();
                client = null;
            }

            if(stream != null)
            {
                stream.Close();
                stream = null;
            }

            if(writer != null)
            {
                writer.Close();
                writer = null;
            }

            if(reader != null)
            {
                reader.Close();
                reader = null;
            }
        }

        //
        // protected methods //////////////////////////////////////////////////
        //

        protected override void ProcessPackets()
        {
            try
            {
                if(client.Client.Poll(1, SelectMode.SelectError))
                {
                    NotifyError("Connection error encountered.");
                }

                ReadIncomingData();
                WriteOutgoingData();
            }
            catch(Exception e)
            {
                NotifyError(e.ToString());
            }
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private void ReadIncomingData()
        {
            bool canRead = isConnected && client.Available > 0;
            if(canRead)
            {
                try
                {
                    builder.AddBytes(reader);
                    RawPacket rawPacket = null;
                    bool hasMore = true;
                    while(hasMore)
                    {
                        rawPacket = builder.GetRawPacket();
                        if(rawPacket != null)
                        {
                            rawPacket.receiver = this;
                            rawPacket.senderEndpoint = config.Value.remoteEndpoint;
                            AddIncomingPacket(rawPacket);
                        }
                        else
                        {
                            hasMore = false;
                        }
                    } 
                }
                catch( System.Exception ex )
                {
                    NotifyError(ex.ToString());
                }
            }
        }

        //
        // --------------------------------------------------------------------
        //

        private void WriteOutgoingData()
        {
            if(isConnected)
            {
                processingPackets = TakeOutgoingPackets();
                foreach(Packet sendPacket in processingPackets)
                {
                    RawPacket rawPacket = sendPacket.Encode();
                    rawPacket.WriteToStream(writer);
                }
            }
        }

        //
        // ------------------------------------------------------------------------
        //
        
        private bool ValidateServerCertificate(object sender, X509Certificate cert, X509Chain chain, SslPolicyErrors policyErrs)
        {
            if (policyErrs == SslPolicyErrors.None)
            {
                return true;
            }

            Dbg.Error("Certificate error: {0}", policyErrs);
            
            return false;
        }

        //
        // End Class //////////////////////////////////////////////////////////
        //

    }
}
