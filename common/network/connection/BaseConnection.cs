//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace blacktriangles.Network
{
    public abstract class BaseConnection
        : IConnection
        , IDisposable
    {
        //
        // events /////////////////////////////////////////////////////////////
        //

        public event ConnectionCallbacks.Connect OnConnect;
        public event ConnectionCallbacks.Disconnect OnDisconnect;
        public event ConnectionCallbacks.Error OnNetworkError;
        public event ConnectionCallbacks.Message OnMessageReceived;

        //
        // members ////////////////////////////////////////////////////////////
        //

        public abstract Socket socket                           { get; }
        public bool isActive                                    { get; protected set; }
        public abstract bool isConnected                        { get; }
        public ConnectionConfig? config                         { get; protected set; }

        public int incomingCount                                { get { lock(incomingPackets) return incomingPackets.Count; } }

        private List<RawPacket> incomingPackets                 = new List<RawPacket>();
        private List<Packet> outgoingPackets                    = new List<Packet>();

        private Thread procThread                               = null;

        //
        // constructor / finalizer ////////////////////////////////////////////
        //

        ~BaseConnection()
        {
            Dispose();
        }

        //
        // --------------------------------------------------------------------
        //

        public virtual void Dispose()
        {
            Disconnect();
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public abstract bool Connect(ConnectionConfig _config);

        //
        // --------------------------------------------------------------------
        //
        
        public void SimulateIncomingPacket(Packet packet)
        {
            RawPacket rawPacket = packet.Encode();
            lock(incomingPackets)
            {
                incomingPackets.Add(rawPacket);
            }
            NotifyMessage(packet);
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void Send(Packet packet)
        {
            if(packet == null) return;

            lock(outgoingPackets)
            {
                outgoingPackets.Add(packet);
            }
        }

        //
        // --------------------------------------------------------------------
        //


        public void Send(IEnumerable<Packet> packets)
        {
            if(packets == null) return;
            lock(outgoingPackets)
            {
                foreach(Packet packet in packets)
                {
                    Send(packet);
                }
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public List<RawPacket> TakePackets()
        {
            List<RawPacket> result = null;
            lock(incomingPackets)
            {
                result = new List<RawPacket>(incomingPackets);
                incomingPackets.Clear();
            }

            return result;
        }

        //
        // --------------------------------------------------------------------
        //

        public virtual void Disconnect()
        {
            isActive = false;
            if(procThread != null)
            {
                if(!procThread.Join(2000))
                {
                    procThread.Abort();    
                }
                
                procThread = null;
            }
            NotifyDisconnected();
        }

        //
        // protected methods //////////////////////////////////////////////////
        //

        protected abstract void ProcessPackets();

        //
        // --------------------------------------------------------------------
        //

        protected void StartThread()
        {
            if(procThread == null)
            {
                procThread = new Thread(ThreadProc);
                procThread.IsBackground = true;
                procThread.Start();
            }

        }

        //
        // --------------------------------------------------------------------
        //
        
        protected void ThreadProc()
        {
            while(isActive)
            {
                ProcessPackets();
                Thread.Yield();
            }
        }

        //
        // --------------------------------------------------------------------
        //

        protected void AddIncomingPacket(RawPacket packet)
        {
            Packet incoming = PacketFactory.Create(packet.header.id);
            if(incoming == null)
            {
                throw new System.InvalidOperationException("Could not create a packet for id " + packet.header.id.ToString());
            }

            incoming.Decode(packet);
            NotifyMessage(incoming);

            lock(incomingPackets)
            {
                incomingPackets.Add(packet);
            }
        }

        //
        // --------------------------------------------------------------------
        //

        protected Queue<Packet> TakeOutgoingPackets()
        {
            Queue<Packet> result = null;
            lock(outgoingPackets)
            {
                result = new Queue<Packet>(outgoingPackets);
                outgoingPackets.Clear();
            }

            return result;
        }

        //
        // notify callbacks ///////////////////////////////////////////////////
        //

        protected void NotifyConnected()
        {
            ConnectionCallbacks.Connect cb = OnConnect;
            if(cb != null)
            {
                cb(this);
            }
        }

        //
        // --------------------------------------------------------------------
        //

        protected void NotifyDisconnected()
        {
            ConnectionCallbacks.Disconnect cb = OnDisconnect;
            if(cb != null)
            {
                cb(this);
            }
        }

        //
        // --------------------------------------------------------------------
        //

        protected void NotifyError(string msg)
        {
            ConnectionCallbacks.Error cb = OnNetworkError;
            if(cb != null)
            {
                NetworkError err = new NetworkError(this, msg);
                cb(err);
            }
        }

        //
        // --------------------------------------------------------------------
        //

        private void NotifyMessage(Packet packet)
        {
            ConnectionCallbacks.Message cb = OnMessageReceived;
            if(cb != null)
            {
                cb(this, packet);
            }
        }

        //
        // End Class //////////////////////////////////////////////////////////
        //

    }
}
