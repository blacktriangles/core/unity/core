//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | hsmith@blacktriangles.com
//
//=============================================================================

namespace blacktriangles.Network
{
    public enum NetworkState
    {
        Disconnected,
        Connecting,
        Connected,
        Disconnecting
    }
}

