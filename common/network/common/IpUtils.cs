//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace blacktriangles.Network
{
    public static class IpUtils
    {
        //
        // public methods /////////////////////////////////////////////////////
        //

        public static bool IsNetworkAvailable()
        {
            return System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable();
        }

        //
        // --------------------------------------------------------------------
        //

        public static string GetHostName()
        {
            return Dns.GetHostName();
        }

        //
        // --------------------------------------------------------------------
        //

        public static IPAddress GetLocalIp()
        {
            IPAddress result;
            using(Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, 0))
            {
                socket.Connect("8.8.8.8", 65530);
                IPEndPoint endpoint = socket.LocalEndPoint as IPEndPoint;
                result = endpoint.Address;
            }

            return result;
        }

        //
        // --------------------------------------------------------------------
        //

        public static IPAddress GetRemoteIp()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://api.ipify.org/");
            using(HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using(Stream stream = response.GetResponseStream())
            using(StreamReader reader = new StreamReader(stream))
            {
                return IPAddress.Parse(reader.ReadToEnd());
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        public static IPEndPoint Resolve(string name, int port)
        {
            IPHostEntry host = Dns.GetHostEntry(name);
            if(host.AddressList.Length == 0) return null;

            IPAddress ip = host.AddressList[0];
            return new IPEndPoint(ip, port);
        }

        //
        // end class //////////////////////////////////////////////////////////
        //
    }
}
