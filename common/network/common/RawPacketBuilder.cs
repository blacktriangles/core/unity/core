//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace blacktriangles.Network
{
    public class RawPacketBuilder
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        private bool hasHeader                                  = false;
        private Packet.Header header                            = default(Packet.Header);
        private List<byte> buffer                               = new List<byte>();

        //
        // public methods /////////////////////////////////////////////////////
        //
        
        public void AddBytes(BinaryReader reader)
        {
            byte[] buffer = new byte[1024];
            int bytesRead = 0;
            bytesRead = reader.Read(buffer, 0, buffer.Length);
            if(bytesRead > 0)
            {
                AddBytes(buffer, 0, bytesRead);
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public void AddBytes(byte[] bytes, int offset = 0, int length = -1)
        {
            if(bytes != null)
            {
                if(length < 0) length = bytes.Length - offset;
                ArraySegment<byte> segment = new ArraySegment<byte>(bytes, offset, length);
                buffer.AddRange(segment);
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public RawPacket GetRawPacket()
        {
            RawPacket result = null;
            if(!hasHeader && buffer.Count >= Packet.kHeaderSize)
            {
                BinaryReader reader = new BinaryReader(new MemoryStream(buffer.ToArray()));
                header = new Packet.Header(reader);
                buffer.RemoveRange(0, Packet.kHeaderSize);
                hasHeader = true;
            }

            if(hasHeader && buffer.Count >= header.payloadSize)
            {
                BinaryReader reader = new BinaryReader(new MemoryStream(buffer.ToArray()));
                byte[] payload = new byte[header.payloadSize];
                reader.Read(payload, 0, header.payloadSize);
                buffer.RemoveRange(0, header.payloadSize);
                hasHeader = false;

                result = new RawPacket(header.id, payload);
            }

            return result;
        }
    }
}
