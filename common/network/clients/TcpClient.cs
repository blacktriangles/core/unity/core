//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | hsmith@blacktriangles.com
//
//=============================================================================

using System.Collections;
using System.Collections.Generic;
using System.Net;

namespace blacktriangles.Network
{
    public class TcpClient
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public TcpConnection conn                               { get; private set; }
        public Probe probe                                      { get; private set; }
        public PacketRouter router                              { get; private set; }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void StartProbe(byte sessionType)
        {
            Dbg.Assert(probe == null, "Attempting to start probe when one is already started");
            probe = new Probe();
            probe.Start(sessionType);
        }

        //
        // --------------------------------------------------------------------
        //

        public void Update()
        {
            if(probe != null)   probe.Update();
            if(conn != null)
            {
                List<RawPacket> packets = conn.TakePackets();
                router.Route(packets);
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public void Connect(Probe.Discovered host)
        {
            ConnectionConfig config = new ConnectionConfig();
            config.remoteEndpoint = new IPEndPoint(IPAddress.Parse(host.res.data.address), host.res.data.port);
            Connect(config);
        }

        //
        // --------------------------------------------------------------------
        //

        public void ConnectToLocalHost(int port)
        {
            ConnectionConfig config = new ConnectionConfig();
            config.remoteEndpoint = new IPEndPoint(IpUtils.GetLocalIp(), port);
            Connect(config);
        }

        //
        // --------------------------------------------------------------------
        //

        public void Connect(ConnectionConfig config)
        {
            router = new PacketRouter();
            conn = new TcpConnection();
            conn.Connect(config);
        }

        //
        // --------------------------------------------------------------------
        //

        public void Stop()
        {
            if(probe != null)
            {
                probe.Stop();
                probe = null;
            }

            if(conn != null && conn.isConnected)
            {
                conn.Disconnect();
                conn = null;
            }
        }

        //
        // end class //////////////////////////////////////////////////////////
        //
    }
}
