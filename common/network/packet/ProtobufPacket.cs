//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using blacktriangles.Network;
using ProtoBuf;
using System.IO;
using System;

namespace blacktriangles.Network
{
    public abstract class BaseProtobufPacket
        : Packet
    {
        //
        // packet //////////////////////////////////////////////////////////////
        //
        public virtual void Decode<T>( RawPacket packet )
            where T : BaseProtobufPacket
        {
            bool streamWritable = false;
            MemoryStream stream = new MemoryStream( packet.payload, streamWritable );
            T self = this as T;
            Serializer.Merge<T>(stream, self);
        }

        //
        // protected methods //////////////////////////////////////////////////
        //

        protected virtual void Reset()
        {
        }

        //
        // ------------------------------------------------------------------------
        //

        protected override byte[] EncodePayload()
        {
            MemoryStream stream = new MemoryStream();
            Serializer.Serialize( stream, this );
            return stream.ToArray();
        }
    }

    //
    // ------------------------------------------------------------------------
    //

    public abstract class ProtobufPacket<T>
        : BaseProtobufPacket
        where T: BaseProtobufPacket
    {
        public override void Decode(RawPacket packet)
        {
            Reset();
            base.Decode(packet);
            Decode<T>(packet);
        }
    }
}
