//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace blacktriangles.Network
{
    public static class PacketFactory
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        private static Dictionary<int, ConcurrentBag<object>> bags = new Dictionary<int, ConcurrentBag<object>>();
        
        //
        // public methods /////////////////////////////////////////////////////
        //

        public static Packet Create(int id)
        {
            ConcurrentBag<object> bag = null;
            if(bags.TryGetValue(id, out bag) == false)
            {
                bag = new ConcurrentBag<object>();
                bags[id] = bag;
            }

            object result = null;
            if(bag.TryTake(out result))
            {
                return result as Packet;
            }

            return AttributeFactory<PacketAttribute>.Produce<Packet>( (entry)=>{
                if(entry.attribute.id == id)
                {
                    return System.Activator.CreateInstance(entry.type) as Packet;
                }

                return null;
            });
        }

        //
        // --------------------------------------------------------------------
        //

        public static Packet Create(RawPacket packet)
        {
            Packet result = Create(packet.header.id);
            if(result != null)
            {
                result.Decode(packet);
            }
            return result;
        }

        //
        // --------------------------------------------------------------------
        //

        public static T Create<T>(RawPacket packet)
            where T: Packet
        {
            return Create(packet) as T;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public static void Destroy(Packet packet)
        {
            bags[packet.id].Add(packet);
        }

        //
        // private methods ////////////////////////////////////////////////////
        //
    }
}
 
