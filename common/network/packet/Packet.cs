
//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;

namespace blacktriangles.Network
{
    public abstract class Packet
    {
        //
        // types ##############################################################
        //

        [StructLayout(LayoutKind.Sequential, Pack=1)]
        public struct Header
        {
            //
            // members --------------------------------------------------------
            //

            public int id;
            public double ts;
            public int payloadSize;

            //
            // constructor / initializer --------------------------------------
            //

            public Header(int _id, int _payloadSize)
            {
                id = _id;
                ts = Epoch.now;
                payloadSize = _payloadSize;
            }

            //
            // ----------------------------------------------------------------
            //

            public Header(BinaryReader reader)
            {
                id = reader.ReadInt32();
                ts = reader.ReadDouble();
                payloadSize = reader.ReadInt32();
            }

            //
            // public methods -------------------------------------------------
            //
            
            public override string ToString()
            {
                return System.String.Format( "[ id:{0}, ts: {1}, payloadSize:{2}]", id, ts, payloadSize );
            }

            //
            // ----------------------------------------------------------------
            //

            public byte[] ToBytes()
            {
                MemoryStream stream = new MemoryStream();
                BinaryWriter writer = new BinaryWriter(stream);
                writer.Write(id);
                writer.Write(ts);
                writer.Write(payloadSize);
                return stream.ToArray();
            }
        }

        //
        // constants ==========================================================
        //

        public static readonly int kHeaderSize                  = System.Runtime.InteropServices.Marshal.SizeOf<Header>();

        //
        // members ============================================================
        //

        public int id                                           { get { return packetAttrib.id; } }
        public double timestamp                                 { get; private set; }
        public IConnection receiver                             = null;
        public IPEndPoint senderEndpoint                        = null;
        public PacketAttribute packetAttrib                     { get { return GetPacketAttribute(); } }
        private PacketAttribute _packetAttrib                   = null;

        //
        // public methods =====================================================
        //

        public static PacketAttribute GetPacketAttribute(System.Type type)
        {
            return AssemblyUtility.GetAttribute<PacketAttribute>(type);
        }
        //
        // --------------------------------------------------------------------
        //

        public virtual void Decode( RawPacket data )
        {
            timestamp = data.header.ts;
            receiver = data.receiver;
            senderEndpoint = data.senderEndpoint;
        }

        //
        // --------------------------------------------------------------------
        //

        public RawPacket Encode()
        {
            RawPacket packet = new RawPacket(id, EncodePayload());
            packet.receiver = receiver;
            packet.senderEndpoint = senderEndpoint;
            return packet;
        }

        //
        // --------------------------------------------------------------------
        //

        public override string ToString()
        {
            return GetType().Name;
        }

        //
        // protected methods //////////////////////////////////////////////////
        //

        protected abstract byte[] EncodePayload();

        //
        // private methods ====================================================
        //

        private PacketAttribute GetPacketAttribute()
        {
            if( _packetAttrib == null )
            {
                _packetAttrib = AssemblyUtility.GetAttribute<PacketAttribute>( GetType() );
            }

            return _packetAttrib;
        }

        //
        // --------------------------------------------------------------------
        //
    }
}
