//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace blacktriangles.Network
{
    public class Beacon
    {
        //
        // constants //////////////////////////////////////////////////////////
        //

        public const int kDiscoveryPort                         = 13287;

        //
        // types //////////////////////////////////////////////////////////////
        //

        public DiscoveryData discoveryData                      { get; private set; }
        public bool isActive                                    { get; private set; }
        private UdpConnection conn                              = null;

        //
        // constructor / finalizer ////////////////////////////////////////////
        //

        public Beacon()
        {
            isActive = false;
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void Start(byte sessionType, int listenPort, int discoveryPort = kDiscoveryPort)
        {
            DiscoveryData data = new DiscoveryData();
            data.sessionType = sessionType;
            data.port = listenPort;
            data.address = IpUtils.GetLocalIp().ToString();
            Start(data, discoveryPort);

        }

        //
        // --------------------------------------------------------------------
        //

        public void Start(DiscoveryData data, int discoveryPort = kDiscoveryPort)
        {
            isActive = true;
            discoveryData = data;
            conn = new UdpConnection();
            conn.OnMessageReceived += OnMessageReceived;
            conn.socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            ConnectionConfig config = new ConnectionConfig();
            config.localEndpoint = new IPEndPoint(IPAddress.Any, discoveryPort);
            config.remoteEndpoint = null;
            conn.Connect(config);
        }

        //
        // --------------------------------------------------------------------
        //

        public void Stop()
        {
            isActive = false;
            if(conn != null)
            {
                conn.Disconnect();
                conn = null;
            }
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private void OnMessageReceived(IConnection receiver, Packet packet)
        {
            if(conn == null || !isActive) return;

            DiscoveryRequest req = packet as DiscoveryRequest;
            if(req != null && req.sessionType == discoveryData.sessionType)
            {
                DiscoveryResponse res = new DiscoveryResponse(discoveryData);
                res.title = IpUtils.GetHostName();
                conn.Send(res, packet.senderEndpoint);
            }
        }
    }
}
 
