//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using ProtoBuf;
using UnityEngine;

namespace blacktriangles.Network
{
    //
    // Discovery Data /////////////////////////////////////////////////////////
    //
    [ProtoContract]
    public struct DiscoveryData
    {
        [ProtoMember(1)] public byte sessionType;
        [ProtoMember(2)] public string address;
        [ProtoMember(3)] public int port;
    }

    //
    // Request ////////////////////////////////////////////////////////////////
    //

    [ProtoContract]
    [Packet((int)ReservedPacketType.DiscoveryRequest)]
    public class DiscoveryRequest
        : ProtobufPacket<DiscoveryRequest>
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        [ProtoMember(1)] public byte sessionType                = 0;
    }

    //
    // Response ///////////////////////////////////////////////////////////////
    //

    [ProtoContract]
    [Packet((int)ReservedPacketType.DiscoveryResponse)]
    public class DiscoveryResponse
        : ProtobufPacket<DiscoveryResponse>
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        [ProtoMember(1)] public DiscoveryData data;
        [ProtoMember(2)] public string title;
        [ProtoMember(3)] public string description;

        //
        // constructor / finalizer ////////////////////////////////////////////
        //

        public DiscoveryResponse()
        {
        }

        //
        // --------------------------------------------------------------------
        //

        public DiscoveryResponse(DiscoveryData _data)
        {
            data = _data;
        }

        //
        // --------------------------------------------------------------------
        //

        public bool Compare(DiscoveryResponse other)
        {
            if(other == null) return false;
            return data.sessionType == other.data.sessionType &&
                   data.address == other.data.address &&
                   data.port == other.data.port;
        }
    }
}
