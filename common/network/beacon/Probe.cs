//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace blacktriangles.Network
{
    public class Probe
        : IDisposable
    {
        //
        // events /////////////////////////////////////////////////////////////
        //

        public delegate void ProbeUpdate(Probe.Discovered[] discovered);
        public event ProbeUpdate OnProbeUpdate;

        //
        // types //////////////////////////////////////////////////////////////
        //

        public class Discovered
        {
            public DiscoveryResponse res;
            public int staleCount;
        }

        //
        // constants //////////////////////////////////////////////////////////
        //

        private const double kSleepTime                         = 1.0;
        private const int kMaxStaleCount                        = 5;

        //
        // members ////////////////////////////////////////////////////////////
        //

        public byte sessionType                                 { get; private set; }
        public bool isActive                                    { get; private set; }
        public Discovered[] discovered                          { get { lock(_discovered) { return _discovered.ToArray(); } } }

        private UdpConnection conn                              = null;
        private List<Discovered> _discovered                    = new List<Discovered>();
        private double lastProbeSent                            = 0;

        private IPEndPoint targetEndpoint      = new IPEndPoint(IPAddress.Broadcast, Beacon.kDiscoveryPort);

        //
        // constructor / finalizer ////////////////////////////////////////////
        //

        public Probe()
        {
            isActive = false;
        }

        ~Probe()
        {
            Dispose();
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void Start(byte _sessionType, int discoveryPort = Beacon.kDiscoveryPort)
        {
            targetEndpoint = new IPEndPoint(IPAddress.Broadcast, discoveryPort);

            isActive = true;
            sessionType = _sessionType;
            conn = new UdpConnection();
            conn.OnNetworkError += OnNetworkError;
            conn.OnMessageReceived += OnMessageReceived;

            conn.socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            ConnectionConfig config = new ConnectionConfig();
            config.localEndpoint = new IPEndPoint(IPAddress.Any, 0);
            config.remoteEndpoint = null;
            conn.Connect(config);
        }

        //
        // --------------------------------------------------------------------
        //

        public void Update()
        {
            if(conn == null || !isActive) return;

            double elapsed = Epoch.now - lastProbeSent;
            if(elapsed > kSleepTime)
            {
                lastProbeSent = Epoch.now;
                DiscoveryRequest req = new DiscoveryRequest();
                req.sessionType = sessionType;
                conn.Send(req, targetEndpoint);

                // prune stale items
                lock(_discovered)
                {
                    _discovered.RemoveAll((disc)=>{
                        return ++disc.staleCount > kMaxStaleCount;
                    });
                }

                if(OnProbeUpdate != null)
                {
                    OnProbeUpdate(discovered);
                }
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public void Stop()
        {
            isActive = false;
            conn.Disconnect();
            conn = null;
        }

        //
        // callbacks //////////////////////////////////////////////////////////
        //

        private void OnNetworkError(NetworkError err)
        {
            Dbg.Error(err.ToString());
        }

        private void OnMessageReceived(IConnection recieved, Packet packet)
        {
            if(conn == null || !isActive) return;

            DiscoveryResponse res = packet as DiscoveryResponse;
            if(res != null)
            {
                lock(_discovered)
                {
                    Discovered disc = _discovered.Find((check)=>check.res.Compare(res));
                    if(disc == null)
                    {
                        disc = new Discovered();
                        disc.res = res;
                        _discovered.Add(disc);
                    }

                    disc.staleCount = 0;
                }
            }
        }

        //
        // IDisposable ////////////////////////////////////////////////////////
        //
        
        public void Dispose()
        {
            Stop();
        }
                 
    }
}
 
