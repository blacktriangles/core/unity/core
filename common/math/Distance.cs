//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

namespace blacktriangles
{
    //
    // ////////////////////////////////////////////////////////////////////////
    //
    
    public enum DistanceUnit
    {
        Meter,
        Kilometer,
        Megameter,
        AstronomicalUnit,
        Lightyear,
        Parsec
    }

    //
    // ////////////////////////////////////////////////////////////////////////
    //

    public class Distance
    {
        //
        // constants //////////////////////////////////////////////////////////
        //

        private static readonly double[] kFactors = new double[] {
            1000.0,         // m -> km
            1000.0,         // km -> Mm
            149598,         // Mm -> AU
            63241.1,        // AU -> LY
            3.26156,        // LY -> PC
        };
        
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public double value;
        public DistanceUnit unit;

        //
        // constructor ////////////////////////////////////////////////////////
        //

        public Distance()
        {
            value = 0.0;
            unit = DistanceUnit.Meter;
        }

        //
        // --------------------------------------------------------------------
        //

        public Distance(double value, DistanceUnit unit)
        {
            this.value = value;
            this.unit = unit;
        }

        //
        // public static methods //////////////////////////////////////////////
        //

        public Distance Convert(DistanceUnit newUnit)
        {
            int steps = (int)newUnit - (int)unit;
            int dir = btMath.Clamp(steps, -1, 1);
            steps = btMath.Abs(steps);

            Distance result = new Distance() {
                value = this.value,
                unit = this.unit
            };

            for(int i = 0; i < steps; ++i)
            {
                int idx = 0;
                double factor = 0.0;
                if(dir > 0)
                {
                    idx = (int)result.unit;
                    Dbg.Assert(kFactors.IsValidIndex(idx), "Index invalid");
                    factor = 1.0 / kFactors[idx];
                    idx = (int)result.unit + 1;
                }
                else
                {
                    idx = (int)result.unit - 1;
                    Dbg.Assert(kFactors.IsValidIndex(idx), "Index invalid");
                    factor = kFactors[idx];
                }

                result.value *= factor;
                result.unit = (DistanceUnit)idx;
            }

            return result;
        }

        //
        // operators //////////////////////////////////////////////////////////
        //
        
        public static Distance operator*(Distance lhs, double rhs)
        {
            return new Distance() {
                value = lhs.value * rhs,
                unit = lhs.unit
            };
        }

        //
        // --------------------------------------------------------------------
        //
        
        public override string ToString()
        {
            return $"Distance {value} {unit}";
        }

        //
        // private methods ////////////////////////////////////////////////////
        //
    }
}
