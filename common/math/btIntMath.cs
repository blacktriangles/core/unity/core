//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

namespace blacktriangles
{
    public static partial class btMath
    {
        public static int Min(int a, int b)
        {
            return System.Math.Min(a, b);
        }

        //
        // --------------------------------------------------------------------
        //

        public static int Max(int a, int b)
        {
            return System.Math.Max(a, b);
        }

        //
        // --------------------------------------------------------------------
        //
        
        public static int Clamp( int value, int min, int max )
        {
            return Max( Min( max, value ), min );
        }

        //
        // --------------------------------------------------------------------
        //
        
        public static int Wrap( int value, int min, int max )
        {
            int range = max - min;
            value = value % range;
            return min + value;
        }

        //
        // --------------------------------------------------------------------
        //

        public static int Abs( int value )
        {
            return System.Math.Abs(value);
        }
    }
}
