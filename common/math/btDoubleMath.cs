//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

namespace blacktriangles
{
    public static partial class btMath
    {
        public static double Max(double lhs, double rhs)
        {
            return System.Math.Max(lhs, rhs);
        }
    }
}
