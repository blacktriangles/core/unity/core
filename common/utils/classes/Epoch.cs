//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System;

namespace blacktriangles
{
    public static class Epoch
    {
        // constants //////////////////////////////////////////////////////////
        public static readonly DateTime kEpochStart             = new DateTime(1970,1,1);
        public static double now                                { get { return DateTime.UtcNow.Subtract(kEpochStart).TotalSeconds; } }
    }
}
