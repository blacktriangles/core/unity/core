//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

namespace blacktriangles
{
    public static class Delay
    {
        public static void Callback(System.Action cb, int ms)
        {
            System.Threading.Timer timer = null;
            timer = new System.Threading.Timer((obj)=>{
                cb();
                timer.Dispose();
            }, null, ms, System.Threading.Timeout.Infinite);
        }
    }
}
