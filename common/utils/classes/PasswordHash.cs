//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
// stolen shamelessly from Roger Knapp's awesome blog post
//  http://csharptest.net/470/another-example-of-how-to-store-a-salted-password-hash/
//
//=============================================================================

using System;
using System.Security.Cryptography;

namespace blacktriangles
{
    public sealed class PasswordHash
    {
        // constants ///////////////////////////////////////////////////////////
        const int SaltSize = 16, HashSize = 20, HashIter = 10000;

        // members /////////////////////////////////////////////////////////////
        readonly byte[] _salt, _hash;
        public byte[] Salt { get { return (byte[])_salt.Clone(); } }
        public byte[] Hash { get { return (byte[])_hash.Clone(); } }

        // constructor / destructor ////////////////////////////////////////////
        public PasswordHash(string password)
        {
            new RNGCryptoServiceProvider().GetBytes(_salt = new byte[SaltSize]);
            _hash = new Rfc2898DeriveBytes(password, _salt, HashIter).GetBytes(HashSize);
        }
        public PasswordHash(byte[] hashBytes)
        {
            Array.Copy(hashBytes, 0, _salt = new byte[SaltSize], 0, SaltSize);
            Array.Copy(hashBytes, SaltSize, _hash = new byte[HashSize], 0, HashSize);
        }

        public PasswordHash(byte[] salt, byte[] hash)
        {
            Array.Copy(salt, 0, _salt = new byte[SaltSize], 0, SaltSize);
            Array.Copy(hash, 0, _hash = new byte[HashSize], 0, HashSize);
        }

        // operators ///////////////////////////////////////////////////////////
        public byte[] ToArray()
        {
            byte[] hashBytes = new byte[SaltSize + HashSize];
            Array.Copy(_salt, 0, hashBytes, 0, SaltSize);
            Array.Copy(_hash, 0, hashBytes, SaltSize, HashSize);
            return hashBytes;
        }

        public bool Verify(string password)
        {
            byte[] test = new Rfc2898DeriveBytes(password, _salt, HashIter).GetBytes(HashSize);
            for (int i = 0; i < HashSize; i++)
            {
                if (test[i] != _hash[i])
                {
                    return false;
                }
            }

            return true;
        }
    }
}
