//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using System.Collections.Generic;
using System.Reflection;

namespace blacktriangles
{
    public static class AttributeFactory<AttributeType>
        where AttributeType: System.Attribute
    {
        //
        // types //////////////////////////////////////////////////////////////
        //
        public struct Entry
        {
            public System.Type type;
            public AttributeType attribute;
        }

        //
        // members ////////////////////////////////////////////////////////////
        //

        private static Entry[] entries                          = null;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public static T Produce<T>(System.Func<Entry, T> tryProduce)
            where T: class
        {
            bool force = false;
            
            Initialize(AssemblyUtility.GetAllAssemblies(), force);
            foreach(Entry entry in entries)
            {
                T result = tryProduce(entry);
                if(result != null) return result;
            }

            return null;
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private static void Initialize(Assembly[] asms, bool force = false)
        {
            if(entries != null && !force) return;

            List<Entry> result = new List<Entry>();
            foreach(Assembly asm in asms)
            {
                System.Type[] types = AssemblyUtility.CollectTypesWithAttribute<AttributeType>(asm);
                foreach(System.Type type in types)
                {
                    Entry entry = new Entry();
                    entry.type = type;
                    entry.attribute = AssemblyUtility.GetAttribute<AttributeType>(type);
                    result.Add(entry);
                }
            }

            entries = result.ToArray();
        }
    }
}
