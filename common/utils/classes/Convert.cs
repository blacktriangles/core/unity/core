//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System.Reflection;
using System.Collections.Generic;

namespace blacktriangles
{
    public static class Convert
    {
        public static List<OutputType> All<InputType,OutputType>( IEnumerable<InputType> items, System.Func<InputType,OutputType> convertFunc )
        {
            List<OutputType> result = new List<OutputType>();
            foreach( InputType input in items )
            {
                result.Add( convertFunc( input ) );
            }
            return result;
        }

        public static List<OutputType> DynamicCastAll<InputType,OutputType>( IEnumerable<InputType> items )
        {
            return All<InputType,OutputType>( items, (item)=>{
                    return DynamicCast<OutputType>( item );
                });
        }

        public static ResultType DynamicCast<ResultType>( object field )
        {
            ResultType result = default(ResultType);
            object resultValue = DynamicCast( field, typeof(ResultType) );
            if( resultValue != null )
            {
                result = (ResultType)resultValue;
            }

            return result;
        }

        public static object DynamicCast( object field, System.Type resultType )
        {
            object result = null;

            System.Type fieldType = field.GetType();

            // the value in the dictionary can be cast and passed back directly
            if( resultType.IsAssignableFrom( fieldType ) )
            {
                result = field;
            }
            else if( fieldType == typeof( System.String ) && resultType.IsEnum )
            {
                result = System.Enum.Parse( resultType, field.ToString(), true );
            }
            else if( fieldType == typeof( System.String ) && resultType.IsValueType )
            {
                result = System.Convert.ChangeType( field, resultType );
            }
            else if( fieldType.IsValueType )
            {
                try
                {
                    result = System.Convert.ChangeType( field, resultType );
                }
                catch( System.InvalidCastException )
                {
                    Dbg.Error( "Unable to cast value type to " + resultType.ToString() );
                }
            }
            else if( resultType.IsArray )
            {
                System.Type arrayItemType = resultType.GetElementType();

                System.Array fields = (System.Array)field;
                System.Array array = (System.Array)System.Activator.CreateInstance( resultType, new object[] { fields.Length } );

                for( int i = 0; i < fields.Length; ++i )
                {
                    array.SetValue( Convert.DynamicCast( fields.GetValue(i), arrayItemType ), i );
                }

                result = array;
            }
            else if( resultType == typeof( System.DateTime ) )
            {
                result = System.DateTime.Parse( (string)field );
            }
            else if( fieldType == typeof( JsonObject ) )
            {
                if( resultType.GetInterfaces().Contains( typeof( IJsonSerializable ) ) )
                {
                    if( result == null )
                    {
                        result = System.Activator.CreateInstance( resultType );
                    }

                    ((IJsonSerializable)result).FromJson( (JsonObject)field );
                }
                else
                {
                    // check to see if we have an extension method to read from json
                    MethodInfo method = resultType.GetExtensionMethod( "FromJson" );
                    if( method != null )
                    {
                        ParameterInfo[] info = method.GetParameters();
                        if( info != null )
                        {
                            if( resultType.IsValueType )
                            {
                                if( info.Length == 2
                                        && info[0].ParameterType == resultType
                                        && info[1].ParameterType == typeof( JsonObject )
                                        && method.ReturnType == resultType
                                    )
                                {
                                    result = method.Invoke( null, new object[]{ null, field } );
                                }
                            }
                            else
                            {
                                if( info.Length == 2
                                    && info[0].ParameterType == resultType
                                    && info[1].ParameterType == typeof( JsonObject )
                                )
                                {
                                    result = System.Activator.CreateInstance( resultType );
                                    method.Invoke( null, new object[]{ result, field } );
                                }
                            }
                        }
                    }
                }
            }

            return result;
        }
    }
}
