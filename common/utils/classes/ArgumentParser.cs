//=============================================================================
//
// (C) BLACKTRIANGLES 2016
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System.Collections.Generic;

namespace blacktriangles
{
    public class ArgumentParser
    {
        // members /////////////////////////////////////////////////////////////
        private Dictionary< string, List<string> > args     = new Dictionary< string, List<string> >();

        // constructor /////////////////////////////////////////////////////////
        public ArgumentParser( IEnumerable<string> arguments )
        {
            Parse( arguments );
        }

        public bool HasArgument( string arg )
        {
            return GetValues( arg ) != null;
        }

        public List<string> GetValues( string arg )
        {
            List<string> result = null;
            args.TryGetValue( arg, out result );
            return result;
        }

        public ResultType GetValue<ResultType>( string arg, ResultType defaultValue )
        {
            System.Type resultType = typeof( ResultType );
            if( resultType != typeof( string ) &&
                resultType != typeof( int ) &&
                resultType != typeof( float ) &&
                resultType != typeof( bool ) &&
                resultType != typeof( double ) )
            {
                throw new System.ArgumentException( System.String.Format( "Type {0} is not compatible with ArgumentParser", typeof(ResultType).ToString() ) );
            }

            List<string> values = GetValues( arg );
            if( values == null || values.Count < 1 ) return defaultValue;

            return Convert.DynamicCast<ResultType>(values[0]);
        }

        // private methods /////////////////////////////////////////////////////
        private void Parse( IEnumerable<string> arguments )
        {
            string currentArg = System.String.Empty;
            List<string> items = new List<string>();
            foreach( string arg in arguments )
            {
                if( System.String.IsNullOrEmpty( arg ) ) continue;
                if( arg[0] == '-' )
                {
                    if( currentArg != System.String.Empty )
                    {
                        Commit( currentArg, items );
                    }

                    currentArg = arg.TrimStart( '-' );
                    items = new List<string>();
                }
                else if( arg != System.String.Empty && System.String.IsNullOrEmpty( currentArg ) == false )
                {
                    items.Add( arg );
                }
            }

            Commit( currentArg, items );
        }

        private void Commit( string arg, List<string> items )
        {
            if( arg != System.String.Empty )
            {
                args[ arg ] = items;
            }
        }
    }
}
