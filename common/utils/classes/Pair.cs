//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System;
using System.Collections.Generic;

namespace blacktriangles
{
    public struct Pair<T,U>
    {
        // members ////////////////////////////////////////////////////////////
        public T first;
        public U second;

        // constructor / initializer //////////////////////////////////////////
        public Pair( T _first, U _second )
        {
            first = _first;
            second = _second;
        }

        // operators //////////////////////////////////////////////////////////
        public override bool Equals( object comp )
        {
            Pair<T,U> rhs = (Pair<T,U>)comp;
            return ( EqualityComparer<T>.Default.Equals( first, rhs.first ) && EqualityComparer<U>.Default.Equals( second, rhs.second ) );
        }

        public override int GetHashCode()
        {
            return first.GetHashCode();
        }
    }
}
