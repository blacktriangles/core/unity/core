//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System.Collections;

namespace blacktriangles
{
    public abstract class Coroutine
    {
        //
        // events /////////////////////////////////////////////////////////////
        //

        public delegate void CompleteCallback();
        public event CompleteCallback OnComplete;
        
        //
        // members ////////////////////////////////////////////////////////////
        //

        private IEnumerator enumerator                          = null;
        public bool started                                     { get; private set; }
        public bool completed                                   { get; private set; }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public virtual void Start()
        {
            started = true;
            completed = false;
            enumerator = OnUpdate();
        }

        //
        // --------------------------------------------------------------------
        //

        public virtual bool Update()
        {
            if(completed) return true;
            if(started == false) Start();

            bool finished = Process( enumerator );
            if(finished)
            {
                completed = true;
                NotifyComplete();
                HandleComplete();
            }

            return finished;
        }

        //
        // protected methods //////////////////////////////////////////////////
        //
        
        protected abstract IEnumerator OnUpdate();

        //
        // --------------------------------------------------------------------
        //

        protected virtual void HandleComplete()
        {
            // noop
        }

        //
        // --------------------------------------------------------------------
        //

        protected IEnumerator WaitForSeconds(double seconds)
        {
            double start = Epoch.now;
            while((Epoch.now - start) < seconds)
            {
                yield return false;
            }
        }

        //
        // --------------------------------------------------------------------
        //

        protected IEnumerator WaitForSeconds(double seconds, System.Action<double> cb)
        {
            double start = Epoch.now;
            double elapsed = 0f;
            while(elapsed < seconds)
            {
                elapsed = Epoch.now - start;
                cb(elapsed);
                yield return false;
            }
        }
        

        //
        // private methods ////////////////////////////////////////////////////
        //

        private void NotifyComplete()
        {
            CompleteCallback cb = OnComplete;
            if(cb != null)
            {
                cb();
            }
        }

        //
        // --------------------------------------------------------------------
        //
        

        private bool Process( IEnumerator enumerator )
        {
            bool finished = true;
            if( enumerator != null )
            {
                IEnumerator subEnumerator = enumerator.Current as IEnumerator;
                if( subEnumerator != null )
                {
                    bool subfinished = Process( subEnumerator );
                    if( subfinished )
                    {
                        finished = !enumerator.MoveNext();
                    }
                    else
                    {
                        finished = false;
                    }
                }
                else
                {
                    finished = !enumerator.MoveNext();
                }
            }

            return finished;
        }

        //
        // end class //////////////////////////////////////////////////////////
        //
    }
}
