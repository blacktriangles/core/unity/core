//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System;
using System.Collections.Generic;

namespace blacktriangles
{
    public static class Platform
    {
        // types //////////////////////////////////////////////////////////////
        public enum Type { Windows, OSX };

        // constants //////////////////////////////////////////////////////////
        public const Type current =
            #if UNITY_STANDALONE_WIN
                Type.Windows;
            #else
                Type.OSX;
            #endif

    }
}
