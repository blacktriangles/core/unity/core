//
// blacktriangles (c) 2019
//

using UnityEngine;

namespace blacktriangles
{

    //
    // Cardinal Directions ////////////////////////////////////////////////////
    //

    public enum OrdinalDirection
    {
        North = 0,
        Northeast,
        East,
        Southeast,
        South,
        Southwest,
        West,
        Northwest
    }

    //
    // Utilities //////////////////////////////////////////////////////////////
    //

    public static class OrdinalDirectionUtils
    {
        //
        // --------------------------------------------------------------------
        //
        
        public static OrdinalDirection Inverse(OrdinalDirection dir)
        {
            switch(dir)
            {
                case OrdinalDirection.North: return OrdinalDirection.South;
                case OrdinalDirection.Northeast: return OrdinalDirection.Southwest;
                case OrdinalDirection.East: return OrdinalDirection.West;
                case OrdinalDirection.Southeast: return OrdinalDirection.Northwest;
                case OrdinalDirection.South: return OrdinalDirection.North;
                case OrdinalDirection.Southwest: return OrdinalDirection.Northeast;
                case OrdinalDirection.West: return OrdinalDirection.East;
                case OrdinalDirection.Northwest: return OrdinalDirection.Southeast;
            }

            throw new System.ArgumentException("Invalid input direction dir");
        }

        //
        // --------------------------------------------------------------------
        //

        public static OrdinalDirection RotateClockwise(OrdinalDirection dir, uint steps = 1)
        {
            switch(dir)
            {
                case OrdinalDirection.North: return OrdinalDirection.Northeast;
                case OrdinalDirection.Northeast: return OrdinalDirection.East;
                case OrdinalDirection.East: return OrdinalDirection.Southeast;
                case OrdinalDirection.Southeast: return OrdinalDirection.South;
                case OrdinalDirection.South: return OrdinalDirection.Southwest;
                case OrdinalDirection.Southwest: return OrdinalDirection.West;
                case OrdinalDirection.West: return OrdinalDirection.Northwest;
                case OrdinalDirection.Northwest: return OrdinalDirection.North;
            }

            throw new System.ArgumentException("Invalid input direction dir");
        }

        //
        // --------------------------------------------------------------------
        //

        public static Vector3 ToDirectionXZ(OrdinalDirection dir)
        {
            switch(dir)
            {
                case OrdinalDirection.North:     return new Vector3( 0f, 0f, 1f);
                case OrdinalDirection.Northeast: return new Vector3( 1f, 0f, 1f).normalized;
                case OrdinalDirection.East:      return new Vector3( 1f, 0f, 0f);
                case OrdinalDirection.Southeast: return new Vector3( 1f, 0f,-1f).normalized;
                case OrdinalDirection.South:     return new Vector3( 0f, 0f,-1f);
                case OrdinalDirection.Southwest: return new Vector3(-1f, 0f,-1f);
                case OrdinalDirection.West:      return new Vector3(-1f, 0f, 0f);
                case OrdinalDirection.Northwest: return new Vector3(-1f, 0f, 1f);
            }

            throw new System.ArgumentException("Invalid input direction dir");
        }

        //
        // --------------------------------------------------------------------
        //

        public static IntVec2 ToIntVec2(OrdinalDirection dir)
        {
            switch(dir)
            {
                case OrdinalDirection.North:     return IntVec2.north; 
                case OrdinalDirection.Northeast: return IntVec2.northeast; 
                case OrdinalDirection.East:      return IntVec2.east; 
                case OrdinalDirection.Southeast: return IntVec2.southeast; 
                case OrdinalDirection.South:     return IntVec2.south; 
                case OrdinalDirection.Southwest: return IntVec2.southwest; 
                case OrdinalDirection.West:      return IntVec2.west; 
                case OrdinalDirection.Northwest: return IntVec2.northwest; 
            }

            throw new System.ArgumentException("Invalid input direction dir");

        }
    }
}
    
