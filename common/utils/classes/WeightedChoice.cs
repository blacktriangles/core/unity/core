//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;
using System.Collections.Generic;

namespace blacktriangles
{
    public class WeightedChoice<T>
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        [System.Serializable]
        public class Choice
        {
            public float weight;
            public T value;
        }

        //
        // members ////////////////////////////////////////////////////////////
        //

        [SerializeField] private List<Choice> choices           = new List<Choice>();

        private float total                                     = -1;
        private float max                                       = 0f;
        private float min                                       = System.Single.MaxValue;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void Normalize()
        {
            foreach(Choice choice in choices)
            {
                min = Mathf.Min(min, choice.weight);
                max = Mathf.Max(max, choice.weight);
            }

            float last = 0f;
            total = 0f;
            foreach(Choice choice in choices)
            {
                choice.weight = (choice.weight - min) / (max-min) + last;
                last = choice.weight;
                total += choice.weight;
            }
        }

        //
        // --------------------------------------------------------------------
        //
        

        public T Select()
        {
            return Select(null);
        }

        //
        // --------------------------------------------------------------------
        //
        
        public T Select(System.Random rnd)
        {
            if(rnd == null) rnd = new System.Random();
            if(total < 0f)
            {
                Normalize();
            }

            float sel = rnd.Next(0f, total);
            foreach(Choice choice in choices)
            {
                if(choice.weight < sel)
                {
                    return choice.value;
                }
            }

            return default(T);
        }
    }
}
