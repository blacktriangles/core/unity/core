//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;

namespace blacktriangles
{
    public class BaseTimestampedValue
    {
        public virtual object baseValue                         { get { return null; } }
        public virtual double timestamp                         { get; protected set; }
        public virtual double accessTimestamp                   { get; protected set; }
        public double elapsed                                   { get { return Epoch.now - timestamp; } }
        public double accessElapsed                             { get { return Epoch.now - accessTimestamp; } }
    }

    //
    // ////////////////////////////////////////////////////////////////////////
    //
    
    public class TimestampedValue<T>
        : BaseTimestampedValue
    {
        //
        // events /////////////////////////////////////////////////////////////
        //

        public delegate void ValueChangedCallback(T prev, T curr);
        public event ValueChangedCallback OnValueChanged;
        
        //
        // members ////////////////////////////////////////////////////////////
        //

        public T val                                            { get { return GetValue(); } set { Set( value ); } }
        public override object baseValue                        { get { return val; } }

        private T _val;

        //
        // constructor / initializer //////////////////////////////////////////
        //

        public TimestampedValue()
        {
            val = default( T );
            timestamp = Epoch.now;
            accessTimestamp = Epoch.now;
        }

        //
        // --------------------------------------------------------------------
        //

        public TimestampedValue( T defaultValue )
            : this()
        {
            val = defaultValue;
        }

        //
        // --------------------------------------------------------------------
        //

        public TimestampedValue( TimestampedValue<T> copy )
            : this( copy.val )
        {
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void Set( T newValue )
        {
            if(_val.Equals(newValue) == false)
            {
                timestamp = Epoch.now;
                T old = _val;
                _val = newValue;
                NotifyValueChanged(old, _val);
            }
        }

        //
        // --------------------------------------------------------------------
        //
        

        public void Restamp()
        {
            timestamp = Epoch.now;
            accessTimestamp = Epoch.now;
        }

        //
        // --------------------------------------------------------------------
        //

        public void ForceSet( T newValue, double elapsed)
        {
            timestamp = Epoch.now - elapsed;
            T old = _val;
            _val = newValue;
            NotifyValueChanged(old, _val);
        }

        //
        // protected methods //////////////////////////////////////////////////
        //

        protected T GetValue()
        {
            accessTimestamp = Epoch.now;
            return _val;
        }

        //
        // --------------------------------------------------------------------
        //
        
        protected void NotifyValueChanged(T prev, T curr)
        {
            var cb = OnValueChanged;
            if(cb != null)
            {
                cb(prev, curr);
            }
        }

        //
        // operators //////////////////////////////////////////////////////////
        //

        public static implicit operator T( TimestampedValue<T> v )
        {
            return v.val;
        }
    }
}
