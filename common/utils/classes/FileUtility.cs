//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using System;
using System.IO;

namespace blacktriangles
{
    public static class FileUtility
    {
        //
        // utility functions //////////////////////////////////////////////////
        //

        public static void EnsureDirectoryExists( string path )
        {
            if( !System.IO.Directory.Exists( path ) )
                Directory.CreateDirectory( path );
        }

        //
        // --------------------------------------------------------------------
        //
        
        public static void ForEachDirectory( string srcDir, System.Action<DirectoryInfo> action )
        {
            if( !System.IO.Directory.Exists(srcDir) ) return;

            DirectoryInfo dir = new DirectoryInfo(srcDir);
            DirectoryInfo[] subDirs = dir.GetDirectories();

            try
            {
                #if UNITY_EDITOR
                    UnityEditor.EditorUtility.DisplayProgressBar(System.String.Format("Processing {0}", srcDir), System.String.Empty, 0f);
                    int i = 0;
                #endif

                foreach(DirectoryInfo subdir in subDirs)
                {
                    action(subdir);
                    #if UNITY_EDITOR
                        UnityEditor.EditorUtility.DisplayProgressBar( System.String.Format( "Processing {0}", srcDir ), System.String.Format( "Processing {0}", subdir.FullName ), ((float)i++/subDirs.Length) );
                    #endif
                }
            }
            finally
            {
                #if UNITY_EDITOR
                    UnityEditor.EditorUtility.ClearProgressBar();
                #endif
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public static void ForEachFile( string srcDir, System.Action<FileInfo> action )
        {
            if( !System.IO.Directory.Exists( srcDir ) ) return;

            DirectoryInfo dir = new DirectoryInfo( srcDir );
            DirectoryInfo[] subDirs = dir.GetDirectories();
            FileInfo[] files = dir.GetFiles();

            try
            {
                #if UNITY_EDITOR
                    UnityEditor.EditorUtility.DisplayProgressBar( System.String.Format( "Processing {0}", srcDir ), System.String.Empty, 0f );
                    int i = 0;
                #endif
                foreach( FileInfo file in files )
                {
                    action(file);
                    #if UNITY_EDITOR
                        UnityEditor.EditorUtility.DisplayProgressBar( System.String.Format( "Processing {0}", srcDir ), System.String.Format( "Processing {0}", file.FullName ), ((float)i++/files.Length) );
                    #endif
                }
            }
            finally
            {
                #if UNITY_EDITOR
                    UnityEditor.EditorUtility.ClearProgressBar();
                #endif
            }

            foreach( DirectoryInfo subDir in subDirs )
            {
               ForEachFile( subDir.FullName, action );
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public static void CopyDirectory( string srcDir, string dstDir, System.Action<FileInfo> process = null, System.Func<string, string> procFilename = null )
        {
            if( !System.IO.Directory.Exists( srcDir ) ) return;

            EnsureDirectoryExists( dstDir );

            if( System.IO.Directory.Exists( dstDir ) == false ) Dbg.Error( "Directory " + dstDir + " does not exist!" );

            string fullSrcDir = System.IO.Path.GetFullPath(srcDir);
            string fullDstDir = System.IO.Path.GetFullPath(dstDir);
            ForEachFile( srcDir, (file)=> {
                string dstPath = file.FullName.Replace( fullSrcDir, fullDstDir );
                if(procFilename != null)
                {
                    dstPath = procFilename(dstPath);
                }

                EnsureDirectoryExists( System.IO.Path.GetDirectoryName(dstPath) );
                System.IO.File.Copy( file.FullName, dstPath, true );
                Dbg.Log("Copy {0} to {1}", file.FullName, dstPath);
                if( process != null )
                {
                    FileInfo info = new FileInfo(dstPath);
                    process(info);
                }
            });
        }

        //
        // --------------------------------------------------------------------
        //

        public static string MakePathRelative( string path, string fromDir )
        {
            System.Uri fullPath = new Uri( path );
            System.Uri relRoot = new Uri( fromDir );
            return System.Uri.UnescapeDataString(relRoot.MakeRelativeUri( fullPath ).ToString());
        }

        //
        // --------------------------------------------------------------------
        //

        public static void FindAndReplaceTextFile( string path, string find, string replace )
        {
            string text = File.ReadAllText(path);
            text = text.Replace(find, replace);
            File.WriteAllText(path, text);
        }

        //
        // --------------------------------------------------------------------
        //

        #if UNITY_EDITOR
        public static string MakePathRelativeToAssetDir( string fromPath )
        {
            return MakePathRelative( fromPath, System.IO.Path.GetFullPath( "Assets" ) );
        }
        #endif

        //
        // --------------------------------------------------------------------
        //
        
        public static string PathCombine( params string[] parts )
        {
            // unity doesn't want System Paths, it wants '/' paths
            #if UNITY_EDITOR
                return System.String.Join( "/", parts );
            #else
                return PathCombineSystemSeparator( parts );
            #endif
        }

        //
        // --------------------------------------------------------------------
        //

        public static string PathCombineSystemSeparator( params string[] parts )
        {
            return System.String.Join( System.IO.Path.DirectorySeparatorChar.ToString(), parts );
        }
    }
}
