//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

namespace blacktriangles
{
    public struct SmoothAccumilator<T>
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public T current                                        { get { return accumilated.current; } }
        public T last                                           { get { return accumilated.last; } } 
        public T delta                                          { get { return accumilated.delta; } }
        
        private SmoothValue<T> smooth;
        private Accumilator<T> accumilated;

        //
        // constructor ////////////////////////////////////////////////////////
        //
        
        public SmoothAccumilator(int count)
        {
            smooth = new SmoothValue<T>(count);
            accumilated = new Accumilator<T>();
        }

        //
        // --------------------------------------------------------------------
        //
        
        public SmoothAccumilator(int count, T defaultValue)
        {
            smooth = new SmoothValue<T>(count);
            accumilated = new Accumilator<T>(defaultValue);
        }

        //
        // public methods /////////////////////////////////////////////////////
        //
        
        public void Add(T value)
        {
            smooth.Add(value);
            accumilated.Add(smooth.smoothed);
        }

        //
        // ----------------------------------------------------------------------------
        //
        
        public void Set(T value)
        {
             accumilated.Set(value);
        }
    }
}
