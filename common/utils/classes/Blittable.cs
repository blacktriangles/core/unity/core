//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System;
using System.Runtime.Serialization;
using System.Runtime.InteropServices;

// https://stackoverflow.com/questions/10574645/the-fastest-way-to-check-if-a-type-is-blittable

namespace blacktriangles
{
    public static class BlittableHelper
    {
        //
        // public static methods //////////////////////////////////////////////
        //
        
        public static bool IsBlittable<T>()
        {
            return IsBlittableCache<T>.Value;
        }

        //
        // --------------------------------------------------------------------
        //
    
        public static bool IsBlittable(this Type type)
        {
            if (type.IsArray)
            {
                var elem = type.GetElementType();
                return elem.IsValueType && IsBlittable(elem);
            }
            try
            {
                object instance = FormatterServices.GetUninitializedObject(type);
                GCHandle.Alloc(instance, GCHandleType.Pinned).Free();
                return true;
            }
            catch
            {
                return false;
            }
        }

        //
        // --------------------------------------------------------------------
        //
    
        private static class IsBlittableCache<T>
        {
            public static readonly bool Value = IsBlittable(typeof(T));
        }
    }
}
