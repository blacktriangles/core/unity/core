//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Xml;
using UnityEngine;

namespace blacktriangles
{
    public static class XmlNodeExtension
    {
        //
        // --------------------------------------------------------------------
        //
        
        public static string Attribute(this XmlNode self, string name, string def = null)
        {
            if(self.Attributes != null)
            {
                XmlNode attrib = self.Attributes[name];
                return attrib == null ? def : attrib.Value;
            }

            return def;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public static T EnumAttribute<T>(this XmlNode self, string name, T def, bool caseInsensitive = false)
            where T : System.Enum
        {
            string attrib = self.Attribute(name);
            if(attrib != null)
            {
                return EnumUtility.Convert<T>(attrib, caseInsensitive);
            }

            return def;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public static Color ColorAttribute(this XmlNode self, string name, Color def)
        {
            string attrib = self.Attribute(name);
            if(attrib != null)
            {
                return ColorExtension.FromHexString(attrib);
            }

            return def;
        }

        //
        // --------------------------------------------------------------------
        //

        public static float FloatAttribute(this XmlNode self, string name, float def)
        {
            string attrib = self.Attribute(name);
            if(attrib != null)
            {
                return System.Single.Parse(attrib);
            }

            return def;
        }

        //
        // --------------------------------------------------------------------
        //
    }
}
