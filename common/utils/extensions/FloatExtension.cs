//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;

public static class FloatExtension
{
    public static bool IsApproximately( this float self, float rhs )
    {
        return btMath.Approximately( self, rhs );
    }

    public static bool IsNearZero( this float self )
    {
        return btMath.Approximately( self, 0 );
    }

    public static int FloorInt( this float self )
    {
        return (int)btMath.Floor( self );
    }

    public static int RoundInt( this float self )
    {
        return (int)btMath.Round( self );
    }

    public static int CeilInt( this float self )
    {
        return (int)btMath.Ceil( self );
    }
}
