//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

public static class DoubleExtension
{
    public const double kEpsilon                                = 0.0001;

    public static bool IsApproximately( this double self, double rhs )
    {
        return System.Math.Abs( self - rhs ) < kEpsilon;
    }

    public static bool IsNearZero( this double self )
    {
        return self.IsApproximately( 0 );
    }
}
