//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;

public static class Vector2Extension
{
    public static Vector3 CloneWithX( this Vector2 self, float x )
    {
        return new Vector3( x, self.y, self.x );
    }

    //
    // ------------------------------------------------------------------------
    //

    public static Vector3 CloneWithY( this Vector2 self, float y )
    {
        return new Vector3( self.x, y, self.y );
    }

    //
    // ------------------------------------------------------------------------
    //

    public static Vector3 CloneWithZ( this Vector2 self, float z )
    {
        return new Vector3( self.x, self.y, z );
    }

    //
    // ------------------------------------------------------------------------
    //

    public static Vector3 ToVector3XY( this Vector2 self )
    {
        return CloneWithZ( self, 0.0f );
    }

    //
    // ------------------------------------------------------------------------
    //

    public static Vector2 Round( this Vector2 self )
    {
        return new Vector2( btMath.Round( self.x ), btMath.Round( self.y ) );
    }

    //
    // ------------------------------------------------------------------------
    //

    public static Vector2 Ceil( this Vector2 self )
    {
        return new Vector2( btMath.Ceil( self.x ), btMath.Ceil( self.y ) );
    }

    //
    // ------------------------------------------------------------------------
    //

    public static Vector2 Floor( this Vector2 self )
    {
        return new Vector2( btMath.Floor( self.x ), btMath.Floor( self.y ) );
    }

    //
    // ------------------------------------------------------------------------
    //

    public static bool IsNearZero( this Vector2 self )
    {
        return self.sqrMagnitude < Mathf.Epsilon;
    }

    //
    // ------------------------------------------------------------------------
    //
    
    public static bool IsNaN( this Vector2 self )
    {
        return System.Single.IsNaN(self.sqrMagnitude);
    }

    //
    // ------------------------------------------------------------------------
    //

    public static Vector3 ToVector3( this Vector2 self )
    {
        return new Vector3( self.x, self.y, 0f );
    }

    //
    // ------------------------------------------------------------------------
    //

    public static Vector3 ToVector3XZ( this Vector2 self )
    {
        return new Vector3( self.x, 0f, self.y );
    }

    //
    // ------------------------------------------------------------------------
    //

    public static Vector2 RandomRange( Vector2 min, Vector2 max )
    {
        return new Vector2( btRandom.Range( min.x, max.x ), btRandom.Range( min.y, max.y ) );
    }

    //
    // ------------------------------------------------------------------------
    //
    
    public static Vector2 Random()
    {
        return RandomRange(-Vector2.one, Vector2.one);
    }

    //
    // ------------------------------------------------------------------------
    //

    public static Vector2 VectorScale( this Vector2 self, Vector2 scale )
    {
        return new Vector2( self.x * scale.x, self.y * scale.y );
    }

    //
    // ------------------------------------------------------------------------
    //

    public static Vector2 Swizzle( this Vector2 self )
    {
        return new Vector2(self.y, self.x);
    }

    //
    // ------------------------------------------------------------------------
    //

    public static Vector2 Interp( this Vector2 self, Vector2 target, Vector2 max)
    {
        return self + new Vector2(
            Mathf.Clamp(target.x - self.x, -max.x, max.x),
            Mathf.Clamp(target.y - self.y, -max.y, max.y));
    }

    //
    // ------------------------------------------------------------------------
    //
    
    public static Vector2 Interp( this Vector2 self, Vector2 target, float max)
    {
        return self.Interp(target, new Vector2(max,max));
    }

    //
    // ------------------------------------------------------------------------
    //
    
    public static Vector2 Max( Vector2 lhs, Vector2 rhs )
    {
        return new Vector2( btMath.Max(lhs.x, rhs.x), btMath.Max(lhs.y, rhs.y) );
    }
}
