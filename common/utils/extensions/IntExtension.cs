//
// (c) BLACKTRIANGLES 2016
// http://www.blacktriangles.com
//

namespace blacktriangles
{
    public static class IntExtension
    {
        public static bool IsFlagSet<EnumType>( this int self, EnumType e )
            where EnumType: System.Enum
        {
            int flag = e.ToFlag();
            return (self & flag) == flag;
        }
    }
}
