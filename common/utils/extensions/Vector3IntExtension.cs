//
// (c) BLACKTRIANGLES 2021
// http://www.blacktriangles.com
//

using UnityEngine;
using System.Collections.Generic;

namespace blacktriangles
{
    public static class Vector3IntExtension
    {
        public static IEnumerable<Vector3Int> EachVector3Int(this Vector3Int self, int start = 0, int step = 1)
        {
            for(int y = start; y < self.y; ++y)
            {
                for(int x = start; x < self.x; ++x)
                {
                    for(int z = start; z < self.z; ++z)
                    {
                        yield return new Vector3Int(x,y,z);
                    }
                }
            }
        }
    }
}
