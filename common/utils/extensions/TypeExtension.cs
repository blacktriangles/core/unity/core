//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
// Stolen shamelessly from stack overflow post by Wolfgang Stelzhammer
// http://stackoverflow.com/questions/299515/reflection-to-identify-extension-methods
//
// Based on an answer made by another user John Skeet
//=============================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace blacktriangles
{
    public static class TypeExtension
    {
        public static MethodInfo[] GetExtensionMethods( this Type self )
        {
            List<Type> AssTypes = new List<Type>();

            foreach (Assembly item in AppDomain.CurrentDomain.GetAssemblies())
            {
                AssTypes.AddRange(item.GetTypes());
            }

            var query = from type in AssTypes
                where type.IsSealed && !type.IsGenericType && !type.IsNested
                from method in type.GetMethods(BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic)
                where method.IsDefined(typeof(ExtensionAttribute), false)
                where method.GetParameters()[0].ParameterType == self
                select method;
            return query.ToArray<MethodInfo>();
        }

        public static MethodInfo GetExtensionMethod( this Type self, string MethodeName )
        {
            var mi = from methode in self.GetExtensionMethods()
                where methode.Name == MethodeName
                select methode;
            if (mi.Count<MethodInfo>() <= 0)
                return null;
            else
                return mi.First<MethodInfo>();
        }
    }
}
