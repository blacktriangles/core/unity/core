//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

namespace blacktriangles
{
    //
    // Deep Cloneable /////////////////////////////////////////////////////////
    //

    public interface IDeepCloneable<T>
    {
        T DeepClone();
    }

    //
    // Shallow Cloneable //////////////////////////////////////////////////////
    //

    public interface IShallowCloneable<T>
    {
        T ShallowClone();
    }
}
