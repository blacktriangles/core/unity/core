//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
	public class EnumFlagAttribute 
        : PropertyAttribute
    {
        public System.Type enumType;
        
        public EnumFlagAttribute(System.Type enumType_)
        {
            enumType = enumType_;
        }
	}
}
