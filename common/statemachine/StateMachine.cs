//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System.Collections;
using System.Collections.Generic;

namespace blacktriangles.FSM
{
    public class StateMachine
    {
        // members ////////////////////////////////////////////////////////////
        public IState currentState                                { get; private set; }
        public bool isActive                                    { get; set; }

        // constructor / destructor ///////////////////////////////////////////
        public StateMachine()
        {
            isActive = true;
        }

        // public methods /////////////////////////////////////////////////////
        public void RequestStateChange( IState newState )
        {
            if( newState != null )
            {
                newState.Initialize( this );
            }

            if( currentState != null )
            {
                currentState.ExitState();
            }

            IState oldState = currentState;
            currentState = newState;

            if( currentState != null )
            {
                currentState.EnterState( oldState );
            }
        }

        public void Update()
        {
            if( isActive )
            {
                if( currentState != null )
                    currentState.UpdateState();
            }
        }
    }
}
