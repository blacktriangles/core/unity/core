//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System.Collections.Generic;

namespace blacktriangles.FSM
{
    public abstract class BaseState
        : IState
    {
        // members ////////////////////////////////////////////////////////////
        public StateMachine stateMachine                        { get; protected set; }
        public virtual string stateName                            { get { return GetType().Name; } }
        public virtual bool isActive                            { get { return _isActive; } }
        private bool _isActive                                    = false;

        // constructor / initializer //////////////////////////////////////////
        public virtual void Initialize( StateMachine _stateMachine )
        {
            stateMachine = _stateMachine;
        }

        // public methods /////////////////////////////////////////////////////
        public void UpdateState()
        {
            OnUpdate();
        }

        public void EnterState( IState prevState )
        {
            _isActive = true;
            OnEnter( prevState );
        }

        public void ExitState()
        {
            _isActive = false;
            OnExit();
        }

        public abstract void OnUpdate();
        public abstract void OnEnter( IState prevState );
        public abstract void OnExit();
    }
}
